<?php /* Template_ 2.2.7 2018/03/06 10:43:50 /home/dartz/public_html/application/views/point/index.html 000006014 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4>포인트 적립안내</h4>
                    <p class="sm">일루마 사이트 회원가입, 아기정보등록 등 다양한 방법으로 포인트 적립이 가능하며, 결제금액의 10%도 포인트로 적립됩니다.<br>
                        <br>
                        포인트는 적립한 시점으로부터 24개월이 지나면 소멸되어 복구되지 않습니다.<br>
                        포인트가 소멸되기 전에 사이트 이벤트 참여, 상품권 교환 등을 통해 포인트를 모두 사용해 주세요.
                    </p>
                </div>

                <div class="page_body join_form">
                    <section>
                        <h4 style="margin-top:0;">기본 정보</h4>
                        <div class="tb tb_view tb_point">
                            <table>
                                <colgroup>
                                    <col width="200px">
                                    <col width="120px">
                                    <col width="*">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="line">항목</th>
                                        <th class="line">포인트</th>
                                        <th>내용</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="line">회원가입</td>
                                        <td class="line"><strong>500</strong></td>
                                        <td>최초 가입 시 1회 제공</td>
                                    </tr>
                                    <tr>
                                        <td class="line">아기정보등록</td>
                                        <td class="line"><strong>500</strong></td>
                                        <td>아기정보를 처음 등록 시 지급</td>
                                    </tr>
                                    <tr>
                                        <td class="line">제품구매</td>
                                        <td class="line"><strong>10%</strong></td>
                                        <td>결제금액의 10% 포인트 적립</td>
                                    </tr>
                                    <tr>
                                        <td class="line">로그인</td>
                                        <td class="line"><strong>10</strong></td>
                                        <td>ID당 매일 1회 제한</td>
                                    </tr>
                                    <tr>
                                        <td class="line">추천인</td>
                                        <td class="line"><strong>50</strong></td>
                                        <td>가입 시 추천인 ID 입력하면 가입 회원에게 제공</td>
                                    </tr>
                                    <tr>
                                        <td class="line">피추천인</td>
                                        <td class="line"><strong>100</strong></td>
                                        <td>가입 시 추천인 ID 입력하면 추천받은 회원에게 제공</td>
                                    </tr>
                                    <tr>
                                        <td class="line">이벤트참여리워드</td>
                                        <td class="line"><strong>50</strong></td>
                                        <td>적립이벤트 당 1회 참여</td>
                                    </tr>
                                    <tr>
                                        <td class="line">이벤트 참여 1</td>
                                        <td class="line"><strong>3000/5000</strong></td>
                                        <td>소모 이벤트(참여형)에 따라 다름. 중복 가능</td>
                                    </tr>
                                    <tr>
                                        <td class="line">이벤트 참여 2</td>
                                        <td class="line"><strong>500</strong></td>
                                        <td>소모 이벤트(댓글형) 당 1회 참여</td>
                                    </tr>
                                    <tr>
                                        <td class="line">이벤트 참여 3</td>
                                        <td class="line"><strong>0</strong></td>
                                        <td>무료 이벤트. 매월 1회. 포인트가 없어도 회원가입한 회원은 참여 가능</td>
                                    </tr>
                                    <tr>
                                        <td class="line">상품권 교환</td>
                                        <td class="line"><strong>50000</strong></td>
                                        <td>5만원권 상품권 교환.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>

                </div>
            </div>
        </section>
    </article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>