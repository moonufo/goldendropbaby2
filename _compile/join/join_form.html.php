<?php /* Template_ 2.2.7 2018/03/09 19:05:22 /home/dartz/public_html/application/views/join/join_form.html 000033103 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<script src="/assets/js/join.js"></script>
<article id="contents">

        <section id="join">
            <div class="container">
                <div class="join_form_wrap">
                    <div class="join_top ">
                        <h3>회원가입<span class="txt">일루마의 회원이 되시면 500포인트를 드립니다.</span></h3>
                    </div>
                    <form method="post" action="/join/join_process" id="frm_input">
                    <div class="join_form">
						<!--<input type="hidden" name="SexFlag" id="SexFlag" value="<?php echo $TPL_VAR["SexFlag"]?>">-->
						<input type="hidden" name="ret_url" id="ret_url" value="<?php echo $TPL_VAR["ret_url"]?>">
						<input type="hidden" name="M_ACCOUNTYN" id="M_ACCOUNTYN" value="N">
						<input type="hidden" name="M_NAMEYN" id="M_NAMEYN" value="N">
                        <section>
                            <h4>기본 정보 입력
                                <p class="txt_caution">*필수 입력 항목</p>
                            </h4>
                            <div class="tb">
                                <table>
                                    <colgroup>
                                        <col width="200px">
                                        <col width="*">
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <th><label for="">아이디 <span class="important">*</span></label></th>
                                            <td class="ipt_id td_M_ACCOUNT">
                                                <input type="text" name="M_ACCOUNT" id="M_ACCOUNT" value="" onchange="chk_data();" foc class="ipt_box sz_lg">
                                                <a href="javascript:chk_id('M');" role="button" class="btn_inner">중복확인</a>
                                                <span class="txt_chk msg_M_ACCOUNT"></span>
                                                <p class="txt_info">아이디는 영문, 숫자를 조합한 6~15자리로 사용하실 수 있습니다.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><label>이름 <span class="important">*</span></label></th>
                                            <td class="ipt_id td_M_NAME">
                                                <input type="text" name="M_NAME" id="M_NAME" value="<?php echo $TPL_VAR["UserName"]?>" onchange="chk_name();" class="ipt_box sz_lg">
                                                <span class="txt_chk msg_M_NAME"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><label>이메일 <span class="important">*</span></label></th>
                                            <td class="td_M_EMAIL">
                                                <input type="text" class="ipt_box sz_md" name="M_EMAIL_1" id="M_EMAIL_1">
                                                <span class="unit">@</span>
                                                <input type="text" class="ipt_box sz_md" name="M_EMAIL_2_txt" id="M_EMAIL_2_txt" placeholder="직접입력">
                                                <div class="select ipt_box sz_md">
                                                    <span>직접입력</span>
                                                    <select id="M_EMAIL_2_sel" name="M_EMAIL_2_sel"  onchange="change_txt();">
                                                    	<option value="">직접입력</option>
    			                                        <option value="nate.com">nate.com</option>
    			                                        <option value="naver.com">naver.com</option>
    			                                        <option value="hanmail.net">hanmail.net</option>
    			                                        <option value="gmail.com">gmail.com</option>
                                                    </select>
                                                </div>
                                     
                                                <p class="txt_chk msg_M_EMAIL"></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><label>생년월일 <span class="important">*</span></label></th>
                                            <td>
                                                <div class="select ipt_box sz_sm" name="bir">
                                                    <span><?php echo $TPL_VAR["now_year"]?>년</span>
                                                    <select name="birthday_year"  id="birthday_year">
                                                        <?php echo $TPL_VAR["year_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <div class="select ipt_box sz_sm">
                                                    <span><?php echo $TPL_VAR["now_month"]?>월</span>
                                                    <select name="birthday_month"  id="birthday_month">
                                                        <?php echo $TPL_VAR["month_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <div class="select ipt_box sz_sm">
                                                    <span><?php echo $TPL_VAR["now_day"]?>일</span>
                                                    <select name="birthday_day"  id="birthday_day">
                                                        <?php echo $TPL_VAR["day_list"]?>

                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><label>비밀번호 <span class="important">*</span></label></th>
                                            <td class="td_M_PW">
                                                <input type="password" name="M_PW" id="M_PW" class="ipt_box sz_lg " >
                                                <span class="txt_chk msg_M_PW"></span>
                                                <p class="txt_info">영문,숫자,특수문자(.!@#$%)를 조합한 8자~20자로 사용하실 수 있습니다.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><label>비밀번호 확인 <span class="important">*</span></label></th>
                                            <td class="td_M_PWCHK">
                                                <input type="password" name="M_PWCHK" id="M_PWCHK" class="ipt_box sz_lg " >
                                                <span class="txt_chk msg_M_PWCHK"></span>
                                            </td>
                                        </tr>
                                    	<tr>
                                            <th rowspan="2" class="rowtop"><label>휴대폰 번호 <span class="important">*</span></label></th>
                                            <td class="line_dash td_M_HP">
                                                <div class="select ipt_box sz_sm">
                                                    <span>010</span>
                                                    <select class="" id="M_HP_1" name="M_HP_1">
                                                        <option <?php if($TPL_VAR["hp1"]=="010"||$TPL_VAR["hp1"]==""){?>selected="selected" <?php }?>>010</option>
                                                        <option <?php if($TPL_VAR["hp1"]=="011"){?>selected="selected" <?php }?> value="011">011</option>
    	                                        		<option<?php if($TPL_VAR["hp1"]=="016"){?>selected="selected" <?php }?>  value="016">016</option>
                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <input type="tel" name="M_HP_2" id="M_HP_2" value="<?php echo $TPL_VAR["hp2"]?>" maxlength="4" class="ipt_box sz_sm">
                                                <span class="unit dash"></span>
                                                <input type="tel" type="tel" name="M_HP_3" value="<?php echo $TPL_VAR["hp3"]?>" id="M_HP_3" maxlength="4" class="ipt_box sz_sm">
                                                <a href="#" onclick="sms_send();return false;" role="button" class="btn_inner line_onoff on">인증번호 발송</a>
                                                <div class="" id="chk_red">
                                            </td>
                                        </tr>
                                       <tr>
                                            <td class="td_M_HP">
                                                <input type="text" name="chknum" id="chknum" class="ipt_box sz_lg" placeholder="휴대폰 인증번호 입력">
                                                <a href="#" onclick="sms_chknum();return false;" role="button" class="btn_inner">인증 확인</a>
                                                
                                                <span class="txt_chk msg_M_HP"></span>
                                            </td>
                                        </tr>
										<input type="hidden" id="textyn" value="N" />
                                        <input type="hidden" id="hp_chk" value="N"/>
                                        <tr>
                                            <th><label>집전화</label></th>
                                            <td>
                                                <div class="select ipt_box sz_sm">
                                                    <span>02</span>
                                                    <select  name="M_CP_1" id="M_CP_1" >
                                                        <?php echo $TPL_VAR["cp_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <input type="tel"  name="M_CP_2" id="M_CP_2" maxlength="4"  class="ipt_box sz_sm">
                                                <span class="unit dash"></span>
                                                <input type="tel" name="M_CP_3" id="M_CP_3" maxlength="4" class="ipt_box sz_sm">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><label>성별 <span class="important">*</span></label></th>
                                            <td>
                                                <div class="select ipt_box sz_sm">
                                                    <span>선택</span>
                                                    <select class=" " name="M_GENDER" id="M_GENDER" >
                                                    	<option value="N" selected="selected">선택</option>
                                                        <option value="F">여</option>
                                                        <option value="M">남</option>
                                                        
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th rowspan="2" class="rowtop"><label>배송지</label></th>
                                            <td class="line_dash">
                                                <input type="text" name="M_POST" id="M_POST" readonly="readonly" value="" class="ipt_box sz_sm">
                                                <a href="#" onclick="get_post();" role="button" class="btn_inner">우편번호 검색</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="M_ADDR1" id="M_ADDR1" readonly="readonly" class="ipt_box sz_xl">
                                                <input type="text" name="M_ADDR2" id="M_ADDR2" placeholder="상세주소" class="ipt_box sz_xl" style="margin-left:8px;">
                                            </td>
                                        </tr>

                                        <tr>
                                       		<th rowspan="2" class="rowtop">마케팅 활동<br>동의 및 수신</th>
                                            <td class="line_dash">
                                                <span class="tit_inner">SMS/MMS/LMS 수신</span>
                                                <span class="radio_wrap">
                                                    <input type="radio" value="Y" name="M_SMS_YN" id="M_SMS_Y">
                                                    <label for="M_SMS_Y"><span></span>동의함</label>
                                                </span>
                                                <span class="radio_wrap">
                                                    <input type="radio" value="N" checked="checked" name="M_SMS_YN" id="M_SMS_N">
                                                    <label for="M_SMS_N"><span></span>동의안함</label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="tit_inner">이메일 수신</span>
                                                <span class="radio_wrap">
                                                    <input type="radio" value="Y"  name="M_EMS_YN" id="M_EMS_Y">
                                                    <label for="M_EMS_Y"><span></span>동의함</label>
                                                </span>
                                                <span class="radio_wrap">
                                                    <input type="radio" value="N" checked="checked"  name="M_EMS_YN"  id="M_EMS_N">
                                                    <label for="M_EMS_N"><span></span>동의안함</label>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                        
                        <section>
                            <h4>아이정보
                                <p class="infotxt"><strong>(선택사항)</strong> - 아이정보 입력시, 한 명당 <span>500포인트</span>를 드립니다. 정보는 가입 후에도 수정 가능합니다.</p>
                            </h4>
                            <div class="tb">
                                <table>
                                    <colgroup>
                                        <col width="200">
                                        <col width="430">
                                        <col width="150">
                                        <col width="*">
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="line">이름(태명)</th>
                                            <th class="line">생년월일(출산예정일)</th>
                                            <th class="line">성별</th>
                                            <th>추가/삭제</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fnlist">
                                        <tr class="fnli hiddenform">
                                            <td class="line">
                                                <input type="text" name="ch_name[]" class="ipt_box">
                                            </td>
                                            <td class="line">
                                                <div class="select ipt_box sz_sm">
                                                    <span>2018년</span>
                                                    <select class="" name="ch_year[]">
                                                        <?php echo $TPL_VAR["year_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <div class="select ipt_box sz_sm">
                                                    <span>1월</span>
                                                    <select class="" name="ch_month[]">
                                                        <?php echo $TPL_VAR["month_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <div class="select ipt_box sz_sm">
                                                    <span>1일</span>
                                                    <select class="" name="ch_day[]">
                                                        <?php echo $TPL_VAR["day_list"]?>

                                                    </select>
                                                </div>
                                            </td>
                                            <td class="line">
                                                <div class="select ipt_box sz_sm">
                                                    <span>성별</span>
                                                    <select class="" name="ch_gender[]">
                                                        <option value="">성별</option>
                                                        <option value="F">여</option>
                                                        <option value="M">남</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn_add">
                                                    <a href="#" class="btn_plus" onclick="fnlist.add(this);return false;"><span></span></a>
                                                    <a href="#" class="btn_minus" onclick="fnlist.del(this);return false;"><span></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fnli">
                                            <td class="line">
                                                <input type="text" name="ch_name[]" class="ipt_box">
                                            </td>
                                            <td class="line">
                                                <div class="select ipt_box sz_sm">
                                                    <span>2018년</span>
                                                    <select class="" name="ch_year[]">
                                                        <?php echo $TPL_VAR["year_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <div class="select ipt_box sz_sm">
                                                    <span>1월</span>
                                                    <select class="" name="ch_month[]">
                                                        <?php echo $TPL_VAR["month_list"]?>

                                                    </select>
                                                </div>
                                                <span class="unit dash"></span>
                                                <div class="select ipt_box sz_sm">
                                                    <span>1일</span>
                                                    <select class="" name="ch_day[]">
                                                        <?php echo $TPL_VAR["day_list"]?>

                                                    </select>
                                                </div>
                                            </td>
                                            <td class="line">
                                                <div class="select ipt_box sz_sm">
                                                    <span>성별</span>
                                                    <select class="" name="ch_gender[]">
                                                        <option value="">성별</option>
                                                        <option value="F">여</option>
                                                        <option value="M">남</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="btn_add">
                                                    <a href="#" class="btn_plus" onclick="fnlist.add(this);return false;"><span></span></a>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>

                                <script type="text/javascript">
                                    var fnlist = {
                                        add:function(obj){
                                        	var count=$('.fnlist tr').length;
                                        	if(count<4){
	                                            var $obj = $(obj).closest(".fnlist");
	                                            var $li = $(obj).closest(".fnli");
	                                            var $tg = $obj.find(".hiddenform").clone().removeClass("hiddenform");
	                                            $li.after("<tr class='fnli'>"+$tg.html()+"</tr>");
                                           }
                                           selAct();
                                        },
                                        del:function(obj){
                                            var $li = $(obj).closest(".fnli");
                                            $li.remove();
                                        }
                                    }
                                </script>
                            </div>
                        </section>

                        <section>
                            <h4>추가정보
                                <p class="infotxt"><strong>(선택사항)</strong></p>
                            </h4>
                            <div class="tb">
                                <table>
                                    <colgroup>
                                        <col width="200">
                                        <col width="*">
                                    </colgroup>
                                    <tbody>
                                        <tr class="">
                                            <th rowspan="2" class="rowtop">가입경로</th>
                                            <td class="line_dash join_root">
                                                <p class="txt_info">가입하시게 된 경로를 선택해주신 후에 간단한 메모를 남겨주시면 됩니다. (예 지인소개 : 이웃집 엄마를 통해서 알게 되었어요)</p>
                                                <div>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO01" id="M_ENTRYOBJECT1">
                                                        <label for="M_ENTRYOBJECT1"><span></span>지인소개</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO02" id="M_ENTRYOBJECT2">
                                                        <label for="M_ENTRYOBJECT2"><span></span>TV광고</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO03" id="M_ENTRYOBJECT3">
                                                        <label for="M_ENTRYOBJECT3"><span></span>검색어</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO04" id="M_ENTRYOBJECT4">
                                                        <label for="M_ENTRYOBJECT4"><span></span>전단지, 홍보물</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO05" id="M_ENTRYOBJECT5">
                                                        <label for="M_ENTRYOBJECT5"><span></span>인터넷 커뮤니티</label>
                                                    </span>
                                                    <br>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO06" id="M_ENTRYOBJECT6">
                                                        <label for="M_ENTRYOBJECT6"><span></span>지하철광고</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO07" id="M_ENTRYOBJECT7">
                                                        <label for="M_ENTRYOBJECT7"><span></span>블로그</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO08" id="M_ENTRYOBJECT8">
                                                        <label for="M_ENTRYOBJECT8"><span></span>SNS</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO09" id="M_ENTRYOBJECT9">
                                                        <label for="M_ENTRYOBJECT9"><span></span>포털사이트</label>
                                                    </span>
                                                    <span class="radio_wrap">
                                                        <input type="radio" name="M_ENTRYOBJECT" value="EO10" id="M_ENTRYOBJECT10">
                                                        <label for="M_ENTRYOBJECT10"><span></span>기타</label>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="join_because">
                                                <span class="tit_inner">이유:</span>
                                                <input type="text" name="M_ENTRYOBJECT_TXT" id="M_ENTRYOBJECT_TXT" class="ipt_box" placeholder="어떤 검색어로 방문했는지 또는 위 내용 중 좀더 구체적인 글을 남기시면 서비스개선에 도움이 될 것 같습니다.">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>지인추천</th>
                                            <td class="td_M_RECOMMEND">
                                                <input type="text" name="M_RECOMMEND" id="M_RECOMMEND" class="ipt_box sz_lg" placeholder="추천인 ID를 입력해주세요." onfocusout="chk_id('S')">
                                                <span class="txt_chk msg_M_RECOMMEND"></span>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </section>

                        <div class="btn_wrap">
                            <a href="#" onclick="frm_input();" role="button" class="btn btn_next"><span>가입하기</span></a>
                            <a href="#" role="button" class="btn btn_round"><span>취소</span></a>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
        </section>
        
    </article>
    
    <script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
	<script>
	function get_post(){
	    new daum.Postcode({
	        oncomplete: function(data) {
	            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분입니다.
	            // 예제를 참고하여 다양한 활용법을 확인해 보세요.
	            $("#M_POST").val(data.zonecode);
	            $("#M_ADDR1").val(data.address);
	        }
	    }).open();
	    }
	</script>
    <script>

	</script>
	
<?php $this->print_("layout_footer",$TPL_SCP,1);?>