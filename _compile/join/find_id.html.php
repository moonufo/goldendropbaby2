<?php /* Template_ 2.2.7 2018/03/09 16:55:11 /home/dartz/public_html/application/views/join/find_id.html 000005601 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

    <script src="/assets/js/id_pw.js"></script>
<article id="contents">
	<section id="join">
            <div class="container">
                <div class="join_form_wrap">
                    <div class="join_top">
                        <h3>아이디 찾기</h3>
                        <p class="txt">가입 시 입력한 정보로 아이디를 찾을 수 있습니다.</p>
                    </div>
                </div>

                <div class="find_certify">
                    <div class="find_certify_box form only">
                        <!-- <h4>회원 정보로 찾기</h4> -->
                        <div class="ipt_wrap on">
                        	<input type="hidden" id="hp_chk" value="N"/>
                            <input type="text" name="username" id="username" class="ipt_box" placeholder="이름">
                            <input type="tel" onkeydown="onlyNumber(this);" onKeyUp="onlyNumber(this);" name="userhp" id="userhp" class="ipt_box ipt_left" placeholder="휴대전화"><a href="#" onclick="sms_send('find_id');return false;" role="button" class="btn_inner line_onoff on ">인증번호 받기</a>
                            <div class="" id="chk_red"></div><input type="text" name="chknum" id="chknum" class="ipt_box ipt_left" placeholder="인증번호"><a href="#" onclick="sms_chknum();return false;" role="button" class="btn_inner">인증 확인</a>
                        </div>
                        <a href="#" onclick="chk_hp('id');return false;" class="button btn_solid"><span>아이디 확인</span></a>
                    </div>
                    <!-- <div class="find_certify_box">
                        <h4>아이핀(I-PIN)으로 찾기</h4>
                        <div class="img">
                            <img src="/images/join/ico_ipin.png" alt="아이핀">
                        </div>
                        <a href="javascript:fnPopup_ipin();" class="button btn_solid "><span>아이디 찾기</span></a>
                    </div>
                    <div class="find_certify_box">
                        <h4>휴대폰 번호로 찾기</h4>
                        <div class="img">
                            <img src="/images/join/ico_phone.png" alt="아이핀">
                        </div>
                        <a href="javascript:fnPopup_chk();" class="button btn_solid"><span>아이디 찾기</span></a>
                    </div> -->
                </div>

                <p class="find_info" style="display: none;"></p>
            </div>
        </section>
</article>
    
    
       <form name="form_chk" method="post">
		<input type="hidden" name="m" value="checkplusSerivce">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
		<input type="hidden" name="EncodeData" value="<?php echo $TPL_VAR["enc_data"]?>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
		<input type="hidden" name="param_r1" value='Y'>	
	    
	</form>
	<form name="form_ipin" method="post">
		<input type="hidden" name="m" value="pubmain">						<!-- 필수 데이타로, 누락하시면 안됩니다. -->
	    <input type="hidden" name="enc_data" value="<?php echo $TPL_VAR["sEncData"]?>">		<!-- 위에서 업체정보를 암호화 한 데이타입니다. -->
		<input type="hidden" name="param_r1" value="Y">
	</form>
	
    <form name="vnoform" action="/join/find_id_chk" method="post">
		<input type="hidden" name="UserName" value="">
	    <input type="hidden" name="SexFlag" value="">
	    <input type="hidden" name="BirthDay" value="">
	    <input type="hidden" name="CertKey" id="CertKey" value="">
	    <input type="hidden" name="Mobileno" value="">
	    <input type="hidden" name="ret_url" id="ret_url" value="<?php echo $TPL_VAR["ret_url"]?>">
	</form>
	<input type="hidden" id="textyn" value="N" />
    <script>
    function chk(){
    	var CertKey=$("#CertKey").val();
    	if(CertKey!=""){
			$.post("/join/find_id_chk" ,
		    {
		        CertKey : CertKey
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="false"){
					$(".find_info").css("display","block");
					$(".find_info").html("회원님의 아이디를 찾을 수 없습니다.<br/>이름과 휴대전화를 재확인해주시기 바랍니다.");
				}else{
			    	$(".find_info").css("display","block");
		    		$(".find_info").html("회원님의 아이디는 <span class='getid'>"+rslt+"</span> 입니다.");
		    		
			    }
		    });
	    }
	}

	
	window.name ="Parent_window";
	function fnPopup_ipin(){
		
		
	
		window.open('', 'popupIPIN2', 'width=450, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		
		document.form_ipin.target = "popupIPIN2";
		document.form_ipin.action = "https://cert.vno.co.kr/ipin.cb";
		document.form_ipin.submit();
	}
	
	function fnPopup_chk(){
	
		window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
		
		document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
		document.form_chk.target = "popupChk";
		document.form_chk.submit();
	}
	</script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>