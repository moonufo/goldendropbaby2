<?php /* Template_ 2.2.7 2017/12/26 13:53:48 /home/dartz/public_html/application/views/join/join_end.html 000001045 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">

        <section id="join">
            <div class="container">
                <div class="complete">
                    <h4>회원 가입이 완료 되었습니다.</h4>
                    <hr>
                    <div class="txt">
                       이제부터 일루마의 다양한 서비스와 이벤트를 이용할 수 있습니다.<br><br>
                       <strong>감사합니다.</strong>
                    </div>
                    <hr>
                    <div class="btn_wrap">
                        <a href="/" class="btn btn_next"><span>메인으로</span></a>    
                    </div>
                </div>
            </div>
        </section>
        
    </article>
    
    
<?php $this->print_("layout_footer",$TPL_SCP,1);?>