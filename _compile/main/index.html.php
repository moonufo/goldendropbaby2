<?php /* Template_ 2.2.7 2018/03/12 10:02:31 /home/dartz/public_html/application/views/main/index.html 000014060 */ 
$TPL_visual_1=empty($TPL_VAR["visual"])||!is_array($TPL_VAR["visual"])?0:count($TPL_VAR["visual"]);
$TPL_popup_1=empty($TPL_VAR["popup"])||!is_array($TPL_VAR["popup"])?0:count($TPL_VAR["popup"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

    <article id="contents" class="mainpage">
        <div class="main_slide">

            <!--<div class="slider">-->
                <!--<div class="main_sld_video">-->
                    <!--<a href="#" class="btn_video" loof>-->
                        <!--<video id="myVideo">-->
                            <!--<source src="/video/ILLUMA2016.mp4" type="video/mp4">-->
                             <!--Your browser does not support the video tag.-->
                        <!--</video>-->
                    <!--</a>-->
                <!--</div>-->
                <!--<div class="sld_standard"><img src="/images/main/main_slide_banner.png" alt=""></div>-->
            <!--</div>-->
            <div class="slider">
<?php if($TPL_visual_1){foreach($TPL_VAR["visual"] as $TPL_V1){?>
<?php if($TPL_V1["MV_GUBUN"]=="I"){?>
<?php if($TPL_V1["MV_IMG_URL"]!=""){?>
                            <div class="sld_standard"><a href="<?php echo $TPL_V1["MV_IMG_URL"]?>" target="win_1" ><img src="http://dartz.swtown.co.kr<?php echo $TPL_V1["MV_PATH"]?><?php echo $TPL_V1["MV_FILENAME"]?>" alt=""></a></div>
<?php }else{?>
                            <div class="sld_standard"><img src="http://dartz.swtown.co.kr<?php echo $TPL_V1["MV_PATH"]?><?php echo $TPL_V1["MV_FILENAME"]?>" alt=""></div>
<?php }?>

<?php }else{?>
                        <div class="main_sld_video">
                            <iframe width="100%" height="100%" src="<?php echo $TPL_V1["MV_MOVIE_URL"]?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
<?php }?>
<?php }}?>
            </div>
            <div class="controls pager">
                <a href="#" class="btn_play stop"></a>
                <a href="#" onclick="mainsld.goToSlide(0);return false;" class="btn_num on"></a>
                
            </div>
        </div>
        <script language="JavaScript" type="text/javascript">
            var mainsld;
            $(function(){
                setAlign();
                mainsld = $('.slider').bxSlider({
                    controls:false,
                    auto:true,
                    autoHover:true,
                    pager:false,
                    infiniteLoop:false,
                    onSlideAfter:function(){
                        pagerfn(mainsld.getCurrentSlide());
                    }
                });
                setPage(mainsld.getSlideCount());
                $(".btn_play").on("click",function(){
                    if($(this).hasClass("stop")){
                        mainsld.stopAuto();
                    }else{
                        mainsld.startAuto();
                    }
                    $(this).toggleClass('stop');
                })
                $(".btn_video").on("click",function(){
                    var $video = $(this).find("video").attr("id");
                    var myVideo = document.getElementById($video); 
                    if($(this).hasClass("on")){
                        myVideo.pause();
                        mainsld.startAuto();
                        $(".btn_play").addClass("stop");
                    }else{
                        myVideo.play();
                        mainsld.stopAuto();
                        $(".btn_play").removeClass("stop");
                    }
                    $(this).toggleClass('on');
                })
            });

            function setPage(total){
                var $pager = $(".pager");
                for(var i=1;i<total;i++){
                    var txt = "<a href='#' onclick='mainsld.goToSlide("+i+");return false;' class='btn_num'></a>";
                    $pager.append(txt)
                }
            }

            function setAlign(){
                var videoheight = 780;
                if($(".sld_standard").length>0)var videoheight = $(".sld_standard").height();
                $(".main_sld_video").css({'height':videoheight})
            }

            $(window).resize(function(){
                mainsld.redrawSlider();
                setAlign();
            })


            function pagerfn(num){
                var num = num;
                $(".controls .btn_num.on").removeClass("on");
                $(".controls .btn_num:eq("+num+")").addClass("on");
            }

            function getCookie(name){
                var nameOfCookie = name + "=";
                var x =0;
                while (x<=document.cookie.length){
                    var y = (x+nameOfCookie.length);
                    if(document.cookie.substring(x,y) == nameOfCookie){
                        if((endOfCookie=document.cookie.indexOf(";",y))==-1)
                            endOfCookie = document.cookie.length;
                        return unescape(document.cookie.substring(y, endOfCookie));
                    }
                    x=document.cookie.indexOf(" ",x) +1;
                    if(x==0)
                        break;
                }
                return "";
            }


            function setCookie(name, value, expiredays){
                var todayDate = new Date();
                todayDate.setDate(todayDate.getDate() + expiredays);
                document.cookie=name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
            }

            function pop_close() {

                if($("input:checkbox[name='popup_chk']").is(":checked")){
                    setCookie("layer_pop", "done" , 1000);
                }

                $("#layer_pop").hide();
            }

            function closeWin(mp_idx) {
                var _win = "popup_" + mp_idx;
                var form_idx = "pop_form" + mp_idx;
                var notice_idx = "notice_" + mp_idx;

                if ($("#"+notice_idx).prop("checked") == "true"){
                    setpopCookie(_win, "done"); // 오늘 하루 보지 않기
                }

                $("#popup_" + mp_idx).hide();

            }

            function setpopCookie(name, value) {
                todayDate.setHours(23);
                todayDate.setMinutes(59);
                todayDate.setSeconds(59);
                document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
            }

        </script>
        <section class="main_banner">
            <div class="container">
                <div class="banner ireland">
                    <a href="/story/product_lreland" class="button btn_wh"><span>자세히 보기</span></a>
                </div>
                <div class="banner opo">
                    <a href="/story/product_opo" class="button btn_wh"><span>자세히 보기</span></a>
                </div>
            </div>
        </section>

        <section class="main_join">
            <div class="container">
                <div class="join_wrap">
                    <div class="prd"><img src="/images/main/prd.png" alt=""></div>
                    <div class="box">
                        <h3><img src="/images/main/tit_join.png" alt="회원가입"></h3>
                        <p>프리미엄 일루마의 친구가 되어 특별한 혜택을 누려보세요</p>
                        <div class="box_bot">
                            <div class="join_event">
                                <p>회원가입 이벤트</p>
                                <a href="/join/" class="button"><span>가입하기</span></a>
                            </div>
                            <div class="join_point">
                                <p>회원가입 포인트</p>
                                <a href="/point/index" class="button"><span>포인트 알아보기</span></a>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </section>

        <section class="main_instagram">
            <div class="container">
                <h3><img src="/images/main/tit_insta.png" alt="인스타그램"></h3>
                <p class="txt">INSTAGRAM</p>
                <div class="sns_wrap">
                    <ul id="sns_wrap" name="sns_wrap">

                    </ul>
                </div>
            </div>
        </section>
        
    </article>
<input type="hidden" id="instaPics" name="instaPics" value="">
<!-- 고정 레이어 팝업-->
<div class="modal modal_info"  id="layer_pop" name="layer_pop" style="display: none;">
    <div class="modal_wrap">
        <div class="modal_txt">
            <p class="tit">모유가 아기에게<br>가장 좋은 식품입니다.</p>
            <p class="txt">
                세계보건기구(WHO)는<br>
                첫 6개월 동안 완모수유를 권장합니다.<br><br>
                자사 또한 지속적인 모유수유를 권장하며,<br>
                주치의 또는 보건당국의 안내에 따라<br>
                이유식 시작을 권고합니다.
            </p>
            <a href="#" onclick="javascript:pop_close();"><img src="/images/main/btn_modal_close.png" alt="닫기"></a>
            <div class="chk_wrap">
                <label>
                    <input type="checkbox" name="popup_chk" id="popup_chk"> 다시 보지 않기
                </label>
            </div>
        </div>
    </div>
</div>
<!-- 고정 레이어 팝업-->

<!-- 일반 다중 팝업-->
<?php if($TPL_popup_1){foreach($TPL_VAR["popup"] as $TPL_V1){?>
<?php if($TPL_V1["MP_IDX"]!=""){?>
        <!--<script type='text/javascript' language='javascript'>-->
            <!--if (getCookie('popup_<?php echo $TPL_V1["MP_IDX"]?>') != 'done') {-->
                <!--window.open('/main/popup?mp_idx=<?php echo $TPL_V1["MP_IDX"]?>','popup_<?php echo $TPL_V1["MP_IDX"]?>', 'width=<?php echo $TPL_V1["MP_WIDTH"]?>,height=<?php echo $TPL_V1["mp_h"]?>,left=<?php echo $TPL_V1["MP_LEFT"]?>,top=<?php echo $TPL_V1["MP_TOP"]?>, scrollbars=no');-->
            <!--}-->
        <!--</script>-->

<div class='modal_pop modal_info_pop'  id='popup_<?php echo $TPL_V1["MP_IDX"]?>' name='popup_<?php echo $TPL_V1["MP_IDX"]?>' style=' width: <?php echo $TPL_V1["MP_WIDTH"]?>px; height:<?php echo $TPL_V1["mp_h"]?>px; left: <?php echo $TPL_V1["MP_LEFT"]?>px; top: <?php echo $TPL_V1["MP_TOP"]?>px; '>

</div>
    <script type='text/javascript' language='javascript'>

        if (getCookie('popup_<?php echo $TPL_V1["MP_IDX"]?>') == 'done') {
            $("#popup_<?php echo $TPL_V1["MP_IDX"]?>").hide();

        }else{
            var temp_idx = <?php echo $TPL_V1["MP_IDX"]?>;
            $.get("/main/popup/" ,
                {mp_idx : temp_idx},
                function(rslt2){
                    var array_rslt2 = JSON.parse(rslt2);
                    $("#popup_<?php echo $TPL_V1["MP_IDX"]?>").html(array_rslt2.real_txt);
                });
        }

    </script>

<?php }?>
<?php }}?>

<!-- 일반 다중 팝업-->
<script type="text/javascript">
    $(function(){
        modal.open('info');
        modal.open('info_pop');
        if(getCookie('layer_pop') != 'done'){
            $("#layer_pop").show();
        }else{
            $("#layer_pop").hide();
        }

        sbi_init();
    })


    function sbi_init() {
        jQuery(function ($) {
            var tocken = "1422047775.98a9970.9676e68d23dd45c083ce1ecb42d976a8"; /* Access Tocken 입력 */
            var count = "10";
            $.ajax({
                type: "GET",
                dataType: "jsonp",
                cache: false,
                url: "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + tocken + "&count=" + count,
                success: function (response) {
                    if (response.data.length > 0) {
                        for(var i = 0; i < 10; i++) {
                            var li_css = "";
                            if((i+1)%5 == 0){
                                li_css = "right";
                            }

                            var insta = "<li class='" + li_css + "'>";
                                    insta += "<a target='_win_1' href='" + response.data[i].link + "'>";
                                        insta += '<img src="' + response.data[i].images.standard_resolution.url + '">';
                                            if ( response.data[i].caption !== null ) {
                                                insta += "<div class='caption-layer'>";
                                                if ( response.data[i].caption.text.length > 0 ) {
                                                    insta += "<p class='insta-caption'>" + response.data[i].caption.text + "</p>"
                                                }
                                                    insta += "<span class='insta-likes'>" + response.data[i].likes.count + " Likes</span>";
                                                insta += "</div>";
                                            }
                                    insta += "</a>";
                                insta += "</li>";

                            $("#sns_wrap").append(insta);
                        }
                    }
                }
            });
        });
    }


</script>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>