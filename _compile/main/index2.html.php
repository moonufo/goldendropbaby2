<?php /* Template_ 2.2.7 2018/02/26 11:25:27 /home/dartz/public_html/application/views/main/index2.html 000003198 */ 
$TPL_instar_1=empty($TPL_VAR["instar"])||!is_array($TPL_VAR["instar"])?0:count($TPL_VAR["instar"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

    <article id="contents" class="mainpage">
        <div class="main_visual">
            <div class="video">
                <a href="#" class="btn_video"></a>
            </div>
        </div>
       
        <section class="main_banner">
            <div class="container">
                <div class="banner ireland">
                    <a href="/story/product_lreland/" class="button btn_wh"><span>자세히 보기</span></a>
                </div>
                <div class="banner opo">
                    <a href="/story/product_opo/" class="button btn_wh"><span>자세히 보기</span></a>
                </div>
            </div>
        </section>

        <section class="main_join">
            <div class="container">
                <div class="join_wrap">
                    <div class="prd"><img src="/images/main/prd.png" alt=""></div>
                    <div class="box">
                        <h3><img src="/images/main/tit_join.png" alt="회원가입"></h3>
                        <p>프리미엄 일루마의 친구가 되어 특별한 혜택을 누려보세요</p>
                        <div class="box_bot">
                            <div class="join_event">
                                <p>회원가입 이벤트</p>
                                <a href="/join/" class="button"><span>가입하기</span></a>
                            </div>
                            <div class="join_point">
                                <p>회원가입 포인트</p>
                                <a href="/event/point_index/" class="button"><span>포인트 알아보기</span></a>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </section>

        <section class="main_instagram">
            <div class="container">
                <h3><img src="/images/main/tit_insta.png" alt="인스타그랩"></h3>
                <p class="txt">INSTAGRAM</p>
                <div class="sns_wrap">
                    <ul>
<?php if($TPL_instar_1){$TPL_I1=-1;foreach($TPL_VAR["instar"] as $TPL_V1){$TPL_I1++;?>
                        <li <?php if((($TPL_I1+ 1)% 5)== 0){?>class="right"<?php }?> style=""><a href="https://www.instagram.com/p/<?php echo $TPL_V1["ID_URL"]?>" target="win_1"><img src="<?php echo $TPL_V1["ID_IMG_SRC"]?>" alt=""></a></li>
<?php }}?>
                    </ul>
                </div>
                <div class="btn_wrap">
                    <a href="#" class="btn btn_round"><span>MORE</span></a>
                </div>
            </div>
        </section>
        
    </article>
<script type="text/javascript">
    $(function(){
        modal.open('info');
    })
</script>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>