<?php /* Template_ 2.2.7 2018/03/09 16:38:28 /home/dartz/public_html/application/views/main/popup.html 000001970 */ ?>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <title><?php echo $TPL_VAR["MP_TITLE"]?></title>
    <style type="text/css">
        html{ overflow:hidden;margin:0;padding:0;}
        body{padding:0;margin:0;}
        a, img{border:none;padding:0;margin:0;}
        label{font-size:15px;color:#333;}
        .pop_bot_right{float:right;width:60px;height:40px;display:block;}
    </style>
</head>
<script type="text/javascript">
    function setCookie(name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
    }

    function closeWin() {
        var _win = "popup_<?php echo $TPL_VAR["MP_IDX"]?>";
        if (document.form1.notice.checked)
            setCookie(_win, "done", 1000); // 다시보지 않기
        self.close();
    }

</script>
<form name="form1">

    <div class="modal modal_info" >
        <div class="modal_wrap">
            <div class="modal_txt">
                <p class="txt">
                    <?php echo $TPL_VAR["MP_CONTENT"]?>

                </p>
                <a href="#" onclick="javascript:closeWin();"><img src="/images/main/btn_modal_close.png" alt="닫기"></a>
                <div class="chk_wrap">
                    <label>
                        <input type="checkbox" name="notice" id="notice" value="1" id="c" > 다시 보지 않기
                    </label>
                </div>
            </div>
        </div>
    </div>


</form>