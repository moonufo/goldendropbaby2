<?php /* Template_ 2.2.7 2018/03/06 15:22:35 /home/dartz/public_html/application/views/my/index_chk.html 000002157 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4>비밀번호 확인</h4>
                </div>
                <form id="frm_input" name="frm_input" action="/my/index_process" method="post">
                	<input type="hidden" name="chk" value="<?php echo $TPL_VAR["chk"]?>">
                <div class="page_body join_form">
                    <section>
                        <div class="tb tb_view">
                            <table>
                                <colgroup>
                                    <col width="200px">
                                    <col width="*">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th><label>현재 비밀번호</label></th>
                                        <td>
                                            <input type="password" name="pwori" id="pwori"  class="ipt_box sz_lg">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
 
                        </div>
                    </section>

                    <div class="btn_wrap">
                        <a href="#" onclick="index_chk();return false;"  role="button" class="btn btn_next"><span>확인</span></a>
                    </div>

                </div>
                </form>
            </div>
        </section>
    </article>
    
    <script>
	function index_chk(){
		if($.trim($('#pwori').val()) == ''){
			alert("비밀번호를 입력해주세요");
			return;
		}
		$("#frm_input").submit();
		
	}
	
    </script>
    
<?php $this->print_("layout_footer",$TPL_SCP,1);?>