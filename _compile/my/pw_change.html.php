<?php /* Template_ 2.2.7 2018/03/05 17:36:00 /home/dartz/public_html/application/views/my/pw_change.html 000004815 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4>비밀번호 변경</h4>
                    <p>※ 비밀번호는 영문(대,소), 숫자, 특수문자 사용가능 : <br>
                        <span class="txt_caution" style="padding-left:20px;">영문,숫자,특수문자(.!@#$%)를 조합한 8자~20자로 사용하실 수 있습니다.</span>
                    </p>
                </div>
                <div class="page_body join_form">
                    <section>
                        <div class="tb tb_view">
                            <table>
                                <colgroup>
                                    <col width="200px">
                                    <col width="*">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th><label>현재 비밀번호</label></th>
                                        <td>
                                            <input type="password" name="M_PWORI" id="M_PWORI"  class="ipt_box sz_lg">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><label>새 비밀번호</label></th>
                                        <td>
                                            <input type="password" name="M_PW" id="M_PW"  class="ipt_box sz_lg">
                                        </td>
                                    </tr>
                                     <tr>
                                        <th><label>비밀번호 확인</label></th>
                                        <td>
                                            <input type="password" name="M_PWCHK" id="M_PWCHK"  class="ipt_box sz_lg">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
 
                        </div>
                    </section>

                    <div class="btn_wrap">
                        <a href="#" onclick="pw_frm_input();return false;"  role="button" class="btn btn_next"><span>저장</span></a>
                        <a href="#" role="button" class="btn btn_round"><span>취소</span></a>
                    </div>

                
                </div>
            </div>
        </section>
    </article>
    
    <script>
	function chkPwd(pwd) {
		var pw = pwd;
		var num = pw.search(/[0-9]/g);
		var eng = pw.search(/[a-z]/ig);
		var spe = pw.search(/[.!@#$%]/gi);
		var nospe = pw.search(/[`~^&*|\\\'\";:\/?]/gi);

	
		if (pw.length < 8 || pw.length > 20) {
			
			alert("새로운 비밀번호의 조합이 맞지 않습니다.\n다시 입력해주시기 바랍니다.");
			$(".td_M_PW").addClass("caution");
			return;
		}
		if (pw.search(/₩s/) != -1) {
			
			alert("새로운 비밀번호의 조합이 맞지 않습니다.\n다시 입력해주시기 바랍니다.");
			
			return;
		}
		if (num < 0 || eng < 0 || spe < 0 || nospe >-1) {
			
			alert("새로운 비밀번호의 조합이 맞지 않습니다.\n다시 입력해주시기 바랍니다.");
			
			return;
		}
		return true;
	}
	function pw_frm_input(){
		var chk="Y";
		if($.trim($('#M_PW').val()) == ''){
			alert("비밀번호를 입력해주세요");
			return;
			chk="N";
		}else if (!chkPwd($.trim($('#M_PW').val()))) {
			chk="N";
			return;
		}
		
		if($.trim($('#M_PWCHK').val()) == ''){
			alert("비밀번호를 한번 더 입력해주세요");
			return;
			chk="N";
		}else if($('#M_PW').val()!=$('#M_PWCHK').val()){
			alert("입력하신 비밀번호가 동일하지 않습니다.");
			return;
			chk="N";
		}
		if(chk=="Y"){
			var pwori=$("#M_PWORI").val();
			var pw=$("#M_PW").val();
			$.get("/my/pw_chk" ,
		    {
		        pw : pw,
		        pwori:pwori
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt!="N"){
		    		alert("새로운 비밀번호로 변경되었습니다.");
		    		//location.reload(); 
					window.location.href="/my/";
		    	}else{
		    		alert("현재 비밀번호가 다릅니다.\n확인 후 이용해주시기 바랍니다.");
		    	}
		    });
		
		}
	}
	
    </script>
    
<?php $this->print_("layout_footer",$TPL_SCP,1);?>