<?php /* Template_ 2.2.7 2018/01/04 18:29:00 /home/dartz/public_html/application/views/my/my_orderlist.html 000007169 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
    <script src="//code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>  
        <link rel="stylesheet" type="text/css" href="/assets/css/reset.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/assets/css/illuma.css" media="all" />
    <script src="/assets/js/order_index.js" ></script>  
<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4 style="margin-bottom:5px;">주문내역</h4>
                </div>
                <div class="page_body">
                    <section class="order_list">
                        <h5 style="margin-top:0;">기간별 조회
                            <p class="infotxt">조회일자를 설정하시면 최대 6개월 간의 내역을 조회하실 수 있습니다.</p>
                        </h5>
                        <div class="search_date" id="search_date" name="search_date">
                            <a href="#" class="btn"><span>1개월</span></a>
                            <a href="#" class="btn"><span>3개월</span></a>
                            <a href="#" class="btn"><span>6개월</span></a>
                            <a href="#" class="btn"><span>12개월</span></a>
                            <a href="#" class="btn"><span>24개월</span></a>
                            <script type="text/javascript">
                                $(function(){
                                    var $rdobtn = $(".search_date .btn");
                                    $rdobtn.on("click",function(){
                                        if(!$(this).hasClass("on")){
                                            $rdobtn.removeClass("on");
                                            $(this).addClass("on");
                                        }
                                    })
                                })
                            </script>
                            <div class="cal">
                                <input type="text" name="" class="ipt_box sz_md ipt_cal" id="calendar">
                                <script type="text/javascript">
                                    $(function() {
                                        $( "#calendar" ).datepicker({
                                            dateFormat:"yy-mm-dd",
                                            monthNames:["1","2","3","4","5","6","7","8","9","10","11","12"],
                                            dayNames:["일요일","월요일","화요일","수요일","목요일","금요일","토요일"],
                                            dayNamesMin: [ "일", "월", "화", "수", "목", "금", "토" ] ,
                                            nextText: '다음 달',
                                            prevText: '이전 달',
                                            maxDate: 0,
                                            showOtherMonths : true
                                        });
                                    });
                                </script>
                                <a href="#" class="btn_srch"><span>직접조회</span></a>
                            </div>
                            
                        </div>
                    </section>

                    <section class="order_list">
                        <h5>주문 현황</h5>
                        <div class="tb tb_sm">
                            <table>
                                <colgroup>
                                    <col width="90">
                                    <col width="70">
                                    <col width="80">
                                    <col width="120">
                                    <col width="40">
                                    <col width="40">
                                    <col width="40">
                                    <col width="80">
                                    <col width="80">
                                    <col width="80">
                                    <col width="240">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>구분</th>
                                        <th>주문번호</th>
                                        <th>주문일</th>
                                        <th>상품명</th>
                                        <th>수량</th>
                                        <th>주기</th>
                                        <th>회수</th>
                                        <th>결제일</th>
                                        <th>결제금액</th>
                                        <th>적립포인트</th>
                                        <th>상태</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
                                    <tr>
                                        <td><?php if($TPL_V1["OM_TYPE"]=="T"){?>트라이얼<?php }else{?>정기배송<?php }?></td>
                                        <td><?php echo $TPL_V1["OM_ORDERCODE"]?></td>
                                        <td><?php echo substr($TPL_V1["OM_CREDATE"], 0, 10)?></td>
                                        <td><?php echo $TPL_V1["PD_PRODUCTNAME"]?></td>
                                        <td><?php if($TPL_V1["OM_TYPE"]=="T"){?>1<?php }else{?>3<?php }?></td>
                                        <td><?php echo $TPL_V1["OM_SENDCYCLE"]?></td>
                                        <td><?php echo $TPL_V1["OM_COUNT"]?></td>
                                        <td><?php echo substr($TPL_V1["OM_CREDATE"], 0, 10)?></td>
                                        <td><?php echo number_format($TPL_V1["OM_CHARGE"])?></td>
                                        <td>0</td>
                                        <td><span class="status">배송완료</span><a href="#" class="btn_inner"><span>상세보기</span></a></td>
                                    </tr>
<?php }}?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>