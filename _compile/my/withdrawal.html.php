<?php /* Template_ 2.2.7 2018/03/03 12:37:35 /home/dartz/public_html/application/views/my/withdrawal.html 000003885 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4>회원탈퇴</h4>
                    <p>그 동안 일루마 홈페이지에 관심을 가져주셔서 감사 드립니다. <br>
                    보다 나은 홈페이지가 되도록 노력하겠습니다.
                    </p>
                    <p class="txt_sm">※ 회원탈퇴 시 그 동안 보유했던 포인트는 모두 소멸되며 회원님의 개인정보는 모두 삭제됩니다. <br>
                        <span>또한 해당 아이디로는 180일 동안 재가입이 불가능합니다.</span></p>
                </div>
				<form name="frm_input" id="frm_input" action="withdrawal_process" method="post">
                <div class="page_body join_form">
                    <section>
                        <div class="tb tb_view">
                            <table>
                                <colgroup>
                                    <col width="280px">
                                    <col width="*">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <th><label>아이디</label></th>
                                        <td>
                                            <p class="invalue"><?php echo $TPL_VAR["M_ACCOUNT"]?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><label>성명</label></th>
                                        <td>
                                            <p class="invalue"><?php echo $TPL_VAR["M_NAME"]?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><label>비밀번호</label></th>
                                        <td>
                                            <input type="password" name="M_PW" id="M_PW" class="ipt_box sz_lg">
                                            <span class="txt_caution">* 비밀번호 확인 후 정식탈퇴요청을 진행합니다. </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th><label>남은 포인트</label></th>
                                        <td>
                                            <?php echo number_format($TPL_VAR["pldata"]['PL_TOTALPOINT'])?>P
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
 
                        </div>
                    </section>

                    <div class="btn_wrap">
                        <a href="#" onclick="frm_out();return false;" role="button" class="btn btn_next"><span>탈퇴하기</span></a>
                        <a href="#" role="button" class="btn btn_round"><span>취소</span></a>
                    </div>

                
                </div>
                </form>
            </div>
        </section>
    </article>
    
    <script>
	function frm_out(){
		if($.trim($('#M_PW').val()) == ''){
			alert("비밀번호를 입력해주세요");
			return;
		}
		$("#frm_input").submit();
	}
    </script>
    
<?php $this->print_("layout_footer",$TPL_SCP,1);?>