<?php /* Template_ 2.2.7 2018/03/06 13:20:39 /home/dartz/public_html/application/views/my/my_qnalist.html 000003785 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

    <article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4 class="kor">1:1문의내역</h4>
                    <p>문의하신 내용과 답변을 편리하게 확인할 수 있습니다.</p>
                </div>
                <div class="page_body">
                    <section class="qna_list">
                       
                        <div class="acc-list">
                            <ul>
                                <li class="head">
                                    <span class="sort">구분</span>
                                    <span class="tit">내용</span>
                                    <span class="status">상태</span>
                                </li>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
                                <li>
<?php if($TPL_V1["QNA_ANSWER_YN"]=="Y"){?>
                                    <a href="#" role="button" onclick="accFn(this);return false;">
<?php }else{?>
                                	<a href="javascirpt:;" role="button" >
<?php }?>
                                        <span class="sort">Q</span>
                                        <span class="tit"><?php echo nl2br($TPL_V1["QNA_QUESTION"])?>

<?php if($TPL_V1["QNA_QUESTION_FILENAME"]!=""){?>
                                            <span class="file"><?php echo $TPL_V1["QNA_QUESTION_FILENAME"]?></span>
<?php }?>
                                        </span>
<?php if($TPL_V1["QNA_ANSWER_YN"]=="Y"){?>
                                        <span class="status color">답변완료</span>
<?php }else{?>
                                        <span class="status wait">답변대기</span>
<?php }?>
                                    </a>
<?php if($TPL_V1["QNA_ANSWER_YN"]=="Y"){?>
                                    <div class="answer">
                                        <span class="sort">A</span>
                                        <span class="txt">
                                           <?php echo nl2br($TPL_V1["QNA_ANSWER"])?>

                                        </span>
                                    </div>
<?php }?>
                                </li>
<?php }}else{?>
                                <li>
	                                <span class="tit none">등록 된 문의내역이 없습니다.</span>
                                </li>
<?php }?>
                            </ul>
                            <script type="text/javascript">
                            
                            function accFn(obj){
                                var $tg = $(obj).closest("li");
                                var $answer = $tg.find(".answer")
                                $tg.toggleClass("on");
                                var _h = $answer.find(".txt").outerHeight();
                                if($tg.hasClass("on")) $answer.css({'height':_h})
                                else $answer.css({'height':0});
                            }
                        </script>
                        </div>
                        <?php echo $TPL_VAR["pagination"]?>

                    </section>
                </div>
            </div>
        </section>
    </article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>