<?php /* Template_ 2.2.7 2018/03/08 14:45:11 /home/dartz/public_html/application/views/my/point_list.html 000004023 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4 style="margin-bottom:0;">포인트 내역</h4>
                </div>
                <div class="my_point">
                    <div class="crt_point">
                        <span>현재 보유 포인트</span>
                        <span class="pt"><?php echo number_format($TPL_VAR["pldata"]['PL_TOTALPOINT'])?></span>
                        <div class="disappear_point">
                            당월 소멸 예정 포인트 <span><?php echo number_format($TPL_VAR["ptdata"]['pt_point'])?>p</span>
                        </div>
                    </div>
                    <a href="/point/index" class="button btn_line"><span>포인트 안내</span></a>
                </div>

                <div class="page_body join_form">
                    <section>
                        <div class="tb tb_view tb_point">
                            <table>
                                <colgroup>
                                    <col width="140px">
                                    <col width="80px">
                                    <col width="320px">
                                    <col width="140px">
                                    <col width="140px">
                                    <col width="140px">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="line">날짜</th>
                                        <th class="line">유형</th>
                                        <th class="line">내용</th>
                                        <th class="line">적립</th>
                                        <th class="line">사용</th>
                                        <th>포인트 잔액</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
                                	<tr>
                                        <td class="line"><?php echo substr($TPL_V1["PL_CREDATE"], 0, 10)?></td>
                                        <td class="line"><?php echo $TPL_V1["PL_USE"]?></td>
                                        <td class="line"><?php echo $TPL_V1["PL_CODE_TXT"]?><?php if($TPL_V1["PL_TITLE"]!=""){?>(<?php echo $TPL_V1["PL_TITLE"]?>)<?php }?></td>
                                        <td class="line"><span class="color"><?php if($TPL_V1["PL_TYPE"]=="U"){?><?php echo number_format($TPL_V1["PL_POINT"])?>p<?php }?></span></td>
                                        <td class="line"><?php if($TPL_V1["PL_TYPE"]=="D"){?>-<?php echo number_format($TPL_V1["PL_POINT"])?>p<?php }?></td>
                                        <td><?php echo $TPL_V1["PL_TOTALPOINT"]?>p</td>
                                    </tr>
<?php }}else{?>
                                    <tr>
                                        <td colspan="6"><p class="none">등록 된 포인트내역이 없습니다.</p></td>
                                    </tr>
<?php }?>
                                    
                                </tbody>
                            </table>
                        </div>
                        <?php echo $TPL_VAR["pagination"]?>

                    </section>

                </div>
            </div>
        </section>
    </article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>