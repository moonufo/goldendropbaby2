<?php /* Template_ 2.2.7 2018/03/05 15:59:38 /home/dartz/public_html/application/views/_wns/layout/header.html 000001420 */ ?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta property="og:title" content="illuma3" />
    <meta property="og:image" content="https://dartz.swtown.co.kr/images/main/prd.png" />
    <meta property="og:description" content="아일랜드 원유와 OPO 구조로 프리미엄 일루마를 완성하다" />
    <meta property="og:url" content="https://dartz.swtown.co.kr" />
    <meta property="og:type" content="website" />

    <title>illuma</title>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <!--[if IE]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="//cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="//cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>


    <link rel="stylesheet" type="text/css" href="/assets/css/reset.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/assets/css/illuma.css" media="all" />
    <script src="/assets/js/common.js"></script>


</head>
<body>
	<div id="illuma">