<?php /* Template_ 2.2.7 2018/02/28 10:55:02 /home/dartz/public_html/application/views/_wns/menu/gnb.html 000006454 */ ?>
<header id="header" class="<?php if(($TPL_VAR["controller_chk"]=='Y')){?>on<?php }?>">
        <div class="container">
            <h1><a href="/"><img src=" <?php if(($TPL_VAR["method"]=='smart'||$TPL_VAR["method"]=='smart_view')){?> /images/diary/h1_smartdiary.png <?php }else{?> /images/logo.png<?php }?> " alt="illuma"></a></h1>
            <nav class="gnb">
                <div class="menu menu1 <?php if(($TPL_VAR["controller"]=='story')){?>on <?php }?>">
                    <a href="/story/brand"><span>illuma Story</span><span class="kor">일루마 이야기</span></a>
                    <ul class="dep2">
                        <li class="subm1 <?php if(($TPL_VAR["method"]=='brand')){?>on <?php }?>"><a href="/story/brand"><span>Brand Story</span><span class="kor">브랜드 이야기</span></a></li>
                        <li class="subm2 <?php if(($TPL_VAR["method"]=='product'||$TPL_VAR["method"]=='product_lreland'||$TPL_VAR["method"]=='product_opo')){?>on <?php }?>"><a href="/story/product"><span>Product Story</span><span class="kor">제품 이야기</span></a>
                            <ul class="dep3">
                                <li class="tri <?php if(($TPL_VAR["method"]=='product_lreland')){?>on <?php }?>"><a href="/story/product_lreland"><span>Ireland Story</span><span class="kor">아일랜드 이야기</span></a></li>
                                <li class="<?php if(($TPL_VAR["method"]=='product_opo')){?>on <?php }?>"><a href="/story/product_opo"><span>OPO Story</span><span class="kor">OPO 이야기</span></a></li>
                            </ul>
                        </li>
                        <li class="subm3 <?php if(($TPL_VAR["method"]=='wyeth')){?>on <?php }?>"><a href="/story/wyeth"><span>Wyeth Story</span><span class="kor">와이어스 이야기</span></a></li>
                    </ul>
                </div>
                <div class="menu menu2 <?php if(($TPL_VAR["controller"]=='shop')){?>on <?php }?>">
                    <a href="/shop/step1"><span>Goldendrop Shop</span><span class="kor">제품구입</span></a>
                    <ul class="dep2">
                        <li class="subm1 <?php if(($TPL_VAR["controller"]=='shop')){?>on <?php }?>"><a href="/shop/step1"><span>Product</span><span class="kor">제품소개</span></a>
                            <ul class="dep3">
                                <li class="<?php if(($TPL_VAR["method"]=='step1')){?>on <?php }?>"><a href="/shop/step1"><span>illuma 1</span><span class="kor">일루마 1단계</span></a></li>
                                <li class="tri <?php if(($TPL_VAR["method"]=='step2')){?>on <?php }?>"><a href="/shop/step2"><span>illuma 2</span><span class="kor">일루마 2단계</span></a></li>
                                <li class="<?php if(($TPL_VAR["method"]=='step3')){?>on <?php }?>"><a href="/shop/step3"><span>illuma Goldendrop 3</span><span class="kor">일루마 골든드롭 3</span></a></li>
                                <li class="<?php if(($TPL_VAR["method"]=='purchasing')){?>on <?php }?>"><a href="/shop/purchasing"><span>Store</span><span class="kor">구입처</span></a></li>
                            </ul>
                        </li>
                        <!--<li class="subm2"><a href="#" onclick="alert_ready();return false;"><span>Subscription</span><span class="kor">정기배송</span></a></li>-->
                    </ul>
                </div>
                <div class="menu menu3 <?php if(($TPL_VAR["controller"]=='diary')){?>on <?php }?>">
                    <a href="/diary/special"><span>Goldendrop Mom's Diary</span><span class="kor">육아일기</span></a>
                    <ul class="dep2">
                        <li class="subm1 <?php if(($TPL_VAR["method"]=='special'||$TPL_VAR["method"]=='special_view')){?>on<?php }?>"><a href="/diary/special"><span>Special Diary</span><span class="kor">특별한 육아소식</span></a></li>
                        <li class="subm2 <?php if(($TPL_VAR["method"]=='smart'||$TPL_VAR["method"]=='smart_view')){?>on<?php }?>"><a href="/diary/smart"><span>Smart Diary</span><span class="kor">똑똑한 육아정보</span></a></li>
                        <li class="subm3 <?php if(($TPL_VAR["method"]=='happy')){?>on<?php }?>"><a href="/diary/happy?H_TYPE="><span>Happy Diary</span><span class="kor">즐거운 육아소통</span></a></li>
                    </ul>
                </div>
                <div class="menu menu4 <?php if(($TPL_VAR["controller"]=='event')){?>on <?php }?>">
                    <a href="/event/event_index"><span>Goldendrop Members</span><span class="kor">회원혜택</span></a>
                    <ul class="dep2">
                        <li class="subm1 <?php if(($TPL_VAR["method"]=='event_index')){?>on<?php }?>"><a href="/event/event_index"><span>Event</span><span class="kor">이벤트</span></a></li>
                        <li class="subm2 <?php if(($TPL_VAR["method"]=='point_index')){?>on<?php }?>"><a href="/event/point_index"><span>Point</span><span class="kor">포인트</span></a></li>
                    </ul>
                </div>
                <div class="menu menu5 <?php if(($TPL_VAR["controller"]=='contact')){?>on <?php }?>">
                    <a href="/contact/contact_index"><span>Contact</span><span class="kor">고객문의</span></a>
                    <ul class="dep2">
                        <li class="subm1 <?php if(($TPL_VAR["method"]=='contact_index')){?>on<?php }?>"><a href="/contact/contact_index"><span>Customer Service</span><span class="kor">고객센터</span></a></li>
                        <li class="subm2 <?php if(($TPL_VAR["method"]=='notice_list')){?>on<?php }?>"><a href="/contact/notice_list"><span>Notice</span><span class="kor">공지사항</span></a></li>
                        <!--<li class="subm3 <?php if(($TPL_VAR["method"]=='faq_list')){?>on<?php }?>"><a href="/contact/faq_list"><span>FAQ</span></a></li>-->
                        <!--<li class="subm4 <?php if(($TPL_VAR["method"]=='qna_index')){?>on<?php }?>"><a href="/contact/qna_index"><span>1:1문의</span></a></li>-->
                    </ul>
                </div>
            </nav>

<?php $this->print_("menu_sub_my",$TPL_SCP,1);?>


        </div>
    </header>