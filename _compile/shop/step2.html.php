<?php /* Template_ 2.2.7 2018/03/07 11:15:43 /home/dartz/public_html/application/views/shop/step2.html 000025225 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/shop/step1">Goldendrop Shop</a><span>/</span><a href="/shop/step1">Product</a><span>/</span><strong>일루마 2단계</strong>
    </div>
    <section id="product" class="page_wrap">
        <div class="container">
            <div class="prd_top">
                <div class="detail_view">
                    <div class="thumbnail">
                        <a href="#" onclick="prdviewfn(0);return false;"><img src="/images/sample/prd_thumb.png" alt=""><span class="off"><img src="/images/sample/prd_thumb_off.png" alt=""></span></a>
                        <a href="#" onclick="prdviewfn(1);return false;"><img src="/images/sample/prd_thumb.png" alt=""><span class="off"><img src="/images/sample/prd_thumb_off.png" alt=""></span></a>
                        <a href="#" onclick="prdviewfn(2);return false;"><img src="/images/sample/prd_thumb.png" alt=""><span class="off"><img src="/images/sample/prd_thumb_off.png" alt=""></span></a>
                        <a href="#" onclick="prdviewfn(3);return false;"><img src="/images/sample/prd_thumb.png" alt=""><span class="off"><img src="/images/sample/prd_thumb_off.png" alt=""></span></a>
                        <a href="#" onclick="prdviewfn(4);return false;"><img src="/images/sample/prd_thumb.png" alt=""><span class="off"><img src="/images/sample/prd_thumb_off.png" alt=""></span></a>
                    </div>
                    <div class="img">
                        <ul>
                            <li><img src="/images/sample/prd_img.png" alt=""></li>
                            <li><img src="/images/sample/prd_img.png" alt=""></li>
                            <li><img src="/images/sample/prd_img.png" alt=""></li>
                            <li><img src="/images/sample/prd_img.png" alt=""></li>
                            <li><img src="/images/sample/prd_img.png" alt=""></li>
                        </ul>
                    </div>
                </div>
                <script type="text/javascript">
                    $(function(){
                        prdviewfn(0);
                    })
                    function prdviewfn(num){
                        var $obj = $(".detail_view");
                        $obj.find(".thumbnail .on, .img .on").removeClass("on");
                        $obj.find(".thumbnail a:eq("+num+"), .img li:eq("+num+")").addClass("on");
                    }
                </script>
                <div class="detail_info">
                    <h4>일루마 2
                        <p>illuma stage 2</p>
                    </h4>
                    <p class="txt">6개월부터 12개월까지 (성장기용 조제유)</p>

                    <div class="prd_info">
                        <dl>
                            <dt>원산지</dt>
                            <dd>아일랜드</dd>
                        </dl>
                        <dl>
                            <dt>내용량</dt>
                            <dd>900g (4,470 kcal)</dd>
                        </dl>
                        <dl>
                            <dt>제조사</dt>
                            <dd>Wyeth Nutritionals Ireland Ltd</dd>
                        </dl>
                    </div>

                </div>
            </div>
        </div>

        <div class="tab color">
            <ul>
                <li class="on"><a href="#" role="button"><span>제품 정보 </span></a></li>
                <li><a href="#" role="button"><span>영양 정보</span></a></li>
                <li><a href="#" role="button"><span>준비와 조유방법</span></a></li>
            </ul>
            <script type="text/javascript">
                $(function(){
                    $(".tab li a").on("click", function(e){
                        if(e.preventDefault())e.preventDefault();
                        var $tab = $(this).parent();
                        var _idx = $tab.index();
                        if(!$tab.hasClass("on")){
                            $(".tab .on,.tab-contents.on").removeClass("on");
                            $(".tab li:eq("+_idx+"),.tab-contents:eq("+_idx+")").addClass("on");
                        }

                    })
                })
            </script>
        </div>
        <div class="container">
            <div class="tab-contents on">
                <div class="prd_contents">
                    <h5>제품 정보</h5>
                    <div class="tb">
                        <table>
                            <colgroup>
                                <col width="200px">
                                <col width="380px">
                                <col width="200px">
                                <col width="180px">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>제품명</th>
                                <td>일루마 2단계</td>
                                <th>축산물의 유형</th>
                                <td>성장기용 조제유</td>
                            </tr>
                            <tr>
                                <th>업소명 및 소재지</th>
                                <td colspan="3">네슬레코리아 유한책임회사, 서울시 서대문구 충정로 70, 15~16층 (미근동, 웨스트게이트타워)</td>
                            </tr>
                            <tr>
                                <th>제조사</th>
                                <td>Wyeth Nutritionals Ireland Ltd</td>
                                <th>원산지</th>
                                <td>아일랜드</td>
                            </tr>
                            <tr>
                                <th>유통기한</th>
                                <td>캔 밑면 별도 표기일까지 (EXP로 표기, 읽는 법: 일. 월. 년. 순)</td>
                                <th>내용량</th>
                                <td>900g (4,470 kcal)</td>
                            </tr>
                            <tr>
                                <th>원재료명</th>
                                <td colspan="3">유당분말, 탈지분유, 오일블렌드(팜유, 대두유, 고올레산해바라기유, 야자유, 대두레시틴, L-아스코빌팔미테이트), 유청단백분말, 프락토올리고당, 구연산삼 나트륨, DHA오일믹스[Algal Oil(DHA함유), 고올레산해바라기유, L-아스코빌팔미테이트, d-토코페롤(혼합형)], ARA오일믹스[Fungal Oil (아라키돈산함유), 고올레산해바라기유, d-토코페롤(혼합형), L-아스코빌팔미테이트], 수산화칼슘, 염화콜린, 비타민C, 염화마그네슘, 탄산칼슘, 탄산수소나트륨, 타우린, 염화칼슘, 황산제일철, 이노시톨, 정제염, 구연산, 황산아연, 지용성비타민프리믹스(해바라기유, dl-α-토코페릴아세테이트, 유성비타민A지방산에스테르, β-카로틴, 비타민D3, 비타민K1), 수용성비타민프리믹스(유당분말, 니코틴산아미드, 판토텐산칼슘, 비타민B1염산염, 비타민B2, 비타민B6염산염, 엽산, 아셀렌산나트륨, 비오틴, 비타민B12), 수산화칼륨, 5’-시티딜산, L-카르니틴, 5’-우리딜산이나트륨, 5’-아데닐산, 5’-구아닐산이나트륨, 5’-이노신산이나트륨, 황산동, 황산망간, 요오드칼륨<br>
                                    <strong>우유, 대두 함유</strong></td>
                            </tr>
                            <tr>
                                <th>성분명 및 함량 (100mL당)</th>
                                <td>illumaopo<sup>TM</sup> (sn-2팔미트산염) 352.5mg</td>
                                <th>보존 기준</th>
                                <td>실온 보관</td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="infotxt">
                            <span>- 질소 및 이산화탄소 혼합 충전</span>
                            <span>- 본 제품은 공정거래위원회 고시 소비자 분쟁해결기준에 의거, 교환 또는 보상 받으실 수 있습니다.</span>
                            <span>- 반품처: 구입처 및 수입원</span>
                            <span>- 부정·불량식품 신고: 국번없이 1399</span>
                            <span>- 소비자 상담실: 080-600-2000</span>
                            <span>- 권고사항: 모유가 아기에게 가장 좋은 식품입니다. 가능한 오래 모유 수유하는 것을 권장합니다.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-contents">
                <div class="prd_contents">
                    <h5>영양 정보<span>총내용량900g 100mL당 <strong>70kcal</strong></span></h5>

                    <div class="tb">
                        <p class="tb_unit"><strong>100 mL당 (섭취시 기준)</strong></p>
                        <table>
                            <colgroup>
                                <col width="150px">
                                <col width="170px">
                                <col width="150px">
                                <col width="170px">
                                <col width="150px">
                                <col width="170px">
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>나트륨</th>
                                <td>40 mg</td>
                                <th>비타민B1</th>
                                <td>0.063 mg</td>
                                <th>sn-2팔미트산염 </th>
                                <td>352.5 mg</td>
                            </tr>
                            <tr>
                                <th>탄수화물</th>
                                <td>8.1 g</td>
                                <th>비타민B2</th>
                                <td>0.17 mg</td>
                                <th>뉴클레오타이드</th>
                                <td>2.6 mg</td>
                            </tr>
                            <tr>
                                <th>당류</th>
                                <td>7.8 g</td>
                                <th>나이아신</th>
                                <td>0.53 mg NE</td>
                                <th>리놀레산</th>
                                <td>423 mg</td>
                            </tr>
                            <tr>
                                <th>지방</th>
                                <td>3 g</td>
                                <th>비타민B6</th>
                                <td>0.05 mg</td>
                                <th>베타카로틴</th>
                                <td>15 μg</td>
                            </tr>
                            <tr>
                                <th>트랜스지방</th>
                                <td>0 g</td>
                                <th>엽산</th>
                                <td>13 μg </td>
                                <th>아라키돈산</th>
                                <td>11.4 mg </td>
                            </tr>
                            <tr>
                                <th>포화지방</th>
                                <td>1.2 g</td>
                                <th>비타민B12</th>
                                <td>0.18 μg</td>
                                <th>알파락트알부민</th>
                                <td>129.7 mg</td>
                            </tr>
                            <tr>
                                <th>콜레스테롤</th>
                                <td>2.6 mg</td>
                                <th>비오틴</th>
                                <td>1.4 μg</td>
                                <th>알파리놀렌산</th>
                                <td>41 mg</td>
                            </tr>
                            <tr>
                                <th>단백질</th>
                                <td>2.15 g</td>
                                <th>판토텐산</th>
                                <td>0.34 mg</td>
                                <th>이노시톨</th>
                                <td>5.5 mg</td>
                            </tr>
                            <tr>
                                <th>칼륨</th>
                                <td>89 mg</td>
                                <th>인</th>
                                <td>51 mg </td>
                                <th>콜린</th>
                                <td>20 mg</td>
                            </tr>
                            <tr>
                                <th>비타민A</th>
                                <td>58 μg RE</td>
                                <th>요오드</th>
                                <td>10 μg</td>
                                <th>타우린</th>
                                <td>4.6 mg</td>
                            </tr>
                            <tr>
                                <th>비타민C</th>
                                <td>9 mg</td>
                                <th>마그네슘</th>
                                <td>8.5 mg</td>
                                <th>프락토올리고당</th>
                                <td>0.3 g</td>
                            </tr>
                            <tr>
                                <th>칼슘</th>
                                <td>75 mg</td>
                                <th>아연</th>
                                <td>0.6 mg</td>
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>철분</th>
                                <td>1.2 mg</td>
                                <th>셀렌</th>
                                <td>2 μg</td>
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>비타민D</th>
                                <td>1.05 μg</td>
                                <th>구리</th>
                                <td>0.029 mg</td>
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>비타민E</th>
                                <td>0.67 mg α-TE</td>
                                <th>망간</th>
                                <td>0.034 mg</td>
                                <th></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>비타민K</th>
                                <td>4.4 μg</td>
                                <th>DHA</th>
                                <td>11.4 mg</td>
                                <th></th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-contents">
                <div class="prd_contents">
                    <h5>준비와 조유방법</h5>
                    <section class="prd_open">
                        <h6>뚜껑 여는 방법</h6>
                        <ul>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_open1.png" alt=""></div>
                                <p class="txt">1. 양손으로 통의 윗면을 잡고,<br>엄지 손가락으로 잠금 장치를 안쪽으로 가볍게 누릅니다.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_open2.png" alt=""></div>
                                <p class="txt">2. 잠금 장치가 열리면 덮개를 부드럽게 위로 움직입니다. 스푼은 뚜껑에 있습니다.<br>구입 당시 뚜껑의 비닐포장이 부착되어 있지 않다면 개봉하여 섭취하지 마십시오.</p>
                            </li>
                        </ul>
                        <div class="caution">
                            <dl>
                                <dt>경고</dt>
                                <dd>- 끓이지 않은 물, 소독하지 않은 젖병이나 컵, 옳지 않은 조유 방법은 아기를 아프게 할 수 있습니다.</dd>
                                <dd>- 바르지 못한 보관과 취급, 준비와 수유는 아기의 건강에 나쁜 영향을 줄 수 있습니다.</dd>
                            </dl>
                        </div>
                    </section>
                    <section class="prd_step">
                        <ul>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step1.png" alt=""></div>
                                <p class="txt">분유를 타기 전 손을 깨끗하게<br>씻어주세요.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step2.png" alt=""></div>
                                <p class="txt">남아 있는 우유가 없도록<br>젖병이나 컵, 젖꼭지, 뚜껑을 깨끗하게<br>세척합니다.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step3.png" alt=""></div>
                                <p class="txt"><span style="letter-spacing:-.15em;">끓는 물에 젖병이나 컵, 젖꼭지, 뚜껑을</span><br>5분간 끓여 주시고 사용 전에는<br>덮어 두세요.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step4.png" alt=""></div>
                                <p class="txt">조유하기 위한 물을<br>5분간 끓인 후,<br>미지근한 물이 될때까지<br>식혀주세요.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step5.png" alt=""></div>
                                <p class="txt">조유 양은 아래의<br>조유 방법 표를 참고하여 주세요.<br>정확한 양의 미지근한<br>물을 젖병 또는 컵에 부어주세요.</p>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step6.png" alt=""></div>
                                <p class="txt">뚜껑에 있는 전용스푼을 이용하여<br>분유 양을 측정해주세요.<br>캔 상단의 바를 이용하여<br>정확하게 계량합니다.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step7.png" alt=""></div>
                                <p class="txt">조유 방법표를 참고하여<br>분유를 넣습니다.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step8.png" alt=""></div>
                                <p class="txt">스푼은 사용 후 뚜껑 아래<br>제자리에 놓아 둡니다.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step9.png" alt=""></div>
                                <p class="txt">분유가 완전히 녹을 수 있도록<br>젖병 또는 컵을 좌우로 흔들어 줍니다.<br>수유하기 전 온도를 확인합니다.</p>
                            </li>
                            <li>
                                <div class="img"><img src="/images/product/prdimg_step10.png" alt=""></div>
                                <p class="txt">사용이 끝난 분유 통은 꽉 닫아<br>시원하고 건조한 곳에<br>보관해 주세요. 개봉 후 4주안에<br>모두 사용해주세요.</p>
                            </li>
                        </ul>
                        <div class="caution">
                            <dl>
                                <dt>경고</dt>
                                <dd>- 아기가 먹을 만큼만 조유 하시고 즉시 먹이시기 바랍니다.</dd>
                                <dd>- 지시 사항을 정확하게 따르고, 남은 분유는 보관하지 말고 폐기하십시오.</dd>
                                <dd>- 수유 시에는 질식이 일어날 수 있으므로 항상 아기를 지켜봐주시고, 아기의 성장발달에 따라 조유 방법을 젖병에서 컵으로 변경해 주세요.</dd>
                            </dl>
                        </div>
                        <div class="tb tb_center">
                            <table>
                                <colgroup>
                                    <col width="320px">
                                    <col width="320px">
                                    <col width="320px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th class="line">미지근한 물 (끓여서 식힌)</th>
                                    <th class="line">스푼</th>
                                    <th>24 시간 내 수유량</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="line">140 mL</td>
                                    <td class="line">3</td>
                                    <td>5-7</td>
                                </tr>

                                </tbody>
                            </table>
                            <div class="infotxt">
                                권고사항 1 스푼=7.05g. illuma 2 세 스푼 당 140mL의 물과 섞어 주세요.<br>
                                동봉된 스푼만 사용 하십시오.<br>
                                지시된 양보다 많거나 적은 분량의 분유를 사용하면 탈수가 일어날 수 있으며 아기가 적절한 영양을 섭취하는 것을 방해합니다.<br>
                                의학적 상담 없이 조유 비율을 변경하지 마십시오.<br>
                                6개월 이상 된 아기에게는 분유 이외의 이유식이 필요합니다.<br>
                                해당 시기에는 아기의 식단이 다양해집니다.<br>
                                아기의 월령, 입맛, 함께 먹이는 이유식에 따라 조유양을 조절하세요.<br>
                                아기에게 새로운 음식을 소개하기 전에 전문가의 상담을 받으시길 권장합니다.<br>
                                이유식 섭취가 좀 더 이른 경우 전문가에게 상담 받기를 권하며 분유의 섭취를 줄이는 것이 좋습니다.<br>
                                높은 온도에서 장시간 보관하지 마십시오.<br>
                                유통기한은 캔 밑에 인쇄되어 있습니다.<br>
                                특정 원료에 알레르기가 있는 아기는 반드시 원재료를 확인 후에 사용하시기 바랍니다.<br>
                                냉장 및 냉동 보관하지 마십시오. 벌레나 해충이 들어가지 않도록 뚜껑을 꼭 닫아 보관하십시오.
                            </div>
                        </div>
                    </section>




                </div>
            </div>
        </div>
    </section>
</article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>