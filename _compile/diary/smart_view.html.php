<?php /* Template_ 2.2.7 2018/03/09 15:30:19 /home/dartz/public_html/application/views/diary/smart_view.html 000004947 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/diary/special">Goldendrop Mom's Diary</a><span>/</span><strong>Smart Diary</strong>
    </div>
    <section id="diary" class="page_wrap">
        <div class="container">
            <div class="tb tb_viewpage">
                <table>
                    <colgroup>
                        <col width="200px">
                        <col width="*">
                    </colgroup>
                    <tbody>
                    <tr>
                        <td colspan="2" class="view_top">
                            <div class="date"><?php echo substr($TPL_VAR["DM_CREDATE"], 0, 10)?> <div class="author" style="display:inline;font-size:18px;float: right">작성자 : <?php echo $TPL_VAR["DM_ID"]?></div></div>
                            <div class="category"><?php echo $TPL_VAR["gubun_title"]?></div>
                            <div class="title"><?php echo $TPL_VAR["DM_TITLE"]?></div>
                            <!--<div class="date"><?php echo substr($TPL_VAR["DM_CREDATE"], 0, 10)?></div>-->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="incont">
                            <div class="contents">
                                <?php echo $TPL_VAR["DM_CONTENT"]?>

                            </div>
                            <div class="bot_contents">
                                <div class="tag_wrap">

                                        <div class="tag" >
<?php if($TPL_VAR["DM_TAG"]!=""){?>
                                                <span class="tit">Tag :</span>
                                                <span class="hash"><?php echo $TPL_VAR["DM_TAG"]?></span>
<?php }else{?>
                                            <br>
<?php }?>
                                        </div>

                                    <div class="share">
                                        <a href="#" onclick="sns_send();return false;"><img src="/images/common/btn_instagram.png" alt="인스타그램"></a>
                                        <a href="javascript:window.print();" class="button btn_print"><span>프린트</span></a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>이전글</th>
<?php if($TPL_VAR["prev_smart_idx"]> 0){?>
                        <td><a href="javascript:move_url('<?php echo $TPL_VAR["prev_smart_idx"]?>')" class="btn_listnav"><strong><?php echo $TPL_VAR["prev_smart_title"]?></strong></a></td>
<?php }else{?>
                        <td><a href="#" onclick="return false;" class="btn_listnav"><strong>이전글이 없습니다.</strong></a></td>
<?php }?>
                    </tr>
                    <tr>
                        <th>다음글</th>
<?php if($TPL_VAR["next_smart_idx"]> 0){?>
                        <td><a href="javascript:move_url('<?php echo $TPL_VAR["prev_special_idx"]?>')" class="btn_listnav"><strong><?php echo $TPL_VAR["prev_special_title"]?></strong></a></td>
<?php }else{?>
                        <td><a href="#" onclick="return false;" class="btn_listnav"><strong>이전글이 없습니다.</strong></a></td>
<?php }?>
                    </tr>

                    </tbody>
                </table>
                <div class="btn_wrap">
                    <a href="javascript:move_list();" class="btn btn_next"><span>목록</span></a>
                </div>
                <form name="frm_view" id="frm_view" method="get" action="/diary/smart_view">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $TPL_VAR["per_page"]?>">
                    <input type="hidden" name="dm_idx" id="dm_idx" value="">
                    <input type="hidden" name="search_gubun" id="search_gubun" value="<?php echo $TPL_VAR["smart_gubun"]?>">
                </form>
            </div>
        </div>
    </section>
</article>
<script>
    function move_url(idx){
        $("#dm_idx").val(idx);
        $("#frm_view").submit();
    }

    function move_list(){
        $("#frm_view").attr("action","smart");
        $("#frm_view").submit();
    }

    function sns_send(){
        var _url = encodeURIComponent(window.location.href);
        window.open("https://www.facebook.com/sharer/sharer.php" + "?u="+_url, "_blank", "resizable=yes","scrollbars=yes");
    }

</script>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>