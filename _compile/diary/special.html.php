<?php /* Template_ 2.2.7 2018/03/09 11:48:45 /home/dartz/public_html/application/views/diary/special.html 000002590 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
	<div class="location">
		<a href="/">Home</a><span>/</span><a href="/diary/special">Goldendrop Mom's Diary</a><span>/</span><strong>Special Diary</strong>
	</div>
	<section id="diary" class="page_wrap">
		<div class="container">
			<!--<div class="page_top">-->
				<!--<h4 style="margin-bottom:0">Special Diary</h4>-->
			<!--</div>-->
			<!--<hr>-->
			<div class="thumb_list">
				<ul class="hor_list">
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
					<li>
						<a href="javascript:move_url('<?php echo $TPL_V1["DS_IDX"]?>');" class="imgwrap"><span class="img"><img src="http://dartz.swtown.co.kr<?php echo $TPL_V1["DS_PATH"]?><?php echo $TPL_V1["DS_FILENAME"]?>" alt=""  style="width: 360px; height: 160px;"></span></a>
						<div class="incont">
							<a href="javascript:move_url('<?php echo $TPL_V1["DS_IDX"]?>','<?php echo $TPL_VAR["start_no"]-$TPL_I1?>');" style="cursor:pointer">
								<span class="date gd">No. <?php echo $TPL_VAR["start_no"]-$TPL_I1?> <span><?php echo substr($TPL_V1["DS_CREDATE"], 0, 10)?></span></span>
								<span class="tit bk"><?php echo $TPL_V1["DS_TITLE"]?></span>
								<?
								 	$content_tag = strip_tags( $TPL_V1["DS_CONTENT"]);
									$content_len = mb_strlen( $content_tag );
									$content_text = mb_substr($content_tag,0,162,"utf-8");

									if($content_len > 168){
										echo "<span class='txt'>$content_text...</span>" ;
									}else{
										echo "<span class='txt'>$content_tag</span>" ;
									}

								?>
							</a>
						</div>
					</li>
<?php }}?>
				</ul>

				<?php echo $TPL_VAR["pagination"]?>


			</div>
			
		</div>
	</section>

	<form class="frmMod" action="/diary/special_view" method="get">
		<input type="hidden" name="per_page" id="per_page" value="<?php echo $TPL_VAR["per_page"]?>">
		<input type="hidden" name="ds_idx" id="ds_idx" value="">
		<input type="hidden" name="ds_no" id="ds_no" value="">
	</form>

</article>
<script type="text/javascript">
    function move_url(idx,ds_no) {
        $('#ds_idx').val(idx);
        $('#ds_no').val(ds_no);
        $('.frmMod').submit();
    }
</script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>