<?php /* Template_ 2.2.7 2018/03/02 20:08:48 /home/dartz/public_html/application/views/contact/notice_view.html 000005187 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/contact/contact_index">Contact</a><span>/</span><strong>공지사항</strong>
    </div>
    <section id="contact" class="page_wrap">
        <div class="notice">
            <div class="container">
                <div class="tb tb_viewpage">
                    <table>
                        <colgroup>
                            <col width="200px">
                            <col width="*">
                        </colgroup>
                        <tbody>
                        <tr>
                            <td colspan="2" class="view_top">
                                <div class="category">공지사항</div>
                                <div class="date"><?php echo substr($TPL_VAR["NOTICE_CREDATE"], 0, 10)?></div>
                                <div class="title"><?php echo $TPL_VAR["NOTICE_TITLE"]?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="incont">
                                <div class="contents">
                                    <?php echo $TPL_VAR["NOTICE_CONTENT"]?>

                                </div>
                                <div class="bot_contents">
                                    <div class="file_wrap">
                                        <span class="tit">첨부파일 :</span>
                                        <a href="http://dartz.swtown.co.kr<?php echo $TPL_VAR["NOTICE_PATH"]?><?php echo $TPL_VAR["NOTICE_FILENAME"]?>" target="_blank" class="file"><?php echo $TPL_VAR["NOTICE_FILENAME"]?></a>
                                    </div>
                                    <div class="tag_wrap">
                                        <div class="tag">
<?php if($TPL_VAR["NOTICE_TAG"]!=""){?>
                                            <span class="tit">Tag :</span>
                                            <span class="hash"><?php echo $TPL_VAR["NOTICE_TAG"]?></span>
<?php }else{?>
                                            <br>
<?php }?>
                                        </div>
                                        <div class="share">
                                            <a href="#" onclick="sns_send();return false;"><img src="/images/common/btn_instagram.png" alt="인스타그램"></a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <th>이전글</th>
<?php if($TPL_VAR["prev_notice_idx"]> 0){?>
                            <td><a href="javascript:move_url('<?php echo $TPL_VAR["prev_notice_idx"]?>')" class="btn_listnav"><strong><?php echo $TPL_VAR["prev_notice_title"]?></strong></a></td>
<?php }else{?>
	                        <td><a href="#" onclick="return false;" class="btn_listnav"><strong>이전글이 없습니다.</strong></a></td>
<?php }?>
                            
                        </tr>
                        <tr>
                            <th>다음글</th>
<?php if($TPL_VAR["next_notice_idx"]> 0){?>
	                        <td><a href="javascript:move_url('<?php echo $TPL_VAR["next_notice_idx"]?>')" class="btn_listnav"><strong><?php echo $TPL_VAR["next_notice_title"]?></strong></a></td>
<?php }else{?>
	                        <td><a href="#" onclick="return false;" class="btn_listnav"><strong>이전글이 없습니다.</strong></a></td>
<?php }?>
                            
                        </tr>

                        </tbody>
                    </table><br><br>
                    <div class="btn_wrap">
                        <a href="javascript:move_list();" class="btn btn_next"><span>목록</span></a>
                    </div>
                    <form name="frm_view" id="frm_view" method="get" action="/contact/notice_view">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $TPL_VAR["per_page"]?>">
                        <input type="hidden" name="notice_idx" id="notice_idx" value="">
                    </form>
                </div>

            </div>

        </div>

    </section>
</article>
<script>
    function move_url(idx){
        $("#notice_idx").val(idx);
        $("#frm_view").submit();
    }

    function move_list(){
        $("#frm_view").attr("action","notice_list");
        $("#frm_view").submit();
    }

    function sns_send(){
        var _url = encodeURIComponent(window.location.href);
        window.open("https://www.facebook.com/sharer/sharer.php" + "?u="+_url, "_blank", "resizable=yes","scrollbars=yes");
    }

</script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>