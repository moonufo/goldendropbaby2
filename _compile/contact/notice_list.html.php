<?php /* Template_ 2.2.7 2018/02/22 10:07:40 /home/dartz/public_html/application/views/contact/notice_list.html 000002629 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/contact/contact_index">Contact</a><span>/</span><strong>공지사항</strong>
    </div>
    <section id="contact" class="page_wrap">
        <div class="container">
            <div class="page_top">
                <h4>공지사항</h4>
            </div>
        </div>
        <div class="notice">
            <div class="container">
                <div class="tb tb_list">
                    <table>
                        <colgroup>
                            <col width="110">
                            <col width="*">
                            <col width="200">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>번호</th>
                            <th>제목</th>
                            <th>작성일</th>
                        </tr>
                        </thead>
                        <tbody>
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
                            <tr style="cursor:pointer" onclick="javascript:move_url('<?php echo $TPL_V1["NOTICE_IDX"]?>');" >
                                <td><?php echo $TPL_VAR["start_no"]-$TPL_I1?></td>
                                <td>
                                    <?php echo $TPL_V1["NOTICE_TITLE"]?>

                                </td>
                                <td><?php echo substr($TPL_V1["NOTICE_CREDATE"], 0, 10)?></td>
                            </tr>
                        </tbody>
<?php }}?>
                    </table>
                </div>

                <?php echo $TPL_VAR["pagination"]?>


            </div>

        </div>

    </section>
    <form class="frmView" action="/contact/notice_view" method="get">
        <input type="hidden" name="per_page" id="per_page" value="<?php echo $TPL_VAR["per_page"]?>">
        <input type="hidden" name="notice_idx" id="notice_idx" value="">
    </form>
</article>
<script type="text/javascript">
    function move_url(idx) {
        $('#notice_idx').val(idx);
        $('.frmView').submit();
    }
</script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>