<?php /* Template_ 2.2.7 2018/03/07 17:30:12 /home/dartz/public_html/application/views/contact/faq_list.html 000006848 */ 
$TPL_code_list_1=empty($TPL_VAR["code_list"])||!is_array($TPL_VAR["code_list"])?0:count($TPL_VAR["code_list"]);
$TPL_type_list_1=empty($TPL_VAR["type_list"])||!is_array($TPL_VAR["type_list"])?0:count($TPL_VAR["type_list"]);
$TPL_faq_list_1=empty($TPL_VAR["faq_list"])||!is_array($TPL_VAR["faq_list"])?0:count($TPL_VAR["faq_list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<script>
    function search_frm(type){
        $("#F_TYPE").val(type);
        $("#frm_search").submit();
    }

    function search_submit() {
        $('#search_frm').submit();
    }

    function view_cnt(id) {
            $.ajax({
                type: "GET",
                url: "/contact/view_chk",
                dataType:'json',
                data: {id:id},
                success: function (result) {

                }
            });

    }

</script>
<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/contact/contact_index">Contact</a><span>/</span><strong>FAQ</strong>
    </div>
    <section id="contact" class="page_wrap" >
        <div class="container">
            <div class="page_top">
                <h4>FAQ</h4>
                <p>자주 문의하시는 질문과 답변을 빠르고 편리하게 확인하세요.</p>
            </div>
        </div>
        <div class="<?php if(($TPL_VAR["method"]=="faq_list")){?>faq_wrap<?php }?>"  >
            <form class="frmEstimate" name="search_frm" id="search_frm"  method="get" action="/contact/faq_list">
                <div class="faq_search">
                    <select class="ipt_box" name="F_TYPE">
                        <option value="" <?php if($TPL_VAR["F_TYPE"]==""){?>selected="selected"<?php }?>>전체</option>
<?php if($TPL_code_list_1){foreach($TPL_VAR["code_list"] as $TPL_V1){?>
                        <option value="<?php echo $TPL_V1["C_CODE_PA"]?>" <?php if($TPL_V1["C_CODE_PA"]==$TPL_VAR["F_TYPE"]){?>selected="selected"<?php }?>><?php echo $TPL_V1["C_CODE_TXT"]?></option>
<?php }}?>
                    </select>
                    <input type="text" name="search_S" class="ipt_box" value="<?php echo $TPL_VAR["search_S"]?>" placeholder="궁금하신 내용을 입력해주세요.">
                    <button class="btn_srch" onclick="javascript:search_submit();">조회</button>
                </div>
            </form>
        </div>
        <div class="faq_list">
            <form id="frm_search" name="frm_search" method="get" action="/contact/faq_list">
                <input type="hidden" name="F_TYPE" id="F_TYPE" value="<?php echo $TPL_VAR["F_TYPE"]?>">
                <input type="hidden" name="search_text" id="search_text" value="">
                <div class="tab color">
                    <ul>
                        <li <?php if($TPL_VAR["F_TYPE"]==""){?> class="on"<?php }else{?>class=""<?php }?>><a href="javascript:search_frm('');"  role="button">전체</a></li>
<?php if($TPL_type_list_1){foreach($TPL_VAR["type_list"] as $TPL_V1){?>
                        <li <?php if($TPL_VAR["F_TYPE"]==$TPL_V1["C_CODE_PA"]){?> class="on"<?php }else{?>class=""<?php }?> role="button" ><a href="javascript:search_frm('<?php echo $TPL_V1["C_CODE_PA"]?>');"><?php echo $TPL_V1["C_CODE_TXT"]?></a></li>
<?php }}?>
                    </ul>
                    <script type="text/javascript">
                        $(function(){
                            $(".tab li a").on("click", function(){
                                var $tab = $(this).parent();
                                var _idx = $tab.index();
                                if(!$tab.hasClass("on")){
                                    $(".tab .on,.tab-contents.on").removeClass("on");
                                    $(".tab li:eq("+_idx+"),.tab-contents:eq("+_idx+")").addClass("on");
                                }
                            })
                        })
                    </script>
                </div>
            </form>
            <div class="container">
                <div class="acc-list">
                    <ul class="tab-contents on">
<?php if($TPL_VAR["faq_list"]){?>
<?php if($TPL_faq_list_1){foreach($TPL_VAR["faq_list"] as $TPL_V1){?>
                            <li>
                                <a href="#" role="button" onclick="accFn(this);return false;">
                                    <span class="sort">Q</span>
                                    <span class="tit" onclick="javascript:view_cnt('<?php echo $TPL_V1["F_IDX"]?>');">[<?php echo $TPL_V1["faq_type_txt"]?>] <?php echo nl2br($TPL_V1["F_QDATA"])?></span>
                                </a>
                                <div class="answer">
                                    <span class="sort">A</span>
                                    <span class="txt">
                                            <?php echo nl2br($TPL_V1["F_ADATA"])?>

                                        </span>
                                </div>
                            </li>
<?php }}?>
<?php }else{?>
<?php if($TPL_VAR["search_S"]!=""){?>
                                <script type="text/javascript" language="JavaScript">
                                    $("#search_text").val("N");
                                </script>
<?php }?>

                            <li>
                                <span class="txt" style="font-size: 16px; text-align: center">등록 된 정보가 없습니다.</span>
                            </li>
<?php }?>
                    </ul>
                    <script type="text/javascript">
                        function accFn(obj){
                            var $tg = $(obj).closest("li");
                            var $answer = $tg.find(".answer")
                            $tg.toggleClass("on");
                            var _h = $answer.find(".txt").outerHeight();
                            if($tg.hasClass("on")) $answer.css({'height':_h})
                            else $answer.css({'height':0});
                        }
                    </script>
                </div>
            </div>
        </div>
    </section>
</article>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>

<script type="text/javascript" language="JavaScript">
    window.onload = function()
    {
        var search_text = $("#search_text").val();
        if(search_text =="N"){
            nonalert();
        }else{

        }
    }
    function nonalert(){
        alert("검색하신 내용의 정보가 없습니다.");
    }
</script>