<?php /* Template_ 2.2.7 2018/02/22 10:07:40 /home/dartz/public_html/application/views/contact/contact_index.html 000001390 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/contact/contact_index">Contact</a><span>/</span><strong>고객센터</strong>
    </div>
    <section id="contact" class="page_wrap">
        <div class="container">
            <div class="page_top">
                <h4>고객지원센터</h4>
                <p>문의사항이나 불편사항을 빠르게 도와 드리겠습니다.</p>
            </div>
            <div class="contact_info_box">
                <div class="box">
                    <p class="txt">1 : 1 문의를 남겨주시면<br>답변을 받아 보실 수 있습니다.</p>
                    <a href="/contact/qna_index" class="btn">1:1문의</a>
                </div>
                <div class="box box_faq">
                    <p class="txt">자주 문의하시는 질문과 답변을<br>빠르고 편리하게 확인하세요.</p>
                    <a href="/contact/faq_list" class="btn">FAQ확인</a>
                </div>
            </div>

        </div>
    </section>
</article>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>