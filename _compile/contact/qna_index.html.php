<?php /* Template_ 2.2.7 2018/03/09 15:49:33 /home/dartz/public_html/application/views/contact/qna_index.html 000005905 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/contact/contact_index">Contact</a><span>/</span><a href="/contact/contact_index">고객센터</a><span>/</span><strong>1:1문의</strong>
    </div>
    <section id="contact" class="page_wrap">
        <div class="container">
            <div class="page_top">
                <h4>1:1문의</h4>
                <p>문의하신 내용과 답변은 MY메뉴 1 : 1 문의 내역에서 확인 가능합니다.</p>
            </div>
        </div>
        <div class="qna_form">
            <form name="frm_input" id="frm_input" role="form" method="post" action="/contact/qna_process" enctype="multipart/form-data">
                <div class="container">
                        <div class="tb">
                            <table>
                                <colgroup>
                                    <col width="200px">
                                    <col width="*">
                                </colgroup>
                                <tbody>
                                <tr>
                                    <th><label>이름</label></th>
                                    <td>
                                        <input type="text" name="qna_name" id="qna_name" maxlength="12" value="<?php echo $TPL_VAR["name"]?>">
                                    </td>
                                </tr>
                                <tr>
                                    <th><label>입력</label></th>
                                    <td>
                                        <textarea class="ipt_box" id="qna_question" name="qna_question" placeholder="문의 하실 내용을 입력하세요." onfocus="if(this.value==''){this.value='';this.style.color='#333';}" ></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <th><label>파일첨부</label></th>
                                    <td>
                                        <div class="filebox">
                                            <input id="filenameView"class="upload-name ipt_box sz_xl" placeholder="첨부파일명" disabled="disabled" >
                                            <label for="QNA_QUESTION_FILENAME">파일 찾기</label>
                                            <input type="file" id="QNA_QUESTION_FILENAME" name="QNA_QUESTION_FILENAME" class="upload-hidden">
                                        </div>

                                        <script type="text/javascript">
                                            $(function(){
                                                $("#QNA_QUESTION_FILENAME").on("change", filename );
                                            })

                                            function filename(){
                                                var fileValue = $("#QNA_QUESTION_FILENAME").val().split("\\");
                                                var fileName = fileValue[fileValue.length-1];
                                                $("#filenameView").val(fileName);
                                            }
                                        </script>


                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    <div class="btn_wrap">
                        <a href="javascript:frmsubmit('Y');" role="button" class="btn btn_next"><span>문의 등록</span></a>
                    </div>
                </div>
            </form>
        </div>

    </section>
</article>

<script>
//    function change_txt(){
//        $("#qna_email_2").val($("#qna_email_sel").val());
//
//    }

    function chk_input(){
        var reg_email = /^[A-Za-z0-9]*$/;
        var reg_email2 = /^([\da-z\.-]+)\.([a-z\.]{2,6})$/;

        if($('#qna_name').val() == ''){
            alert("이름을 입력해주세요.");
            $('#qna_name').focus();
            return;
        }

        //var chk_content = document.getElementById("qna_question").value;
        if($.trim($("#qna_question").val())==''){
            alert("문의 내용을 입력해주세요.");
            $('#qna_question').focus();
            return;
        }

//        if($.trim($('#qna_email_1').val()) == '' || !reg_email.test($("#qna_email_1").val())){
//            alert("이메일을 입력해주세요.");
//            $('#qna_email_1').focus();
//            return;
//        }
//
//        if($.trim($('#qna_email_2').val()) == ''&& $('#qna_email_sel').val()=='' ){
//            alert("이메일을 입력해주세요.");
//            $('#qna_email_2').focus();
//            return;
//        }
//
//        if($.trim($('#qna_email_2').val()) != '' && !reg_email2.test($("#qna_email_2").val())){
//            alert("이메일을 입력해주세요.");
//            $('#qna_email_2').focus();
//            return;
//        }
        return true;
    }

    function frmsubmit(type) {
        if (type == "Y") {
            if (chk_input()) {

                $('#frm_input').submit();
            }
        } else {
            alert('다시 시도해 주시기 바랍니다.');
            location.reload();

            //location.href = '/contact/qna_index?per_page='+$('#per_page').val();
        }
    }

</script>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>