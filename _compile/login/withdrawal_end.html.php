<?php /* Template_ 2.2.7 2018/01/16 11:17:20 /home/dartz/public_html/application/views/login/withdrawal_end.html 000001254 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

    <article id="contents">
        
        <section id="mypage" class="page_wrap">
            <div class="container">
                <section class="complete">
                    <h4>회원탈퇴 처리가<br>정상적으로 완료되었습니다.</h4>
                    <hr>
                    <div class="txt">
                       아이디 <span class="color"><?php echo $TPL_VAR["M_ACCOUNT"]?></span>는 일루마 정책에 따라<br>
                       오늘부터 2년 동안은 해당 아이디로 재가입이 불가합니다.<br><br>
                       <strong>그 동안 일루마 홈페이지를 이용해주셔서 감사합니다.</strong>
                    </div>
                </section>
                <hr>
                <div class="btn_wrap">
                    <a href="/" class="btn btn_next"><span>메인으로</span></a>    
                </div>
            </div>
        </section>
    </article>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>