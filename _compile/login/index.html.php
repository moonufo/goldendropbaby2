<?php /* Template_ 2.2.7 2018/03/12 10:59:16 /home/dartz/public_html/application/views/login/index.html 000003375 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php if($TPL_VAR["isMember"]){?>
<script type="text/javascript">
	window.location.href="/"
</script>
<?php }?>
<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/js/login.js"></script>
<script type="text/javascript">$(document).ready(function() { submit_login(); });</script>
<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

	<article id="contents">
	<form name="login_input" id="login_input" action="/login/login_check" method="post">
	<input type="hidden" name="AuthState" value="<?php echo $TPL_VAR["AuthState"]?>" />
        <section id="login">
            <div class="container">
                <div class="login_wrap">
                    <div class="login_form">
                        <h3>로그인</h3>
                        <div class="form_wrap">
                            <div class="iptbox">
                                <input type="text" id="username" name="username" value="<?php echo $TPL_VAR["sv_sid"]?>" placeholder="아이디" class="ipt">
                                <p class="txt_caution id_txt"></p>
                                <!--*아이디를 입력해주세요-->
                            </div>
                            <div class="iptbox">
                                <input type="password" id="password" name="password" placeholder="비밀번호" class="ipt">
                                <p class="txt_caution pw_txt"></p>
                                <!--*비밀번호를 입력해주세요-->
                            </div>
                        </div>
                        <div class="login_util">
                            <label for="saveId" class="checkbox_wrap">
                                <input type="checkbox" id="saveId" value="Y" name="saveId" <?php if($TPL_VAR["sv_sid"]!=""){?> checked<?php }?>> 아이디 저장
                            </label>

                            <div class="btn_wrap">
                                <a href="/join/find_id" role="button" class="btn_txt btn_find_id"><span>아이디찾기</span></a>
                                <a href="/join/find_pw" role="button" class="btn_txt"><span>비밀번호찾기</span></a>
                            </div>
                        </div>
                        <a href="javascript:login_submit();" class="button btn_solid btn_login"><span>로그인</span></a>
                    </div>
                    <div class="login_join">
                        <h3>회원가입</h3>
                        <p class="txt">프리미엄 일루마의 친구가 되어 특별한 혜택을 누려보세요.</p>
                        <div class="banner">
                            <a href="/join/index" ><img src="/images/login/banner_join.png" alt=""></a>
                        </div>
                        <a href="/join/index" class="button btn_line btn_join"><span>회원가입</span></a>
                    </div>
                </div>
            	
                
            </div>
        </section>
	 </form>       
    </article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>