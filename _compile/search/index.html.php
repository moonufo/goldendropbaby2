<?php /* Template_ 2.2.7 2018/03/09 15:29:03 /home/dartz/public_html/application/views/search/index.html 000004926 */ 
$TPL_taglist_1=empty($TPL_VAR["taglist"])||!is_array($TPL_VAR["taglist"])?0:count($TPL_VAR["taglist"]);
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        <section id="search" class="page_wrap">
        	<form action="/search/index" method="get" name="frm_search" id="frm_search">
            <div class="container">
                <div class="page_top">
                    <h4>검색</h4>
                    <div class="search_box">
                        <input type="search" name="search_S" id="search_S" class="ipt_box" value="<?php echo $TPL_VAR["search_S"]?>">
    
                        <button onclick="search_typechk();" class="btn_srch">조회</button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="search_type" id="search_type" value="<?php echo $TPL_VAR["search_type"]?>">
            </form>
            <div class="search_list">
                <div class="tab">
                    <ul>                    	
                    	<li <?php if($TPL_VAR["search_type"]=="text"||$TPL_VAR["search_type"]==''){?> class="on"<?php }?>><a href="#" onclick="frm_submit('text');" role="button"><span>Text</span></a></li>
<?php if($TPL_taglist_1){foreach($TPL_VAR["taglist"] as $TPL_V1){?>
                    	<li <?php if($TPL_VAR["search_type"]==$TPL_V1["ST_IDX"]){?> class="on"<?php }?>><a href="#"  onclick="frm_submit('<?php echo $TPL_V1["ST_IDX"]?>');" role="button"><span><?php echo $TPL_V1["ST_TITLE"]?></span></a></li>
<?php }}?>
                    </ul>
                    <script type="text/javascript">
                        $(function(){
                            $(".tab li a").on("click", function(){
                                var $tab = $(this).parent();
                                var _idx = $tab.index();
                                if(!$tab.hasClass("on")){
                                    $(".tab .on,.tab-contents.on").removeClass("on");    
                                    $(".tab li:eq("+_idx+"),.tab-contents:eq("+_idx+")").addClass("on");
                                }
                            })
                        })
                    </script>
                </div>
<?php if($TPL_VAR["search_type"]=="text"||$TPL_VAR["search_type"]==""){?>
                <div class="container">
                    <div class="tab-contents on text_list">
                    	<ul class="list">
<?php if(count($TPL_VAR["list"])== 0){?>
                    		<li><span class="tit none noline">검색 된 내용이 없습니다.</span></li>
<?php }else{?>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
	                       	<li>
						        <a href="<?php echo $TPL_V1["url"]?>" role="button" target="_blank">
						            <span class="tit"><?php echo $TPL_V1["title"]?></span>
						            <span class="txt"><?php echo $TPL_V1["content"]?></span>
						        </a>
						    </li>
<?php }}?>
<?php }?>
						</ul>
						<?php echo $TPL_VAR["pagination"]?>

                    </div>

                    <div class="tab-contents  tag_list">
                        
                    </div>
                </div>
<?php }else{?>
                <div class="container">
                    <div class="tab-contents text_list">
                       	
                    </div>

                    <div class="tab-contents on tag_list">
                        <ul class="list">
<?php if(count($TPL_VAR["list"])== 0){?>
                    		<li><span class="tit none noline">검색 된 내용이 없습니다.</span></li>
<?php }else{?>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
	                       	<li>
						        <a href="<?php echo $TPL_V1["url"]?>" role="button" target="_blank">
						            <span class="tit"><?php echo $TPL_V1["title"]?></span>
						            <span class="txt"><?php echo $TPL_V1["content"]?></span>
						        </a>
						    </li>
<?php }}?>
<?php }?>
						</ul>
						<?php echo $TPL_VAR["pagination"]?>

                    </div>
                </div>
<?php }?>
            </div>
        </section>
    </article>
    <script>
    function search_typechk(){
    	$("#search_type").val("text");
    } 
    
    function frm_submit(type){
    	$("#search_type").val(type);
    	$("#frm_search").submit();
    }
    $(document).ready(function() {
    	//X 택스트,T 태그
    	//get_page('X');
    	//_page('T');  
	 });

    </script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>