<?php /* Template_ 2.2.7 2018/02/26 10:45:37 /home/dartz/public_html/application/views/search/search_data.html 000001314 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<ul class="list">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
	<li>
        <a href="#" role="button">
            <span class="tit"><?php echo $TPL_V1["TSE_TITLE"]?></span>
            <span class="txt"><?php echo $TPL_V1["TSE_CONTENT"]?></span>
        </a>
    </li>
<?php }}?>
    
</ul>
<div class="paging">
    <ul>
        <li class="btn_nav first"><a href="#" class="disable">처음 페이지</a></li><li class="btn_nav prev"><a href="#" class="disable">이전 페이지</a></li><li class="num"><a href="#" class="left on">1</a></li><li class="num"><a href="#">2</a></li><li class="num"><a href="#">3</a></li><li class="num"><a href="#">4</a></li><li class="num"><a href="#">5</a></li><li class="num"><a href="#">6</a></li><li class="num"><a href="#">7</a></li><li class="num"><a href="#">8</a></li><li class="num"><a href="#">9</a></li><li class="num"><a href="#" class="right">10</a></li><li class="btn_nav next"><a href="#" class="">다음 페이지</a></li><li class="btn_nav last"><a href="#" class="">마지막 페이지</a></li>
    </ul>
</div>