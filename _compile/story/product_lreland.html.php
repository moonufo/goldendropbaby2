<?php /* Template_ 2.2.7 2018/02/28 10:43:09 /home/dartz/public_html/application/views/story/product_lreland.html 000005311 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        <div class="location">
            <a href="/">Home</a><span>/</span><a href="/story/brand">illuma Story</a><span>/</span><a href="/story/product">Product Story</a><span>/</span><strong>Ireland Story</strong>
        </div>
        <section id="story" class="page_wrap">
            <div class="irelandstory">
                <div class="container">
                    <section>
                        <div class="toptxt">
                            <h3>우수한 자연환경과 체계적인 시스템으로 생산되는 아일랜드 원유</h3>
                        </div>
                        <div class="contents">
                            <div class="video">
                                <a href="#" class="btn_video"><img src="/images/story/ireland_movie.png" alt=""></a>
                            </div>

                            <div class="coltxt">
                                <dl>
                                    <dt>영양이 풍부한 초목</dt>
                                    <dd>4월에서 9월의 초목을 먹은 젖소로부터<br>원유를 수집합니다.</dd>
                                </dl>
                                <dl>
                                    <dt>저밀도 자연 방목</dt>
                                    <dd>운동장 넓이의 들판에서<br>단 두 마리의 소만 키우는<br>저밀도 자연 방목 방식을 고집합니다.</dd>
                                </dl>
                                <dl>
                                    <dt>우수한 자연환경</dt>
                                    <dd>비옥한 토양을 바탕으로<br>낙농업에 적합한 자연환경을<br>가지고 있습니다.</dd>
                                </dl>
                            </div>
                        </div>
                    </section>
                    <section class="sec02">
                        <div class="toptxt">
                            <h3>깨끗하고 맑은 공기와 풍부한 수자원을 가진 아일랜드</h3>
                            <p class="sub">아일랜드의 물 가용 지수는 세계 상위 수준이며, <strong>EU 국가 중 가장 낮은 이산화탄소 배출국</strong>입니다.</p>
                        </div>
                        <div class="graph_wrap">
                            <img src="/images/story/ireland_graph1.png" alt="EU 국가별 탄소 배출량">
                            <img src="/images/story/ireland_graph2.png" alt="연평균 도시 공기 오염도 PM10 (ug/m3)">
                        </div>
                    </section>

                    <section class="sec03">
                        <div class="toptxt">
                            <h3>아일랜드 국가 차원의 식품안전시스템</h3>
                        </div>
                        <div class="contents">
                            <p><img src="/images/story/ireland_og.png" alt="IRELAND Quality Product ORIGIN GREEN"></p>
                            <div class="txt">
                                <span><img src="/images/story/og_txt.png" alt="origin Green"></span>오리진 그린은 식음료 산업을 위한 2012년부터 시작된 아일랜드 국가 차원의 지속가능성 프로그램입니다.<br>
                                정부, 민간 부문과 식품 생산자를 아일랜드 국가 차원에서 운영하는 프로그램으로 Bord Bia 보드 비아를 통해 운영됩니다.<br>
                                단계 별로 독립적인 검증작업이 이루어지는 <span><img src="/images/story/og_txt.png" alt="origin Green"></span>  오리진 그린을 통해 아일랜드의 농장 및 식품 생산자는<br> 측정 가능한 장기적인 목표를 수립하고 달성하며, 깨끗한 자연환경이 유지되도록 기여 합니다.
                            </div>
                            <h4>소 신원, 이력 추적 (여권 시스템)</h4>
                            <div class="txt">
                                소 이름표 Bovine Tagging 보바인 태깅을 부착하고 신원확인 증서와 여권을 발급하여 체계적으로 관리하고,<br>이를 농장에 등록하도록 하며 모든 관리는 동물 신원, 이력 추적 데이터 베이스<br>AIM (Animal Identification and Movement database)을 통해 이루어집니다.
                                <div class="img"><img src="/images/story/img_cow.png" alt=""></div>
                            </div>

                        </div>
                    </section>
                    <div class="btn_wrap">
                        <a href="#" class="btn btn_solid"><span>Bord Bia 더 알아보기</span></a>
                        <a href="#" class="btn btn_solid"><span>Origin Green 더 알아보기</span></a>
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>