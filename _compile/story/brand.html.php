<?php /* Template_ 2.2.7 2018/02/28 10:29:13 /home/dartz/public_html/application/views/story/brand.html 000002370 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        <div class="location">
            <a href="/">Home</a><span>/</span><a href="/story/brand">illuma Story</a><span>/</span><strong>Brand Story</strong>
        </div>
        <section id="story" class="page_wrap">
            <div class="brandstory">
                <div class="topimg"></div>
                <div class="container">
                    <div class="toptxt">
						<h3>“사랑하는 아가야, 너는 세상을 이끌어라.<br>너의 무한한 가능성으로 세상의 가능성을 키워라”</h3>
                    </div>
                    <div class="txt">
                        엄마는 아기와 첫 번째로 교감하여<br>
                        아기가 가진 가능성을 발견하고 키워주는 중요한 역할을 합니다.<br>
                        일루마가 추구하는 엄마의 모습은 자신의 삶에 만족하고 자신감 있으며,<br>
                        아기에게 내재된 무한한 가능성을 인지하고 있습니다. <br>
                        엄마는 아기의 성장에 중요한 시기를 놓치지 않고 필요한 영양을 아기에게 줌으로써,<br>
                        아기의 가능성이 발견될 수 있도록 도와줍니다.<br><br>
                        우리 아기는 자라서 어떤 어른이 될까요?<br>
                        일루마는 자연으로부터 영감을 받아 과학을 통해 본질을 연구합니다.<br>
                        현대 기술을 바탕으로 지방의 분자 구조적 측면을 확인하고 실험하여<br>
                        아기에게 균형 잡힌 영양소를 제공하고, 스스로 건강하게 자라날 수 있도록 돕고자 합니다.<br><br>
                        “아기의 1000일은 100년을 위한 소중한 시간이니까”

                        <strong>아일랜드 원유와 OPO구조로 프리미엄 일루마를 완성하다</strong>
                    </div>
                </div>
            </div>
        </section>
    </article>

<?php $this->print_("layout_footer",$TPL_SCP,1);?>