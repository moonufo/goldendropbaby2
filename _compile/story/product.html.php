<?php /* Template_ 2.2.7 2018/02/28 10:37:19 /home/dartz/public_html/application/views/story/product.html 000002182 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/story/brand">illuma Story</a><span>/</span><strong>Product Story</strong>
    </div>
    <section id="story" class="page_wrap">
        <div class="productstory">
            <div class="container">
                <div class="toptxt">
                    <h3>스스로 건강하게 자라날 수 있도록<br>성장에 필요한 충분한 영양소를 배합한 일루마</h3>
                </div>
                <div class="txt">
                    <div class="coltxt">
                        <dl>
                            <dt>두뇌구성 &amp; 소화와 흡수</dt>
                            <dd>뇌 구성 성분이며<br>눈 건강에 도움을 주는 DHA와<br>장건강에 도움을 주는 프락토올리고당 함유</dd>
                        </dl>
                        <dl>
                            <dt>성장체계</dt>
                            <dd>칼슘의 흡수에 도움을 주는 비타민D와<br>아기의 발달에 필요한<br>타우린, 아라키돈산, 콜린 등을 함유</dd>
                        </dl>
                        <dl>
                            <dt>illumaopo™ <span>(sn–2팔미트산염)</span></dt>
                            <dd>지방을 분자 구조적 측면에서 확인하고<br>임상적으로 연구하여<br>칼슘과 지방의 흡수를 도와주는 OPO구조</dd>
                        </dl>
                    </div>
                </div>
                <div class="btn_wrap">
                    <a href="/story/product_lreland" class="btn btn_solid"><span>아일랜드 이야기</span></a>
                    <a href="/story/product_opo" class="btn btn_solid"><span>OPO 이야기</span></a>
                </div>

            </div>
        </div>
    </section>
</article>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>