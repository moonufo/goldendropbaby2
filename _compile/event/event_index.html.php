<?php /* Template_ 2.2.7 2018/03/07 14:12:57 /home/dartz/public_html/application/views/event/event_index.html 000003911 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>


<script type="text/javascript">
    function search_submit() {
        $('#search_frm').submit();
    }
 </script>

<article id="contents">
    <div class="location">
        <a href="/">Home</a><span>/</span><a href="/event/event_index">Goldendrop Members</a><span>/</span><strong>Event</strong>
    </div>
    <section id="event" class="page_wrap">
        <div class="container">
            <div class="thumb_list">
                <form class="frmEstimate" name="search_frm" id="search_frm"  method="get" action="/event/event_index">
                    <div class="list_search">
                        <div class="select ipt_box sz_md">
                            <span>전체</span>
                            <select name="search_date" id="search_date">
                                <option value="">전체</option>
                                <option value="chk_date_now" <?php if($TPL_VAR["search_date"]=="chk_date_now"){?> selected<?php }?>>진행 중 이벤트</option>
                                <option value="chk_date_pass" <?php if($TPL_VAR["search_date"]=="chk_date_pass"){?> selected<?php }?>>지난 이벤트</option>
                            </select>
                        </div>
                        <a href="javascript:search_submit();" class="btn btn_inner"><span>조회</span></a>
                    </div>
                    <script type="text/javascript">
                        $(function(){
                            var _txt = $("#search_date option:selected").text();
                            if(_txt){
                                $(".select span").html(_txt)
                            }
                        })
                    </script>
                </form>
                <ul class="col2">
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
                    <li onclick="javascript:move_url('<?php echo $TPL_V1["E_IDX"]?>');" <?php if((($TPL_I1+ 1)% 2)== 0){?>class="right"<?php }?>>
                        <a href="#">
                            <span class="img"><img src="http://dartz.swtown.co.kr<?php echo $TPL_V1["E_PATH"]?><?php echo $TPL_V1["E_FILENAME"]?>" alt="" style="width: 470px; height: 140px;"></span>
                            <span class="intro gd"><?php echo $TPL_V1["E_SUBJECT"]?></span>
                            <span class="tit bk"><?php echo $TPL_V1["E_TITLE"]?></span>
                            <span class="txt"><?php echo substr($TPL_V1["E_STRDATE"], 0, 10)?> ~ <?php echo substr($TPL_V1["E_ENDDATE"], 0, 10)?> <?php echo $TPL_V1["content_txt"]?></span>
                            <span class="date gd"><strong>No. <?php echo $TPL_VAR["start_no"]-$TPL_I1?></strong> <?php echo substr($TPL_V1["E_CREDATE"], 0, 10)?> </span>
                        </a>
                    </li>
<?php }}?>
                </ul>
                <?php echo $TPL_VAR["pagination"]?>

            </div>
        </div>
    </section>
    <form class="frmMod" action="/event/event_view" method="get">
        <input type="hidden" name="per_page" id="per_page" value="<?php echo $TPL_VAR["per_page"]?>">
        <input type="hidden" name="search_date" id="search_date" value="<?php echo $TPL_VAR["search_date"]?>">
        <input type="hidden" name="e_idx" id="e_idx" value="">
    </form>
</article>

<script type="text/javascript">
    function move_url(idx) {
        $('#e_idx').val(idx);
        $('.frmMod').submit();
    }

</script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>