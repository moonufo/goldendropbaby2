<?php /* Template_ 2.2.7 2018/03/09 19:26:33 /home/dartz/public_html/application/views/event/point_index.html 000003960 */ ?>
<?php $this->print_("layout_header",$TPL_SCP,1);?>

<?php $this->print_("menu_gnb",$TPL_SCP,1);?>

<article id="contents">
        <div class="location">
            <a href="#">Home</a><span>/</span><a href="/event/event_index">Goldendrop Members</a><span>/</span><strong>Point</strong>
        </div>
        <section id="point" class="page_wrap">
            <div class="container">
                <div class="page_top">
                    <h4>포인트 교환</h4>
                    <p>일루마 제품 구매 및 사이트 이용 시 포인트가 적립되며 적립한 포인트로 백화점 상품권 교환이 가능합니다.</p>
                </div>
<?php if(!$TPL_VAR["isMember"]){?>
                <div class="pt_view">
                    <p class="before_login"><strong>포인트 조회는 로그인 후 확인 가능합니다.</strong> <a href="/login/index" class="btn_inner"><span>로그인 하기</span></a></p>
                </div>
<?php }else{?>
                <div class="pt_view">
                    <p class="after_login">회원님 보유 포인트 <span><?php echo number_format($TPL_VAR["PL_TOTALPOINT"])?>p</span> <a href="/my/point_list/" class="btn_inner"><span>포인트 내역</span></a></p>
                </div>
<?php }?>

                <div class="page_body">
                    <section class="pt_change">
                        <div class="ticket"><img src="/images/members/pt_ticket.png" alt="신세계 50,000P"></div>
                        <div class="intxt">
                            <span class="title">포인트 교환 상품</span>
                            <span class="name">신세계 백화점 50,000원 상품권</span>
                            <span class="txt">교환 차감 포인트 50,000P</span>
                            <a href="javascript:point_use();" class="button btn_wh"><span>교환신청</span></a>
                        </div>    
                    </section>
                    
                    <section class="pt_infotxt">
                        <dl>
                            <dt>포인트 교환안내</dt>
                            <dd>-  포인트로 교환하신 모바일 상품권은 회원 정보에 입력 된 휴대폰 번호로  발송 됩니다.</dd>
                            <dd>- 모바일 상품권은 교환신청 후 차주 월요일에  일괄 발송 되며, 차주 월요일에 상품권이 도착하지 않을 경우 고객센터로  문의바랍니다.</dd>
                            <dd>- 포인트 교환 신청 후 교환취소 및 상품권 반품은 불가능 합니다. </dd>
                            <dd>- 회원 정보에 휴대폰 번호를 잘못 입력하여 타인에게 발송 된 경우 포인트 환급 및 재발송이 불가합니다.</dd>
                        </dl>
                    </section>
                </div>
                
            </div>
        </section>
    </article>

<script type="text/javascript">
	function point_use(){
<?php if(!$TPL_VAR["isMember"]){?>
			if(confirm("로그인이 필요한 서비스입니다.\n로그인 하시겠습니까?")){
				window.location.href="/login/index";
			}
			return;
<?php }?>
		$.get("/event/point_check/" ,
	    {},
	    function(rslt){
			var array_rslt = JSON.parse(rslt);
			if(array_rslt.Stat=="Y"){
				if(confirm("혜택상품으로 포인트 교환을 신청하시겠습니까?")){
					$.get("/event/point_use/" ,
					{},
					function(rslt2){
						var array_rslt2 = JSON.parse(rslt2);
						alert(array_rslt2.Msg);
                        location.reload();
					});
				}
			}else{
				alert(array_rslt.Msg);
                location.reload();
			}
	    });
	}
</script>
<?php $this->print_("layout_footer",$TPL_SCP,1);?>