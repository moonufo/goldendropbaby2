//벨리데이션 제한
function submit_login(){
		$.validator.addMethod("reid",  function( value, element ) {
		return this.optional(element) ||  /^[a-zA-Z0-9_\.]{4,12}$/.test(value);
	}); 

	var validate = $("#login_input").validate({
		//규칙 생성
		rules: {
			username: {required: true,reid:true },
			password: {required: true,rangelength: [6, 16]}
		},
		//에러 문구
		messages: {    
			username: {required: "아이디를 입력해 주세요.",reid:"아이디는 영문과 숫자로 4~10자리까지 입니다"},  
			password: {required: "비밀번호를 입력해 주세요.",rangelength: $.validator.format("최소 {0}글자 이상 {1}글자 이하로 입력하세요.")}
		},

		//에러 메시지 범위
		errorPlacement: function(error, element) {
			error.appendTo( element.closest(".errorBox") );
			
			
		},

		invalidHandler: function(form, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				if($("#username").val()==""){
				$(".id_txt").text(" *아이디를 입력해 주세요.");
			}else{
				
				if($("#username").val().length<3 || $("#username").val().length>11){
					$(".id_txt").text(" *아이디는 영문과 숫자로 4~10자리까지 입니다.");			
				}else{
					$(".id_txt").text("");	
				}
				
			}
			if($("#password").val()==""){
				$(".pw_txt").text(" *비밀번호를 입력해 주세요.");
			}else{
				if($("#password").val().length<5 || $("#password").val().length>17){
					$(".pw_txt").text(" *최소 6글자 이상 16글자 이하로 입력하세요.");
				}else{
					$(".pw_txt").text("");	
				}
				
			}
			alert(validator.errorList[0].message);
				validator.errorList[0].element.focus();
			}
		},

		submitHandler: function(form) {

			if ($('#saveId').is(':checked')){
				str_loginSave="Y";
			}else{
				str_loginSave="N";
			}
			$.post("/login/login_check/" ,{
				username:$('#username').val(),
				password:$('#password').val(),
				loginSave:str_loginSave,
			},
			function(rslt){
				
				var rslt2 = JSON.parse(rslt);
				
				if(rslt2.Stat){
					
					window.location.href=rslt2.Url;
				}else{
					
					
					alert(rslt2.Msg);
				}
			});


},
	});

    document.onkeydown=function(evt){
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if(keyCode == 13){
           login_submit();
        }
    }
  
}

function set_login(){
}

function login_submit(){
	$("#login_input").submit();
}

