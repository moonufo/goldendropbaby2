

	//리스트 화면 가져오기
	function get_pddata(type){
		var batch_cycle=$("#batch_cycle").val();
		var code=$("#product_code").val();
		$.get("/order/pddata" ,
	    {
	        code : code,
	        batch_cycle : batch_cycle
	    },
	    function(rslt){
	    	if(code==""){
	    		$(".prd_list").html("");
	    		$(".dis").addClass('disable');
				$(".dis").attr('disabled', 'disabled');
				$(".ipt_cal").val("");
				$("#batch_count").val("1");
				$("#batch_cycle").val("T");
				$(".delivery_date").html("<p class='txt'>정기 배송 주기/횟수 선택 시 일정 미리보기가 제공됩니다.</p>");
	    	}else{
	    		$(".prd_list").html(rslt);	
	    	}
	    	order_chk(type);
	    });
	}
	
	function cycle_chk(val){
		if(val=="T"){
			//console.log(val);
			$(".dis").addClass('disable');
			$(".dis").attr('disabled', 'disabled');
			$(".ipt_cal").val("");
			$("#batch_count").val("1");
			$(".delivery_date").html("<p class='txt'>정기 배송 주기/횟수 선택 시 일정 미리보기가 제공됩니다.</p>");
			orderheightresize();
			set_chk("O");	
		}
		else{
			$(".dis").removeClass('disable');
			$(".dis").removeAttr('disabled', 'disabled'); 
			set_chk("O");
		}
	}
	//정기 배송일정 가져오기
	function set_chk(type){
		//get_pddata(type);
		var batch_cycle=$("#batch_cycle").val();
		var batch_count=$("#batch_count").val();
		var batch_date=$.trim($(".ipt_cal").val());
		if(batch_cycle !="T"&& batch_count !=""&& batch_date !=""){
		$.get("/order/timedata" ,
	    {
	        batch_cycle : batch_cycle,
	        batch_count : batch_count,
	        batch_date : batch_date
	    },
	    function(rslt){
    		$(".delivery_date").html(rslt);	
    		order_chk(type);
    		//orderheightresize();
	    });
	    }else{
	    	order_chk(type);
	    }
	}
	//주문 최종 정보 가져오기
	function order_chk(type){
		
		var batch_cycle=$("#batch_cycle").val();
		var batch_count=$("#batch_count").val();
		var code=$("#product_code").val();
		
		$.get("/order/orderdata" ,
	    {
	        batch_cycle : batch_cycle,
	        batch_count : batch_count,
	        code : code,
	        type : type
	    },
	    function(rslt){
	    	$(".payment_info").html(rslt);	
	    	orderheightresize();
	    });
	    
	}

	//order_index submit
	function frm_submit(){
		var chk="Y";
		var batch_cycle=$("#batch_cycle").val();
		if($.trim($('#product_code').val()) == ''){
			alert("상품을 선택해주세요.");
			chk="N";
		}
		if($("#batch_cycle").val()!="T"){
			if($.trim($('.ipt_cal').val()) == ''){
				alert("결제일을 선택해주세요.");
				chk="N";
			}
		}
		if(chk=="Y"){
			
			if($("#batch_cycle").val()=="T"){
				$("#frm_input").attr("action", "/order/torder_paystep");
				$("#frm_input").submit();	
			}else{
				$("#frm_input").attr("action", "/order/torder_paystep_batch");
				$("#frm_input").submit();
			}
			
		}
	}
	//높이 수정
//높이 수정
	 function orderheightresize(){
        var $abox = $(".order_info");
        var $bbox = $(".payment_info");

        var $atg = $(".delivery_date");
        var $btg = $(".total .btn_next")

        $btg.css({'margin-top':20})
        $atg.css({'margin-bottom':0})

        var _ah = $abox.height();
        var _bh = $bbox.height();

        if(_ah > _bh){
            var mg = _ah -_bh+20;
            $btg.css({'margin-top':mg})
        }else if(_ah < _bh){
            var mg = _bh-_ah;
            $atg.css({'margin-bottom':mg})
        }
    }

	function get_post(){
    new daum.Postcode({
        oncomplete: function(data) {
            // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분입니다.
            // 예제를 참고하여 다양한 활용법을 확인해 보세요.
            $("#EP_batch_post").val(data.zonecode);
            $("#EP_batch_addr1").val(data.address);
        }
    }).open();
    }
