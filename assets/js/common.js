/* GNB */
$(function(){
    var $header = $("#header");
    var _onact = $("#header").hasClass("active");
    var $menu = $(".gnb .menu");
    var $dep2 = $(".gnb .dep2 > li");

    $menu.hover(function(e){
        $header.addClass("active");
        $(this).addClass("hover");
    },function(e){
        if(!_onact) $header.removeClass("active");
        $(this).removeClass("hover");
    })

    $dep2.hover(function(e){
    	$(this).addClass("hover");
    },function(e){
    	$(this).removeClass("hover");
	})

    selAct();

})

function selAct(){
    $("select").change(function(){
        var $sel = $(this).closest(".select");
        $sel.find("span").html($(this).find("option:selected" ).text());
    })
}


var modal = {
    open:function(obj){
        var $obj = $(".modal_"+obj);
        $obj.show();
    },
    close:function(obj){
        var $obj = $(".modal_"+obj);
        $obj.hide();  
    }
}

//comma
function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}
function onlyNumber(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9]/g,""));
    }); 
}
//2자리 맞추기
function pad(n, width) {
  var n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}
//
function alert_ready(){
	alert('준비중입니다.');
}
//준비중
