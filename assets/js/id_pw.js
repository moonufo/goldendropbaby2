	var SetTime = 180;		

	function msg_time() {	
		m = Math.floor(SetTime / 60) + " : " + pad((SetTime % 60),2) + "";	// 남은 시간 계산
		var msg = m;
		$("#chk_red").html(msg);
		$("#chk_red").addClass("timeline");
		$(".line_onoff").removeClass("on");
		$(".line_onoff").addClass("off"); 
		SetTime--;					
		if (SetTime == 0) {		
			clearInterval(tid);		
			out_chknum();
		}
		
	}
	function out_chknum(){
		$("#userhp").removeAttr("disabled");
		var userhp=$.trim($("#userhp").val());
		$.post("/join/out_chknum" ,
	    {
	        userhp : userhp
	    },
	    function(rslt){
	    	$("#chk_red").html('')
			$("#chk_red").removeClass("timeline");
			$(".line_onoff").removeClass("off"); 
			$(".line_onoff").addClass("on"); 
	    });
	}
	function TimerStart(){
		 
		if($("#textyn").val()=="Y"){
			SetTime=0;	
			$("#chk_red").removeClass("timeline");
			$(".line_onoff").addClass("on"); 
			$(".line_onoff").removeClass("off"); 
			$("#chk_red").html('')
			$("#textyn").val('N');
			clearInterval(tid);	
			TimerStart();
		}else{
			$("#textyn").val('Y');
			SetTime=180;
			tid=setInterval('msg_time()',1000)
		}
	 };
    function sms_send(type){
    	var userhp=$.trim($("#userhp").val());
    	if($("#hp_chk").val()=="Y"){
			alert("이미 인증되었습니다.");
    		return;
    	}
    	if(userhp!=""){
			out_chknum();
			$.post("/join/sms_insert" ,
		    {
		        userhp : userhp,
		        type : type
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="Y"){
		    		alert("인증번호가 발송되었습니다.");
		    		$("#userhp").attr("disabled",true);
		    		TimerStart();
		    	}else if(rslt=="X"){
		    		alert("인증번호발송은 일 5회까지만 가능합니다. 익일 다시 시도 해주세요");
	    		}else{
		    		alert("인증번호 발송에 실패했습니다.");
		    	}
		    });
	    }else{
	    	alert('휴대전화 번호가 필요합니다.');
	    }
    }
    function sms_chknum(){
    	var userhp=$("#userhp").val();
    	var chknum=$("#chknum").val();
    	if($("#hp_chk").val()=="Y"){
			alert("이미 인증되었습니다.");
    		return;
    	}
    	if(userhp!="" || chknum!="" ){
			$.post("/join/send_chknum" ,
		    {
		        userhp : userhp,
		        chknum : chknum
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="Y"){
		    		$("#userhp").attr("disabled",true);
					$("#chknum").attr("disabled",true);
					$("#hp_chk").val("Y");
					SetTime=0;	
					$("#chk_red").removeClass("timeline");
					$(".line_onoff").removeClass("off"); 
					$(".line_onoff").addClass("on"); 
					$("#chk_red").html('')
					$("#textyn").val('N');
					clearInterval(tid);	
		    		alert("휴대폰 인증이 완료되었습니다.");
		    	}else{
		    		alert("휴대폰 인증이 실패했습니다.");
		    	}
		    	
		    });
	    }else{
	    	alert('휴대전화 번호와 인증번호가 필요합니다.');
	    }
    }
    
	function chk_hp(type){

    	var userhp=$("#userhp").val();
 		
    	if($("#hp_chk").val()!="Y"){
    		alert("인증이 필요합니다.");
    		return;
    	}else{
   			if(type=="id"){
		    	var username=$("#username").val();

				if($.trim(username)==""){
		 			alert('이름을 입력해주세요');
		 			return; 			
		 		}		    	
		 		$("#userhp").removeAttr("disabled");
		    	$("#chknum").removeAttr("disabled");
				$.post("/join/find_id_hpchk" ,
			    {
			        username : username,
			        userhp : userhp
			    },
			    function(rslt){
			    	rslt=$.trim(rslt);
		    		$("#userhp").attr("disabled",true);
					$("#chknum").attr("disabled",true);
					if(rslt=="false"){
						$(".find_info").css("display","block");
						$(".find_info").html("회원님의 아이디를 찾을 수 없습니다.<br/>이름과 휴대전화를 재확인해주시기 바랍니다.");
					}else{
				    	$(".find_info").css("display","block");
			    		$(".find_info").html("회원님의 아이디는 <span class='getid'>"+rslt+"</span> 입니다.");
			    		
				    }
			    });
		    }else{
		    	var useraccount=$("#useraccount").val();
		 		if($.trim(useraccount)==""){
		 			alert('아이디를 입력해주세요');
		 			return; 			
		 		}
		    	if($("#hp_chk").val()!="Y"){
		    		alert("인증이 필요합니다.");
		    		return;
		    	}else{
		   
			    	$("#userhp").removeAttr("disabled");
			    	$("#chknum").removeAttr("disabled");
			    	  
					$.post("/join/find_pw_hpchk" ,
				    {
				        useraccount : useraccount,
				        userhp : userhp
				    },
				    function(rslt){
				    	rslt=$.trim(rslt);
			    		$("#userhp").attr("disabled",true);
						$("#chknum").attr("disabled",true);
						if(rslt=="false"){
							alert("회원님의 아이디를 찾을 수 없습니다.\n이름과 휴대전화를 재확인해주시기 바랍니다.");
						}else{
					   		alert("임시비밀번호가 발송되었습니다.");
					    }
				    });
			    }
			}
	    }
    }