    $(function(){
		$( "#M_ACCOUNT" )
			.focusout(function() {
				chk_id('M');
		})
		
		$( "#M_PW" )
			.keyup(function() {
			var chk=chkPwd($.trim($('#M_PW').val()));
			if(chk==true){
			$(".msg_M_PW").text("");
			$(".td_M_PW").removeClass("caution");	
			}
		})
		$( "#M_PWCHK" )
			.keyup(function() {
			var chk=$.trim($('#M_PW').val());
			var rechk=$.trim($('#M_PWCHK').val());
			if(rechk==""){
				$(".msg_M_PWCHK").text("* 비밀번호를 한번 더 입력해주세요");
				$(".td_M_PWCHK").addClass("caution");
			}else if(chk==rechk){
				$(".msg_M_PWCHK").text("");
				$(".td_M_PWCHK").removeClass("caution");	
			}else{
				$(".msg_M_PWCHK").text("* 입력하신 비밀번호가 동일하지 않습니다.");
				$(".td_M_PWCHK").addClass("caution");
			}
		})
	});
	function chk_name(){
		var reg =/([^가-힣\x20a-zA-Z])/i;
 		var uid=$('#M_NAME').val();
 		
 		if(reg.test(uid)){
			$(".msg_M_NAME").text("* 이름을 정확히 입력해주세요.");
			$(".td_M_NAME").addClass("caution");
			$('#M_NAMEYN').val('N');
		}else{
			if($.trim(uid).length>2&&$.trim(uid).length<11){
				$(".msg_M_NAME").text("");
				$(".td_M_NAME").removeClass("caution");
				$('#M_NAMEYN').val('Y');	
			}else{
				$(".msg_M_NAME").text("이름은   3~10자리로 사용할 수 있습니다");
				$(".td_M_NAME").addClass("caution");;
				$('#M_NAMEYN').val('N');
			}
		}
	}
	function change_txt(){
		$("#M_EMAIL_2_txt").val($("#M_EMAIL_2_sel").val());
	}
	function chk_data(){
    	$('#M_ACCOUNTYN').val('N');
    }
	function chk_id(type){
		
		if(type=="M"){
			var strReg = /^[a-zA-Z0-9]{6,15}$/; 
		
			var uid=$('#M_ACCOUNT').val();
			if(strReg.test(uid)) {
				var chk_num = uid.search(/[0-9]/g);
			    var chk_eng = uid.search(/[a-z]/ig);
			    if(chk_num < 0 || chk_eng < 0 ) {
			    	$(".msg_M_ACCOUNT").text("* 아이디는 영문, 숫자를 조합한  6~15자리로 사용할 수 있습니다.");
			    	$(".td_M_ACCOUNT").addClass("caution");
			    }else{
		    		$.get("/join/userAccount_chk" ,
				    {
				        M_ACCOUNT : uid
				    },
				    function(rslt){
				    	rslt=$.trim(rslt);
				    	if(rslt=="Y"){
				    		$(".msg_M_ACCOUNT").text("* 사용가능한 아이디입니다.");
				    		$(".td_M_ACCOUNT").removeClass("caution");
				    		$('#M_ACCOUNTYN').val('Y');
				    	}else{
				    		$(".msg_M_ACCOUNT").text("* 이미 사용중인 아이디 입니다.");
				    		$(".td_M_ACCOUNT").addClass("caution");
				    		$('#M_ACCOUNTYN').val('N');
				    	}
				    });
			
			    }
				
			}else{
				$(".msg_M_ACCOUNT").text("* 아이디는 영문, 숫자를 조합한  6~15자리로 사용할 수 있습니다.");
				$(".td_M_ACCOUNT").addClass("caution");
			}
		}else{
			var re_uid=$('#M_RECOMMEND').val();
			if(re_uid!=''){
			$.get("/join/recommend_chk" ,
			    {
			        M_ACCOUNT : re_uid
			    },
			    function(rslt){
			    	rslt=$.trim(rslt);
			    	if(rslt=="Y"){
			    		$(".msg_M_RECOMMEND").text("* 추천인ID가 확인되지 않습니다.");
			    		$(".td_M_RECOMMEND").addClass("caution");
			    		$(".td_M_RECOMMEND").addClass("chkid");
			    	}else if(rslt=="X") {
			    		$(".msg_M_RECOMMEND").text("* 이미 추천된 아이디입니다.");
			    		$(".td_M_RECOMMEND").addClass("caution");
			    		$(".td_M_RECOMMEND").addClass("chkid");
			    	}else{
			    		$(".msg_M_RECOMMEND").text("");
			    		$(".td_M_RECOMMEND").removeClass("caution");
			    		$(".td_M_RECOMMEND").removeClass("chkid");
			    	}
			    });
			}else{
				$(".msg_M_RECOMMEND").text("");
				$(".td_M_RECOMMEND").addClass("caution");
				$(".td_M_RECOMMEND").addClass("chkid");
			}
		}
	}
	function chkPwd(pwd) {
		var pw = pwd;
		var num = pw.search(/[0-9]/g);
		var eng = pw.search(/[a-z]/ig);
		var spe = pw.search(/[.!@#$%]/gi);
		var nospe = pw.search(/[`~^&*|\\\'\";:\/?]/gi);

	
		if (pw.length < 8 || pw.length > 20) {
			
			$(".msg_M_PW").text("* 비밀번호는 영문,숫자,특수문자(.!@#$%)를 조합한 8자~20자로 사용하실 수 있습니다.");
			$(".td_M_PW").addClass("caution");
			return;
		}
		if (pw.search(/₩s/) != -1) {
			
			$(".msg_M_PW").text("* 비밀번호는 영문,숫자,특수문자(.!@#$%)를 조합한 8자~20자로 사용하실 수 있습니다.");
			$(".td_M_PW").addClass("caution");
			return;
		}
		if (num < 0 || eng < 0 || spe < 0 || nospe >-1) {
			
			$(".msg_M_PW").text("* 비밀번호는 영문,숫자,특수문자(.!@#$%)를 조합한 8자~20자로 사용하실 수 있습니다.");
			$(".td_M_PW").addClass("caution");
			return;
		}
		return true;
	
	}
	
//최종 확인
function frm_input() {
	var chk="Y";
	
	if($.trim($('#M_ACCOUNT').val()) == ''){
		$(".msg_M_ACCOUNT").text("* 아이디를 입력해주세요.");
		$(".td_M_ACCOUNT").addClass("caution");
		chk="N";
	}else{
		$(".msg_M_ACCOUNT").text("");
		$(".td_M_ACCOUNT").removeClass("caution");
	}
	var uid=$('#M_ACCOUNT').val();
	if(uid.length < 6 || uid.length > 15){
		$(".msg_M_ACCOUNT").text("* 아이디는 영문, 숫자를 조합한 6~15자리로 사용할 수 있습니다.");
		$(".td_M_ACCOUNT").addClass("caution");
		chk="N";
	}else{
		$(".msg_M_ACCOUNT").text("");
		$(".td_M_ACCOUNT").removeClass("caution");
	}
	if($('#M_ACCOUNTYN').val() == 'N'){
		$(".msg_M_ACCOUNT").text("* 아이디 중복확인을 해주세요.");
		$(".td_M_ACCOUNT").addClass("caution");
		chk="N";
	}else{
		$(".msg_M_ACCOUNT").text("");
		$(".td_M_ACCOUNT").removeClass("caution");
	}
	if($('#M_NAMEYN').val() == 'N'){
		$(".msg_M_NAME").text("* 이름을 확인해 해주세요.");
		$(".td_M_NAME").addClass("caution");
		chk="N";
	}else{
		$(".msg_M_NAME").text("");
		$(".td_M_NAME").removeClass("caution");
	}
	
	var Reg = /[\s]/g;
	var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if( Reg.test($('#M_EMAIL_1').val())==true || $('#M_EMAIL_1').val().length < 2){
		$(".msg_M_EMAIL").text("* 정확한 이메일 주소를 입력해주세요.");
		$(".td_M_EMAIL").addClass("caution");
		chk="N";
	}else if($.trim($('#M_EMAIL_2_txt').val()) == ''&& $('#M_EMAIL_2_sel').val()==''){
		$(".msg_M_EMAIL").text("* 정확한 이메일 주소를 입력해주세요.");
		$(".td_M_EMAIL").addClass("caution");
		chk="N";
	}else{
		$(".msg_M_EMAIL").text("");
		$(".td_M_EMAIL").removeClass("caution");
	}
	var email= $('#M_EMAIL_1').val()+"@"+$('#M_EMAIL_2_txt').val();
	 if(!regEmail.test(email)) {
	        $(".msg_M_EMAIL").text("* 이메일 입력 형식을 확인해주세요.");
			$(".td_M_EMAIL").addClass("caution");
			chk="N";
    }else{
		$(".msg_M_EMAIL").text("");
		$(".td_M_EMAIL").removeClass("caution");
	}


	if($.trim($('#M_PW').val()) == ''){
		$(".msg_M_PW").text("* 비밀번호를 입력해주세요");
		$(".td_M_PW").addClass("caution");
		chk="N";
	}else if (!chkPwd($.trim($('#M_PW').val()))) {
		chk="N";
	}else{
		$(".msg_M_PW").text("");
		$(".td_M_PW").removeClass("caution");
	}
	
	if($.trim($('#M_PWCHK').val()) == ''){
		$(".msg_M_PWCHK").text("* 비밀번호를 한번 더 입력해주세요");
		$(".td_M_PWCHK").addClass("caution");
		chk="N";
	}else if($('#M_PW').val()!=$('#M_PWCHK').val()){
		$(".msg_M_PWCHK").text("* 입력하신 비밀번호가 동일하지 않습니다.");
		$(".td_M_PWCHK").addClass("caution");
		chk="N";
	}else{
		$(".msg_M_PWCHK").text("");
		$(".td_M_PWCHK").removeClass("caution");
	}
	
	if($("#hp_chk").val()!="Y"){
		$(".msg_M_HP").text("* 휴대전화 인증이 필요합니다.");
		$(".td_M_HP").addClass("caution");
		chk="N";
	}

	if(chk=="Y"){
    	if($(".td_M_RECOMMEND").hasClass("chkid") === true) {
			$("#M_RECOMMEND").val("");
		}
		$("#M_HP_1").removeAttr("disabled");
		$("#M_HP_2").removeAttr("disabled");
		$("#M_HP_3").removeAttr("disabled");
		$("#frm_input").submit()
	}
	
}
//문자발송
function sms_send(){
    	var userhp=$("#M_HP_1").val()+"-"+$("#M_HP_2").val()+"-"+$("#M_HP_3").val();
    	if($("#hp_chk").val()=="Y"){
			alert("이미 인증되었습니다.");
    		return;
    	}
    	if($.trim($("#M_HP_2").val())!="" || $.trim($("#M_HP_3").val())!="" ){
    		type="join";
    		out_chknum();
			$.post("/join/sms_insert" ,
		    {
		        userhp : userhp,
		        type : type
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="Z"){
	    			alert("이미 가입되어있는 번호입니다.");
	    		}else if(rslt=="X"){
		    		alert("인증번호발송은 일 5회까지만 가능합니다. 익일 다시 시도 해주세요.");
	    		}else if(rslt=="Y"){
		    		$("#M_HP_1").attr("disabled",true);
		    		$("#M_HP_2").attr("disabled",true);
		    		$("#M_HP_3").attr("disabled",true);
		    		TimerStart();
		    		alert("인증번호가 발송되었습니다.");
		    	}else{
		    		alert("인증번호 발송에 실패했습니다.");
		    	}
		    });
	    }else{
	    	alert('휴대폰번호를 입력해주세요.');
	    }
    }
    function sms_chknum(){
    	var userhp=$("#M_HP_1").val()+$("#M_HP_2").val()+$("#M_HP_3").val();
    	var chknum=$("#chknum").val();
    	if($("#hp_chk").val()=="Y"){
			alert("이미 인증되었습니다.");
    		return;
    	}
    	if(userhp!="" || chknum!="" ){
			$.post("/join/send_chknum" ,
		    {
		        userhp : userhp,
		        chknum : chknum
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="Y"){
		    		$("#M_HP_1").attr("disabled",true);
		    		$("#M_HP_2").attr("disabled",true);
		    		$("#M_HP_3").attr("disabled",true);
					$("#chknum").attr("disabled",true);
					$("#hp_chk").val("Y");
					SetTime=0;
					$("#chk_red").removeClass("timeline");
					$(".line_onoff").removeClass("off"); 
					$(".line_onoff").addClass("on"); 	
					$("#chk_red").html('')
					$("#textyn").val('N');
					clearInterval(tid);	
		    		alert("휴대폰 인증이 완료되었습니다.");
		    	}else{
		    		alert("휴대폰 인증이 실패했습니다.");
		    	}
		    	
		    });
	    }else{
	    	alert('휴대전화 번호와 인증번호가 필요합니다.');
	    }
    }
    var SetTime = 180;		

	function msg_time() {	
		m = Math.floor(SetTime / 60) + " : " + pad((SetTime % 60),2) + "";	// 남은 시간 계산
		var msg = m;
		$("#chk_red").html(msg); 
		$("#chk_red").addClass("timeline");
		$(".line_onoff").removeClass("on");
		$(".line_onoff").addClass("off"); 
		SetTime--;					
		if (SetTime == 0) {		
			clearInterval(tid);		
			out_chknum();
		}
		
	}
	function out_chknum(){
		$("#userhp").removeAttr("disabled");
		var userhp=$.trim($("#M_HP_1").val()+$("#M_HP_2").val()+$("#M_HP_3").val());
		$.post("/join/out_chknum" ,
	    {
	        userhp : userhp
	    },
	    function(rslt){
	    	$("#chk_red").html('');
			$("#chk_red").removeClass("timeline");
			$(".line_onoff").removeClass("off"); 
			$(".line_onoff").addClass("on"); 
	    });
	}
	function TimerStart(){
		 
		if($("#textyn").val()=="Y"){
			SetTime=0;	
			$("#chk_red").removeClass("timeline");
			$(".line_onoff").addClass("on"); 
			$(".line_onoff").removeClass("off"); 
			$("#chk_red").html('')
			$("#textyn").val('N');
			clearInterval(tid);	
			TimerStart();
		}else{
			$("#textyn").val('Y');
			SetTime=180;
			tid=setInterval('msg_time()',1000)
		}
	 };