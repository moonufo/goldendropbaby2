  
	var SetTime = 180;		

	function msg_time() {	
		m = Math.floor(SetTime / 60) + " : " + pad((SetTime % 60),2) + "";	// 남은 시간 계산
		var msg = m;
		$("#chk_red").html(msg); 
		$("#chk_red").addClass("timeline");
		$(".line_onoff").removeClass("on");
		$(".line_onoff").addClass("off"); 
		SetTime--;					
		if (SetTime == 0) {		
			clearInterval(tid);		
			out_chknum();
		}
		
	}
	function out_chknum(){
		$("#userhp").removeAttr("disabled");
		var userhp=$.trim($("#M_HP_1").val()+$("#M_HP_2").val()+$("#M_HP_3").val());
		$.post("/join/out_chknum" ,
	    {
	        userhp : userhp
	    },
	    function(rslt){
	    	$("#chk_red").html('');
			$("#chk_red").removeClass("timeline");
			$(".line_onoff").removeClass("off"); 
			$(".line_onoff").addClass("on"); 
	    	
	    });
	}
	function TimerStart(){
		 
		if($("#textyn").val()=="Y"){
			SetTime=0;	
			$("#chk_red").removeClass("timeline");
			$(".line_onoff").addClass("on"); 
			$(".line_onoff").removeClass("off"); 
			$("#chk_red").html('')
			$("#textyn").val('N');
			clearInterval(tid);	
			TimerStart();
		}else{
			$("#textyn").val('Y');
			SetTime=180;
			tid=setInterval('msg_time()',1000)
		}
	 };
	function change_txt(){
		$("#M_EMAIL_2_txt").val($("#M_EMAIL_2_sel").val());
	}
	//최종 확인
	function frm_input() {
		var chk="Y";
		
		
		var Reg = /[\s]/g;
		
		if( Reg.test($('#M_EMAIL_1').val())==true || $('#M_EMAIL_1').val().length < 2){
			$(".msg_M_EMAIL").text("* 정확한 이메일 주소를 입력해주세요.");
			$(".td_M_EMAIL").addClass("caution");
			chk="N";
		}else if($.trim($('#M_EMAIL_2_txt').val()) == ''&& $('#M_EMAIL_2_sel').val()==''){
			$(".msg_M_EMAIL").text("* 정확한 이메일 주소를 입력해주세요.");
			$(".td_M_EMAIL").addClass("caution");
			chk="N";
		}else{
			$(".msg_M_EMAIL").text("");
			$(".td_M_EMAIL").removeClass("caution");
		}
		var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		var email= $('#M_EMAIL_1').val()+"@"+$('#M_EMAIL_2_txt').val();
		 if(!regEmail.test(email)) {
		        $(".msg_M_EMAIL").text("* 이메일 입력 형식을 확인해주세요.");
				$(".td_M_EMAIL").addClass("caution");
				chk="N";
	    }else{
			$(".msg_M_EMAIL").text("");
			$(".td_M_EMAIL").removeClass("caution");
		}
		
		
		if(chk=="Y"){
			checkUnload = false;
			$("#M_HP_1").removeAttr("disabled");
			$("#M_HP_2").removeAttr("disabled");
			$("#M_HP_3").removeAttr("disabled");
			$("#frm_input").submit();		
		}
		
		
			$("#frm_input").submit();	
	}
	//문자발송
	//문자발송
	function sms_send(){
    	var userhp=$("#M_HP_1 option:selected").val()+$("#M_HP_2").val()+$("#M_HP_3").val();
    	if($("#hp_chk").val()=="Y"){
			alert("이미 인증되었습니다.");
    		return;
    	}
    	
    	if(userhp!=""){
    		type="mypage";
    		out_chknum();
			$.post("/join/sms_insert" ,
		    {
		        userhp : userhp,
		        type : type
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="Z"){
	    			alert("이미 가입되어있는 번호입니다.");
	    		}else if(rslt=="X"){
		    		alert("인증번호발송은 일 5회까지만 가능합니다. 익일 다시 시도 해주세요.");
	    		}else if(rslt=="Y"){
		    		$("#M_HP_1").attr("disabled",true);
		    		$("#M_HP_2").attr("disabled",true);
		    		$("#M_HP_3").attr("disabled",true);
		    		TimerStart();
		    		alert("인증번호가 발송되었습니다.");
		    	}else{
		    		alert("인증번호 발송에 실패했습니다.");
		    	}
		    });
	    }else{
	    	alert('휴대전화 번호가 필요합니다.');
	    }
    }
    function sms_chknum(){
    	var userhp=$("#M_HP_1 option:selected").val()+$("#M_HP_2").val()+$("#M_HP_3").val();
    	var chknum=$("#chknum").val();
    	if($("#hp_chk").val()=="Y"){
			alert("이미 인증되었습니다.");
    		return;
    	}
    	
    	if(userhp!="" || chknum!="" ){
			$.post("/join/send_chknum" ,
		    {
		        userhp : userhp,
		        chknum : chknum
		    },
		    function(rslt){
		    	rslt=$.trim(rslt);
		    	if(rslt=="Y"){
		    		$("#M_HP_1").attr("disabled",true);
		    		$("#M_HP_2").attr("disabled",true);
		    		$("#M_HP_3").attr("disabled",true);
					$("#chknum").attr("disabled",true);
					$("#hp_chk").val("Y");
					SetTime=0;
					$("#chk_red").removeClass("timeline");
					$(".line_onoff").removeClass("off"); 
					$(".line_onoff").addClass("on"); 	
					$("#chk_red").html('')
					$("#textyn").val('N');
					clearInterval(tid);	
		    		alert("휴대폰 인증이 완료되었습니다.");
		    	}else{
		    		alert("휴대폰 인증이 실패했습니다.");
		    	}
		    	
		    });
	    }else{
	    	alert('휴대전화 번호와 인증번호가 필요합니다.');
	    }
	   
    
    }
    