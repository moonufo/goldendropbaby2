<?php
    include("./easypay_client.php");
    /* -------------------------------------------------------------------------- */
    /* ::: 처리구분 설정                                                          */
    /* -------------------------------------------------------------------------- */
    $TRAN_CD_NOR_PAYMENT    = "00101000";   // 승인(일반, 에스크로)
    $TRAN_CD_NOR_MGR        = "00201000";   // 변경(일반, 에스크로)

    /* -------------------------------------------------------------------------- */
    /* ::: 쇼핑몰 지불 정보 설정                                                  */
    /* -------------------------------------------------------------------------- */
    $g_gw_url    = "testgw.easypay.co.kr";               // Gateway URL ( test )
    //$g_gw_url               = "gw.easypay.co.kr";      // Gateway URL ( real )
    $g_gw_port   = "80";                                           // 포트번호(변경불가)

    /* -------------------------------------------------------------------------- */
    /* ::: 지불 데이터 셋업 (업체에 맞게 수정)                                    */
    /* -------------------------------------------------------------------------- */
    /* ※ 주의 ※                                                                 */
    /* cert_file 변수 설정                                                        */
    /* - pg_cert.pem 파일이 있는 디렉토리의  절대 경로 설정                       */
    /* log_dir 변수 설정                                                          */
    /* - log 디렉토리 설정                                                        */
    /* log_level 변수 설정                                                        */
    /* - log 레벨 설정 (1 to 99 (높을수록 상세))                                  */
    /* -------------------------------------------------------------------------- */
    $g_home_dir   = "/home/dartz/public_html/easypaybatch";
    $g_cert_file  = "/home/dartz/public_html/easypaybatch/cert/pg_cert.pem";
    $g_log_dir    = "/home/dartz/public_html/easypaybatch/log";
    $g_log_level  = "1";

    

    /* -------------------------------------------------------------------------- */
    /* ::: 승인요청 정보 설정                                                */
    /* -------------------------------------------------------------------------- */
    //[헤더]
    $tr_cd            = $_POST["EP_tr_cd"];             // [필수]요청구분
    $trace_no         = $_POST["EP_trace_no"];          // [필수]추적고유번호
    $order_no         = $_POST["EP_order_no"];          // [필수]주문번호
    $g_mall_id        = $_POST["EP_mall_id"];           // [필수]몰아이디
    //[공통]
    $sessionkey       = $_POST["EP_sessionkey"];        // [필수]암호화키
    $encrypt_data     = $_POST["EP_encrypt_data"];      // [필수]암호화 데이타


    /* -------------------------------------------------------------------------- */
    /* ::: 결제요청 - 가맹점 주문정보                                                         */
    /* -------------------------------------------------------------------------- */
    $memb_user_no     = $_POST["EP_memb_user_no"];     // [선택]가맹점 고객일련번호
    $user_id          = $_POST["EP_user_id"];          // [선택]고객 ID
    $user_nm          = $_POST["EP_user_nm"];        // [필수]고객명
    $user_mail        = $_POST["EP_user_mail"];        // [필수]고객 E-mail
    $user_phone1      = $_POST["EP_user_phone1"];      // [필수]가맹점 고객 연락처1
    $user_phone2      = $_POST["EP_user_phone2"];      // [선택]가맹점 고객 연락처2
    $user_addr        = $_POST["EP_user_addr"];        // [선택]가맹점 고객 주소
    $product_type     = $_POST["EP_product_type"];     // [필수]상품정보구분[0:실물,1:컨텐츠]
    $product_nm       = $_POST["EP_product_nm"];       // [필수]상품명
    $product_amt      = $_POST["EP_product_amt"];      // [필수]상품금액
    $batch_count      = $_POST["EP_batch_count"];      // 결제횟수
    $batch_cycle      = $_POST["EP_batch_cycle"];      // 결제 주기
    $batch_date      = $_POST["EP_batch_date"];      // 결제 시작일
    $batch_user      = $_POST["EP_batch_user"];      // 수령자
    $batch_hp      = $_POST["EP_batch_hp"];      // 핸드폰
    $batch_cp      = $_POST["EP_batch_cp"];      // 유선
    $batch_post      = $_POST["EP_batch_post"];      // 우편번호
    $batch_addr1      = $_POST["EP_batch_addr1"];      // 주소1
    $batch_addr2      = $_POST["EP_batch_addr2"];      // 주소2
    $batch_memo      = $_POST["EP_batch_memo"];      // 메모
    $product_code      = $_POST["EP_product_code"];      // 상품코드
    $product_price      = $_POST["EP_product_price"];      // 상품금액(정기용)
    
    

    /* -------------------------------------------------------------------------- */
    /* ::: 배치결제 정보 설정                                                */
    /* -------------------------------------------------------------------------- */
    $pay_type         = $_POST["EP_pay_type"];          // [필수]결제수단
    $tot_amt          = $_POST["EP_tot_amt"];           // [필수]총결제금액
    $curr_code        = $_POST["EP_currency"];          // [필수]통화코드
    $client_ip        = $_SERVER['REMOTE_ADDR'];        // [필수]결제고객 IP
    $cli_ver          = "W8";                           // [필수]클라이언트 버젼(W8로 고정)                          
    $complex_yn       = "N";                            // [필수]복합결제유무(배치결제는 사용 안함)                                                  
    $escrow_yn        = "N";                            // [필수]에스크로 여부(배치결제는 사용 안함)                                                 

    $card_txtype      = $_POST["EP_card_txtype"];       // [필수]처리구분
    $req_type         = $_POST["EP_req_type"];          // [필수]결제종류
    $card_amt         = $_POST["EP_card_amt"];          // [필수]결제금액
    $wcc              = $_POST["EP_wcc"];               // [필수]WCC
    $card_no          = $_POST["EP_card_no"];           // [필수]배치키
    $install_period   = $_POST["EP_install_period"];    // [필수]할부개월
    $noint            = $_POST["EP_noint"];             // [필수]무이자여부


    /* -------------------------------------------------------------------------- */
    /* ::: 변경관리 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    $mgr_txtype       = $_POST["mgr_txtype"];       // [필수]거래구분
    $mgr_subtype      = $_POST["mgr_subtype"];      // [선택]변경세부구분
    $org_cno          = $_POST["org_cno"];          // [필수]원거래고유번호
    $mgr_amt          = $_POST["mgr_amt"];          // [선택]부분취소/환불요청 금액
    $mgr_rem_amt      = $_POST["mgr_rem_amt"];      // [선택]부분취소 잔액
    $mgr_msg          = $_POST["mgr_msg"];          // [선택]변경 사유
    
    /* -------------------------------------------------------------------------- */
    /* ::: 전문                                                                   */
    /* -------------------------------------------------------------------------- */
    $mgr_data    = "";     // 변경정보
    $tx_req_data = "";     // 요청전문

    /* -------------------------------------------------------------------------- */
    /* ::: 결제 결과                                                              */
    /* -------------------------------------------------------------------------- */
    $r_res_cd             = "";     //응답코드         
    $r_res_msg            = "";     //응답메시지       
    $r_cno                = "";     //PG거래번호       
    $r_amount             = "";     //총 결제금액      
    $r_order_no           = "";     //주문번호         
    $r_auth_no            = "";     //승인번호         
    $r_tran_date          = "";     //승인일시         
    $r_escrow_yn          = "";     //에스크로 사용유무
    $r_complex_yn         = "";     //복합결제 유무    
    $r_stat_cd            = "";     //상태코드         
    $r_stat_msg           = "";     //상태메시지       
    $r_pay_type           = "";     //결제수단         
    $r_mall_id            = "";     //가맹점ID         
    $r_card_no            = "";     //카드번호         
    $r_issuer_cd          = "";     //발급사코드       
    $r_issuer_nm          = "";     //발급사명         
    $r_acquirer_cd        = "";     //매입사코드       
    $r_acquirer_nm        = "";     //매입사명         
    $r_install_period     = "";     //할부개월         
    $r_noint              = "";     //무이자여부           
    $r_part_cancel_yn     = "";     //부분취소 가능여부
    $r_card_gubun         = "";     //신용카드 종류    
    $r_card_biz_gubun     = "";     //신용카드 구분    
    $r_bk_pay_yn          = "";     //장바구니 결제여부
    $r_canc_acq_date      = "";     //매입취소일시     
    $r_canc_date          = "";     //취소일시         
    $r_refund_date        = "";     //환불예정일시     

    /* -------------------------------------------------------------------------- */
    /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
    /* -------------------------------------------------------------------------- */
    $easyPay = new EasyPay_Client;         // 전문처리용 Class (library에서 정의됨)
    $easyPay->clearup_msg();

    $easyPay->set_home_dir($g_home_dir);
    $easyPay->set_gw_url($g_gw_url);
    $easyPay->set_gw_port($g_gw_port);
    $easyPay->set_log_dir($g_log_dir);
    $easyPay->set_log_level($g_log_level);
    $easyPay->set_cert_file($g_cert_file);

    /* -------------------------------------------------------------------------- */
    /* ::: 승인요청(플러그인 암호화 전문 설정)                                    */
    /* -------------------------------------------------------------------------- */
    if( $TRAN_CD_NOR_PAYMENT == $tr_cd ) {

        /* ---------------------------------------------------------------------- */
        /* ::: 인증요청                                                           */
        /* ---------------------------------------------------------------------- */
        if( $pay_type == "81" ) { //배치인증
            $easyPay->set_trace_no($trace_no);
            $easyPay->set_snd_key($sessionkey);
            $easyPay->set_enc_data($encrypt_data);
        }
        else { //배치승인
        /* ---------------------------------------------------------------------- */
        /* ::: 승인요청                                                           */
        /* ---------------------------------------------------------------------- */
            // 결제 주문 정보 DATA
            $order_data = $easyPay->set_easypay_item("order_data");
            
            $easyPay->set_easypay_deli_us( $order_data, "order_no"      , $order_no      );
            $easyPay->set_easypay_deli_us( $order_data, "memb_user_no"  , $memb_user_no  );
            $easyPay->set_easypay_deli_us( $order_data, "user_id"       , $user_id       );
            $easyPay->set_easypay_deli_us( $order_data, "user_nm"       , $user_nm       );
            $easyPay->set_easypay_deli_us( $order_data, "user_mail"     , $user_mail     );
            $easyPay->set_easypay_deli_us( $order_data, "user_phone1"   , $user_phone1   );
            $easyPay->set_easypay_deli_us( $order_data, "user_phone2"   , $user_phone2   );
            $easyPay->set_easypay_deli_us( $order_data, "user_addr"     , $user_addr     );
            $easyPay->set_easypay_deli_us( $order_data, "product_type"  , $product_type  );
            $easyPay->set_easypay_deli_us( $order_data, "product_nm"    , $product_nm    );
            $easyPay->set_easypay_deli_us( $order_data, "product_amt"   , $product_amt   );

            // 결제정보 DATA부
            $pay_data = $easyPay->set_easypay_item("pay_data");

            // 결제 공통 정보 DATA
            $common_data = $easyPay->set_easypay_item("common");

            $easyPay->set_easypay_deli_us( $common_data, "tot_amt"       , $tot_amt      );
            $easyPay->set_easypay_deli_us( $common_data, "currency"      , $curr_code    );
            $easyPay->set_easypay_deli_us( $common_data, "client_ip"     , $client_ip    );
            $easyPay->set_easypay_deli_us( $common_data, "cli_ver"       , $cli_ver      );
            $easyPay->set_easypay_deli_us( $common_data, "escrow_yn"     , $escrow_yn    );
            $easyPay->set_easypay_deli_us( $common_data, "complex_yn"    , $complex_yn   );
            $easyPay->set_easypay_deli_rs( $pay_data, $common_data);

            $card_data = $easyPay->set_easypay_item("card");

            $easyPay->set_easypay_deli_us( $card_data, "card_txtype"    , $card_txtype       );
            $easyPay->set_easypay_deli_us( $card_data, "req_type"       , $req_type          );
            $easyPay->set_easypay_deli_us( $card_data, "card_amt"       , $card_amt          );
            $easyPay->set_easypay_deli_us( $card_data, "card_no"        , $card_no           );
            $easyPay->set_easypay_deli_us( $card_data, "noint"          , $noint             );
            $easyPay->set_easypay_deli_us( $card_data, "wcc"            , $wcc               );
            $easyPay->set_easypay_deli_us( $card_data, "install_period" , $install_period    );

            $easyPay->set_easypay_deli_rs( $pay_data, $card_data );
        }

    /* -------------------------------------------------------------------------- */
    /* ::: 변경관리 요청                                                          */
    /* -------------------------------------------------------------------------- */
    }else if( $TRAN_CD_NOR_MGR == $tr_cd ) {

        $mgr_data = $easyPay->set_easypay_item("mgr_data");
        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_txtype"      , $mgr_txtype       );
        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_subtype"     , $mgr_subtype      );
        $easyPay->set_easypay_deli_us( $mgr_data, "org_cno"         , $org_cno          );
        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_amt"         , $mgr_amt          );
        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_rem_amt"     , $mgr_rem_amt      );
        $easyPay->set_easypay_deli_us( $mgr_data, "req_ip"          , $client_ip        );
        $easyPay->set_easypay_deli_us( $mgr_data, "mgr_msg"         , $mgr_msg          );       
    }


    /* -------------------------------------------------------------------------- */
    /* ::: 실행                                                                   */
    /* -------------------------------------------------------------------------- */
    $opt = "";
    $easyPay->easypay_exec($g_mall_id, $tr_cd, $order_no, $client_ip, $opt);
    $r_res_cd  = $easyPay->_easypay_resdata["res_cd"];    // 응답코드
    $r_res_msg = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata["res_msg"]);   // 응답메시지
    
    /* -------------------------------------------------------------------------- */
    /* ::: 결과 처리                                                              */
    /* -------------------------------------------------------------------------- */
		$r_cno             = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "cno"             ]);    //PG거래번호
    $r_amount          = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "amount"          ]);    //총 결제금액
    $r_order_no        = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "order_no"        ]);    //주문번호
    $r_auth_no         = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "auth_no"         ]);    //승인번호
    $r_tran_date       = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "tran_date"       ]);    //승인일시
    $r_escrow_yn       = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "escrow_yn"       ]);    //에스크로 사용유무
    $r_complex_yn      = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "complex_yn"      ]);    //복합결제 유무
    $r_stat_cd         = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "stat_cd"         ]);     //상태코드
    $r_stat_msg        = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "stat_msg"        ]);     //상태메시지
    $r_pay_type        = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "pay_type"        ]);     //결제수단
    $r_mall_id         = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "memb_id"         ]);     //가맹점ID
    $r_card_no         = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "card_no"         ]);     //카드번호
    $r_issuer_cd       = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "issuer_cd"       ]);     //발급사코드
    $r_issuer_nm       = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "issuer_nm"       ]);     //발급사명
    $r_acquirer_cd     = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "acquirer_cd"     ]);     //매입사코드
    $r_acquirer_nm     = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "acquirer_nm"     ]);     //매입사명
    $r_install_period  = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "install_period"  ]);     //할부개월
    $r_noint           = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "noint"           ]);     //무이자여부
    $r_part_cancel_yn  = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "part_cancel_yn"  ]);     //부분취소 가능여부
    $r_card_gubun      = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "card_gubun"      ]);     //신용카드 종류
    $r_card_biz_gubun  = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "card_biz_gubun"  ]);     //신용카드 구분
    $r_bk_pay_yn       = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "bk_pay_yn"       ]);     //장바구니 결제여부
    $r_canc_acq_date   = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "canc_acq_date"   ]);     //매입취소일시
    $r_canc_date       = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "canc_date"       ]);     //취소일시
    $r_refund_date     = iconv("EUC-KR","UTF-8",$easyPay->_easypay_resdata[ "refund_date"     ]);     //환불예정일시

    /* -------------------------------------------------------------------------- */
    /* ::: 가맹점 DB 처리                                                         */
    /* -------------------------------------------------------------------------- */
    /* 응답코드(res_cd)가 "0000" 이면 정상승인 입니다.                            */
    /* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */
    /* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */
    /* -------------------------------------------------------------------------- */
    if ( $res_cd == "0000" )
    {
        $bDBProc = "true";     // DB처리 성공 시 "true", 실패 시 "false"
        if ( $bDBProc != "true" )
        {
            // 승인요청이 실패 시 아래 실행
            if( $TRAN_CD_NOR_PAYMENT == $tr_cd )
            {
                $easyPay->clearup_msg();

                $tr_cd = $TRAN_CD_NOR_MGR;
                $mgr_data = $easyPay->set_easypay_item("mgr_data");
                $easyPay->set_easypay_deli_us( $mgr_data, "mgr_txtype"      , "40"   );
                $easyPay->set_easypay_deli_us( $mgr_data, "org_cno"         , $r_cno     );
                $easyPay->set_easypay_deli_us( $mgr_data, "order_no"         , $order_no     );
                $easyPay->set_easypay_deli_us( $mgr_data, "req_ip"          , $client_ip );
                $easyPay->set_easypay_deli_us( $mgr_data, "req_id"          , "MALL_R_TRANS" );
                $easyPay->set_easypay_deli_us( $mgr_data, "mgr_msg"         , "DB 처리 실패로 망취소"  );

                $easyPay->easypay_exec($g_mall_id, $tr_cd, $order_no, $client_ip, $opt);
                $res_cd      = $easyPay->_easypay_resdata["res_cd"     ];    // 응답코드
                $res_msg     = $easyPay->_easypay_resdata["res_msg"    ];    // 응답메시지
                $r_cno       = $easyPay->_easypay_resdata["cno"        ];    // PG거래번호
                $r_canc_date = $easyPay->_easypay_resdata["canc_date"  ];    // 취소일시
            }
        }
    }
?>
<html>
<meta name="robots" content="noindex, nofollow">
<script type="text/javascript">
    function f_submit(){
        document.frm.submit();
    }
</script>

<body onload="f_submit();">
<form name="frm" method="post" action="/order/order_log">
    <input type="hidden" id="res_cd"           name="res_cd"          value="<?=$r_res_cd?>">            <!-- 결과코드 //-->
    <input type="hidden" id="res_msg"          name="res_msg"         value="<?=$r_res_msg?>">           <!-- 결과메시지 //-->
    <input type="hidden" id="cno"              name="cno"             value="<?=$r_cno?>">               <!-- PG거래번호 //-->
    <input type="hidden" id="amount"           name="amount"          value="<?=$r_amount?>">            <!-- 총 결제금액 //-->
    <input type="hidden" id="order_no"         name="order_no"        value="<?=$r_order_no?>">          <!-- 주문번호 //-->
    <input type="hidden" id="auth_no"          name="auth_no"         value="<?=$r_auth_no?>">           <!-- 승인번호 //-->
    <input type="hidden" id="tran_date"        name="tran_date"       value="<?=$r_tran_date?>">         <!-- 승인일시 //-->
    <input type="hidden" id="escrow_yn"        name="escrow_yn"       value="<?=$r_escrow_yn?>">         <!-- 에스크로 사용유무 //-->
    <input type="hidden" id="complex_yn"       name="complex_yn"      value="<?=$r_complex_yn?>">        <!-- 복합결제 유무 //-->
    <input type="hidden" id="stat_cd"          name="stat_cd"         value="<?=$r_stat_cd?>">           <!-- 상태코드 //-->
    <input type="hidden" id="stat_msg"         name="stat_msg"        value="<?=$r_stat_msg?>">          <!-- 상태메시지 //-->
    <input type="hidden" id="pay_type"         name="pay_type"        value="<?=$r_pay_type?>">          <!-- 결제수단 //-->
    <input type="hidden" id="mall_id"          name="mall_id"         value="<?=$r_mall_id?>">           <!-- 가맹점ID //-->
    <input type="hidden" id="card_no"          name="card_no"         value="<?=$r_card_no?>">           <!-- 카드번호 //-->
    <input type="hidden" id="issuer_cd"        name="issuer_cd"       value="<?=$r_issuer_cd?>">         <!-- 발급사코드 //-->
    <input type="hidden" id="issuer_nm"        name="issuer_nm"       value="<?=$r_issuer_nm?>">         <!-- 발급사명 //-->
    <input type="hidden" id="acquirer_cd"      name="acquirer_cd"     value="<?=$r_acquirer_cd?>">       <!-- 매입사코드 //-->
    <input type="hidden" id="acquirer_nm"      name="acquirer_nm"     value="<?=$r_acquirer_nm?>">       <!-- 매입사명 //-->
    <input type="hidden" id="install_period"   name="install_period"  value="<?=$r_install_period?>">    <!-- 할부개월 //-->
    <input type="hidden" id="noint"            name="noint"           value="<?=$r_noint?>">             <!-- 무이자여부 //-->
    <input type="hidden" id="join_no"          name="join_no"         value="<?=$r_join_no?>">           <!-- 가맹점 번호 //-->
    <input type="hidden" id="part_cancel_yn"   name="part_cancel_yn"  value="<?=$r_part_cancel_yn?>">    <!-- 부분취소 가능여부 //-->
    <input type="hidden" id="card_gubun"       name="card_gubun"      value="<?=$r_card_gubun?>">        <!-- 신용카드 종류 //-->
    <input type="hidden" id="card_biz_gubun"   name="card_biz_gubun"  value="<?=$r_card_biz_gubun?>">    <!-- 신용카드 구분 //-->
    <input type="hidden" id="bk_pay_yn"        name="bk_pay_yn"       value="<?=$r_bk_pay_yn?>">         <!-- 장바구니 결제여부 //-->
    <input type="hidden" id="canc_acq_date"    name="canc_acq_date"   value="<?=$r_canc_acq_date?>">     <!-- 매입취소일시 //-->
    <input type="hidden" id="canc_date"        name="canc_date"       value="<?=$r_canc_date?>">         <!-- 취소일시 //-->
    <input type="hidden" id="refund_date"      name="refund_date"     value="<?=$r_refund_date?>">       <!-- 환불예정일시 //-->
    <input type="hidden" id="set_type"         name="set_type"        value="S">      					 <!-- 정시 //-->
    <input type="hidden" id="batch_count"         name="batch_count"        value="<?=$batch_count?>">      					 <!-- 결제횟수//-->
    <input type="hidden" id="batch_cycle"         name="batch_cycle"        value="<?=$batch_cycle?>">      					 <!-- 결제주기 //-->
    <input type="hidden" id="batch_date"         name="batch_date"        value="<?=$batch_date?>">      					 <!-- 결제 시작일 //-->
    <input type="hidden" id="batch_user"         name="batch_user"        value="<?=$batch_user?>">      					 <!-- 수령자 //-->
    <input type="hidden" id="batch_hp"         	name="batch_hp"         value="<?=$batch_hp?>">      					 <!-- 핸드폰 //-->
    <input type="hidden" id="batch_cp"         	name="batch_cp"        	value="<?=$batch_cp?>">      					 <!-- 유선 //-->
    <input type="hidden" id="batch_post"         name="batch_post"        value="<?=$batch_post?>">      					 <!-- 우편번호 //-->
    <input type="hidden" id="batch_addr1"         name="batch_addr1"        value="<?=$batch_addr1?>">      					 <!-- 주소1 //-->
    <input type="hidden" id="batch_addr2"         name="batch_addr2"        value="<?=$batch_addr2?>">      					 <!-- 주소2 //-->
    <input type="hidden" id="batch_memo"         name="batch_memo"        value="<?=$batch_memo?>">      					 <!-- 메모 //-->
    <input type="hidden" id="product_code"         name="product_code"        value="<?=$product_code?>">      					 <!-- 상품코드 //-->
    <input type="hidden" id="product_price"         name="product_price"        value="<?=$product_price?>">      					 <!-- 상품금액(정기) //-->

</form>
</body>
</html>