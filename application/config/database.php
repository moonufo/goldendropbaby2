<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'igini';
$db['default']['password'] = '!spc7200p#';
$db['default']['database'] = 'dbilluma';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


$db['mssql']['hostname'] = "222.235.64.189";
$db['mssql']['username'] = "sa";
$db['mssql']['password'] = "dbcomgot1";
$db['mssql']['database'] = "testPowerlabMall";
$db['mssql']['dbdriver'] = "mssql";
$db['mssql']['dbprefix'] = "";
$db['mssql']['pconnect'] = FALSE;
$db['mssql']['db_debug'] = TRUE;
$db['mssql']['cache_on'] = FALSE;
$db['mssql']['cachedir'] = "";
$db['mssql']['char_set'] = "windows-1252";
$db['mssql']['dbcollat'] = "sql_latin1_general_cp1_ci_as";
/* End of file database.php */
/* Location: ./application/config/database.php */
//오라클 DB 커넥션
/*
$dbhost = "localhost";    //IP of my host database
$dbport = "1521";    //Oracle port
$dbname= "ORCL";    //My database Name
 
$dbConnString = "
 (DESCRIPTION =
 (ADDRESS_LIST =
   (ADDRESS = (PROTOCOL = TCP)(HOST = ".$dbhost.")(PORT = ".$dbport."))
 ) 
 (CONNECT_DATA =
     (SID = ORCL)
  (SERVICE_NAME = ".$dbname.")
 ))";
 
$db['oracle']['hostname'] = $dbConnString;
$db['oracle']['username'] = "combort";   
$db['oracle']['password'] = "1234";  
$db['oracle']['database'] = "";
$db['oracle']['dbdriver'] = "oci8";  
$db['oracle']['dbprefix'] = "";
$db['oracle']['pconnect'] = TRUE;
$db['oracle']['db_debug'] = TRUE;
$db['oracle']['cache_on'] = FALSE;
$db['oracle']['cachedir'] = "";
$db['oracle']['char_set'] = "utf8";
$db['oracle']['dbcollat'] = "utf8_general_ci";
*/