<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);
/*
| -------------------------------------------------------------------
| @ TITLE   이벤트
| @ AUTHOR  PJH
| @ SINCE   17. 12. 11.
| @ PURPOSE 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class event extends common {

    public function __construct(){
        parent::__construct();

        $this->load->helper('download');

    }
    public function event_index() {
        $params = $this->input->get();
        
        // 조건 검색 말머리,제목
        if($params['search_F']!=""){
            if($params['search_S']!="")$params['where'].=" and ".$params['search_F']." like '%".$params['search_S']."%'";
        }

        // 날짜 선택 소팅
        if($params['e_strdate']!=""){
            $params['where'].=" and E_CREDATE between '".$params['e_strdate']."' and '".$params['e_enddate']."' ";
        }
        
        // 분류값에 따른 소팅. 현재,지난,예정 onchange
        $chk_now_date = date("Y-m-d");
        if($params['search_date']!=""){
            if($params['search_date']=="chk_date_pass"){
                $params['where'].=" and E_ENDDATE < '".$chk_now_date."'";
            }elseif ($params['search_date']=="chk_date_for"){
                $params['where'].=" and E_STRDATE > '".$chk_now_date."'";
            }elseif ($params['search_date']=="chk_date_now"){
                $params['where'].=" and E_STRDATE <= '".$chk_now_date."' and  E_ENDDATE >= '".$chk_now_date."'";
            }
        }

//        // 날짜 수에 따른 소팅, 15,30,90 일 onchange
        if($params['total_date_cnt']!=""){
            if($params['total_date_cnt']=="15"){
                $strdate = date("Y-m-d");
                $enddate = date("Y-m-d",strtotime("+15 day"));
                $params['where'].=" and E_CREDATE > date_add(now(),interval -15 day) and E_CREDATE between '".$strdate."' and '".$enddate."'";
            }elseif ($params['total_date_cnt']=="30"){
                $strdate = date("Y-m-d");
                $enddate = date("Y-m-d",strtotime("+30 day"));
                $params['where'].=" and E_CREDATE > date_add(now(),interval -30 day) and E_CREDATE between '".$strdate."' and '".$enddate."'";
            }elseif ($params['total_date_cnt']=="90"){
                $strdate = date("Y-m-d");
                $enddate = date("Y-m-d",strtotime("+90 day"));
                $params['where'].=" and E_CREDATE > date_add(now(),interval -90 day) and E_CREDATE between '".$strdate."' and '".$enddate."'";
            }else{
                $params['where'].="";
            }
        }

        $params['count_yn']='Y';
        $params["maxcount"] = $this->event_model->event_list($params); // pgfile_카운트  정보 갖고오기
        $page_cnt=6;

        if($params['per_page']=="" || $params['per_page']<1)$page= 1;
        else $page= $params['per_page'];
        $params['count_yn']='N';
        $data["pagination"]=$this->get_pagination('/event/event_index?search_F='.$params['search_F'].'&search_S='.$params['search_S'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;
		
        $data['list'] = $this->event_model->event_list($params);
		 
        $data['now_date'] = date("Y-m-d");
        $data['start_no']=count($params["maxcount"])-$result_page;
        if($params['e_strdate']!="") {
            $data['total_date_cnt'] = "";
        }else{
            $data['total_date_cnt'] = $params['total_date_cnt'];
        }
        $data['search_date']=$params['search_date'];
        $data['search_S']=$params['search_S'];
        $data['search_F']=$params['search_F'];
        $data['per_page']=$params['per_page'];

        if($params['total_date_cnt']!=""){
            if($params['total_date_cnt']=="15"){
                $data['e_strdate']=date("Y-m-d");
                $data['e_enddate']=date("Y-m-d",strtotime("+15 day"));
            }elseif ($params['total_date_cnt']=="30"){
                $data['e_strdate']=date("Y-m-d");
                $data['e_enddate']=date("Y-m-d",strtotime("+30 day"));
            }elseif ($params['total_date_cnt']=="90"){
                $data['e_strdate']=date("Y-m-d");
                $data['e_enddate']=date("Y-m-d",strtotime("+90 day"));
            }
        }else{
            $data['e_strdate']=$params['e_strdate'];
            $data['e_enddate']=$params['e_enddate'];
        }

        for($i=0; $i < sizeof($data["list"]); $i++ ) {
            //mb_substr("문자열",시작번호,자를갯수,"인코딩");
            $content_len2=mb_strlen(strip_tags($data["list"][$i]['E_CONTENT'], "utf-8")); // 태그 제거 후 문자 총 길이

            if ($content_len2 >= 70) {

                $data["list"][$i]['content_txt'] = mb_substr(strip_tags($data["list"][$i]['E_CONTENT']), 0, 70, 'UTF-8')."...";

            } else {
                $data["list"][$i]['content_txt'] = strip_tags($data["list"][$i]['E_CONTENT']);
            }

        }

        $this->_print($data);

    }

    public function event_view(){
        $params=$this->input->get();
        $user_code = $this->session->userdata('USER_ACCOUNTCODE');
        $data=$this->common_model->_select_row('TB_EVENT',array('e_idx'=>$params['e_idx'],));

        $data['next']=$this->event_model->event_next(array('e_credate'=>$data['E_CREDATE']));
        $data['next_event_idx']=$data['next']['next_event_idx'];
        $data['next_event_title']=$data['next']['next_event_title'];
        $data['prev']=$this->event_model->event_prev(array('e_credate'=>$data['E_CREDATE']));
        $data['prev_event_idx']=$data['prev']['prev_event_idx'];
        $data['prev_event_title']=$data['prev']['prev_event_title'];

        $data['point_txt'] = "";
        $entry_idx = $params['e_idx'];

        if($data['E_AUTOPOINT_YN']=="Y"){
            if($data['E_POINT'] > 0){
                $data['point_txt'] = "<tr style='text-align: center'><td colspan='2' class='incont'><div><img src='/images/event/event_point.png' onclick=\"javascript:event_entry('$entry_idx','U');\" style='cursor: pointer'></div></td></tr>";
            }else{
                $data['point_txt'] = "<tr style='text-align: center'><td colspan='2' class='incont'><div><img src='/images/event/event_point.png' onclick=\"javascript:event_entry('$entry_idx','D');\" style='cursor: pointer'></div></td></tr>";
            }
        }

        $data['per_page']=$params['per_page'];
        $data['search_date']=$params['search_date'];
        $data['user_code'] = $user_code;
        $this->_print($data);
}

    public function event_entry_chk(){
        $params = $this->input->get();
        $user_code = $this->session->userdata('USER_ACCOUNTCODE');
        $user_name = $this->session->userdata('USER_NAME');
        $user_account = $this->session->userdata('USER_ACCOUNT');

        $params['user_code'] = $user_code;
        $params['user_name'] = $user_name;
        $params['user_account'] = $user_account;
        $params['et_ip']=$_SERVER['REMOTE_ADDR'];

        $entrycount = $this->event_model->entry_row($params); // 응모했는지 카운트
        $cnt_chk = $entrycount['cnt'];
        $msg = "N";

        if($cnt_chk > 0){
            //$this -> javascript_lib -> top_location(array("msg" => '중복 응모입니다.', "nextUrl" => '/event/event_index'));

//            echo ("<SCRIPT LANGUAGE='JavaScript'>
//                window.alert('중복 응모입니다.');
//                window.location.href='/event/event_index';
//            </SCRIPT>");
            $msg = "N";
        }else{
            $this->event_model->entry_insert($params);
            $msg = "Y";
        }

        echo $msg;

    }

    public function point_index(){
		if($this->session->userdata('USER_ACCOUNT')!=""){
			$this->load->model("point_model");
			$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
			$data=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
		}
		$this->_print($data);
	}

    public function point_check(){
		if($this->session->userdata('USER_ACCOUNT')!=""){
			$this->load->model("point_model");
			$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
			$data=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
			if ($data["PL_TOTALPOINT"]<50000){
				 echo json_encode(array('Stat'=>'N','Msg'=>'교환가능한 포인트가 부족합니다.'));
			}else{
				 echo json_encode(array('Stat'=>'Y','Msg'=>'혜택상품으로 포인트 교환을 신청하시겠습니까?'));
			}
		}
	}

    public function point_use(){
		if($this->session->userdata('USER_ACCOUNT')!=""){
			$this->load->model("point_model");
			$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
			$data=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));

            $user_data = $this->point_model->rowdata_user(array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE)); // 회원 데이터 추출

            $this->point_model->insert_point_change($user_data);    // 교환신청 user insert

			if ($data["PL_TOTALPOINT"]<50000){
				 echo json_encode(array('Stat'=>'N','Msg'=>'교환가능한 포인트가 부족합니다.'));
			}else{
				$point['ACCOUNTCODE']=$M_ACCOUNTCODE;
				$point['code']="PC03";
				$point['use']="차감";
				$point['point']=50000;
				$point['title']="신세계 상품권 신청";
				$point['memo']="신세계 상품권 신청";
				$this->out_point($point);
				 echo json_encode(array('Stat'=>'Y','Msg'=>'포인트 교환 신청이 완료되었습니다.
	차주 월요일 모바일 상품권이 회원님 휴대폰번호로 발송 될 예정입니다'));
			}

		}
	}
}

