<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);

/*
| -------------------------------------------------------------------
| @ TITLE   STORY 페이지 컨트롤러
| @ AUTHOR  PJH
| @ SINCE   17. 12. 27.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class story extends common {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $data=$this->common_model->_select_row('TB_STORY',array('TS_GUBUN'=>'Brand'));
        $this->_print($data);
    }

	public function brand(){
        $data=$this->common_model->_select_row('TB_STORY',array('TS_GUBUN'=>'Brand'));
        $this->_print($data);
	}

    public function product(){
        $data=$this->common_model->_select_row('TB_STORY',array('TS_GUBUN'=>'Product'));
        $this->_print($data);
    }

    public function product_lreland(){
        $data=$this->common_model->_select_row('TB_STORY',array('TS_GUBUN'=>'Product_lreland'));
        $this->_print($data);
    }

    public function product_opo(){
        $data=$this->common_model->_select_row('TB_STORY',array('TS_GUBUN'=>'Product_opo'));
        $this->_print($data);
    }

    public function wyeth(){
        $data=$this->common_model->_select_row('TB_STORY',array('TS_GUBUN'=>'Wyeth'));
        $this->_print($data);
    }

}
