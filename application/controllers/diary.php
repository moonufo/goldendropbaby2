<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);
/*
| -------------------------------------------------------------------
| @ TITLE   일루마 Contact
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class diary extends common {

    public function __construct(){
        parent::__construct();
    }
	public function special() {
        $params = $this->input->get();

        if($params['search_F']!=""){
            if($params['search_S']!="")$params['where'].=" and ".$params['search_F']." like '%".$params['search_S']."%'";
        }else{
            $params['search_F'] = "";
            $params['search_S'] = "";
        }
        $params['count_yn']="Y";
        $params["maxcount"] = $this->diary_model->special_list($params); // pgfile_카운트  정보
        $page_cnt=4;

        if($params['per_page']=="" || $params['per_page']<1){
            $page= 1;
            $params['per_page'] = 0;
        }else {
            $page= $params['per_page'];
        }

        $data["pagination"]=$this->get_pagination('/diary/special?search_F='.$params['search_F'].'&search_S='.$params['search_S'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;
        $params['count_yn']="N";
        $data['list'] = $this->diary_model->special_list($params);

        $data['start_no']=count($params["maxcount"])-$result_page;
        $data['search_S']=$params['search_S'];
        $data['search_F']=$params['search_F'];
        $data['per_page']=$params['per_page'];
        $this->_print($data);

    }

    public function special_view(){
        $params=$this->input->get();
        $data=$this->common_model->_select_row('TB_DIARY_SPECIAL',array('ds_idx'=>$params['ds_idx']));
        $data['next']=$this->diary_model->special_next(array('ds_idx'=>$params['ds_idx']));
        $data['next_special_idx']=$data['next']['next_special_idx'];
        $data['next_special_title']=$data['next']['next_special_title'];
        $data['prev']=$this->diary_model->special_prev(array('ds_idx'=>$params['ds_idx']));
        $data['prev_special_idx']=$data['prev']['prev_special_idx'];
        $data['prev_special_title']=$data['prev']['prev_special_title'];

        if($data['ds_id']==""){
            $data['ds_id']=$this->session->userdata('admin_id');
        }
        $data['ds_no']=$params['ds_no'];
        $data['per_page']=$params['per_page'];
        $this->_print($data);
    }

    public function smart() {
        $params = $this->input->get();
        $data['code_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'DM'));

        if($params['search_gubun']!=""){
            $params['where'].=" and DM_TYPE='".$params['search_gubun']."' ";
        }else{
            $params['where'].="";
        }

        if($params['search_F']!=""){
            if($params['search_S']!="")$params['where'].=" and ".$params['search_F']." like '%".$params['search_S']."%'";
        }else{
            $params['search_F'] = "";
            $params['search_S'] = "";
        }
        $params['count_yn']="Y";
        $params["maxcount"] = $this->diary_model->smart_list($params); // pgfile_카운트  정보

        $page_cnt=9;

        if($params['per_page']=="" || $params['per_page']<1){
            $page= 1;
            $params['per_page'] = 0;
        }else {
            $page= $params['per_page'];
        }

        $data["pagination"]=$this->get_pagination('/diary/smart?search_F='.$params['search_F'].'&search_S='.$params['search_S'].'&search_gubun='.$params['search_gubun'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;
        $params['count_yn']="N";
        $data['list'] = $this->diary_model->smart_list($params);
        foreach ($data['list'] as $key => $value) {
            $data['list'][$key]['smart_type_txt']=$this->get_code_txt($data['list'][$key]['DM_TYPE']);
        }
        $total_no=count($params["maxcount"]);
        $data['total_no']=count($params["maxcount"]);
        $data['start_no']=count($params["maxcount"])-$result_page;
        $data['search_S']=$params['search_S'];
        $data['search_F']=$params['search_F'];
        $data['per_page']=$params['per_page'];
        $data['search_gubun'] = $params['search_gubun'];
        $this->_print($data);
    }

    public function smart_view(){
        $params=$this->input->get();
        $data=$this->common_model->_select_row('TB_DIARY_SMART',array('dm_idx'=>$params['dm_idx']));
        $data['next']=$this->diary_model->smart_next(array('dm_idx'=>$params['dm_idx']));
        $data['next_smart_idx']=$data['next']['next_smart_idx'];
        $data['next_smart_title']=$data['next']['next_smart_title'];
        $data['prev']=$this->diary_model->smart_prev(array('dm_idx'=>$params['dm_idx']));
        $data['prev_smart_idx']=$data['prev']['prev_smart_idx'];
        $data['prev_smart_title']=$data['prev']['prev_smart_title'];
        $params['smart_type'] = "'".$data['DM_TYPE']."'";
        if($data['dm_id']==""){
            $data['dm_id']=$this->session->userdata('admin_id');
        }
        $data['smart_gubun'] = $params['search_gubun'];

        $gubun_txt = $this->diary_model->gubun_search($params); // ==== 구분 TITLE
        $data['gubun_title'] = $gubun_txt['gubun_txt'];
        $data['per_page']=$params['per_page'];

        $this->_print($data);
    }

    public function happy() {
        $params = $this->input->get();

        $params['count_yn']='N';

        if($params['H_TYPE']!=""){
            $params['where'].=" and IM_IDX='".$params['H_TYPE']."' ";
        }else{
            $hash_first_idx = $this->diary_model->hash_first_idx($params); // ==== Master 해시태그 디폴드 시퀀스
            $params['where'].=" and IM_IDX ='".$hash_first_idx['im_idx']."' ";
            $params['H_TYPE'] = "'".$hash_first_idx['im_idx']."'";
        }

        $data['tap_list']=$this->common_model->_select_list('TB_INSTAR_MASTER',array('IM_ORDER !='=>'-','IM_REAL_YN'=>'Y','IM_DISPLAY_YN'=>'Y','oType'=>'desc','oKey'=>'IM_ORDER','limit'=>6));

        $params["maxcount"] = $this->diary_model->hashtag_count(array('count_yn'=>'Y','where'=>$params['where'])); // pgfile_카운트  정보 갖고오기
        $page_cnt=9;

        if($params['per_page']=="" || $params['per_page']<1){
            $page= 1;
            $params['per_page'] = 0;
        }else {
            $page= $params['per_page'];
        }

        $data["pagination"]=$this->get_pagination('/diary/happy?H_TYPE='.$params['H_TYPE'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;

        $data['list'] = $this->diary_model->hashtag_list($params);
        $total_no=count($params["maxcount"]);
        $data['total_no']=count($params["maxcount"]);
        $data['start_no']=count($params["maxcount"])-$result_page;
        $img_cnt = $this->diary_model->detail_row($params); // detail 이미지 카운트  정보
        $hash_title = $this->diary_model->hash_search($params); // ==== Master 해시태그명
        $data['img_count'] = $img_cnt['cnt'];
        $data['per_page']=$params['per_page'];
        if($params['H_TYPE'] == ""){
            $data['H_TYPE']= 0;
        }else{
            $data['H_TYPE']=$params['H_TYPE'];
        }
        $data['hash_title'] = $hash_title['im_text'];
        $this->_print($data);
    }


}