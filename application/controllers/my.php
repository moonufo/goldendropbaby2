<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);
/*
| -------------------------------------------------------------------
| @ TITLE   일루마 my
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/
class my extends common {

    public function __construct(){
        parent::__construct();
		
		$this->load->model("join_model");
		$this->load->model("point_model");
		
    }
	public function index() {
		$this->member_lib->loginCheck();
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data['M_ACCOUNT']=$M_ACCOUNT;
		$data['pldata']=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
		$this ->_print($data);
	}
	public function index_chk() {
		$params = $this->input->get();
		$this->member_lib->loginCheck();
		$this ->_print($params);
	}
	public function index_process(){
		$params = $this->input->post();
		//임의설정
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$this->member_lib->urlCheck(array('data'=>$M_ACCOUNTCODE));
		$this->member_lib->loginCheck();
		$result="N";
		$params['pwori']  = hash('sha256',$params['pwori'],true);
		$params['pwori'] =  base64_encode($params['pwori']);
		$data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y','M_PW'=>$params['pwori'] ));
		if($data){
			$this->load->helper('url');
			if($params['chk']=="Y"){
				redirect("/my/mypage#chmove");	
			}else{
				redirect("/my/mypage");
			}
		}else{
			$this->javascript_lib->top_location(array('msg'=>'비밀번호가 다릅니다.','nextUrl'=>'/my/index_chk'));
		}
	}
	
	public function mypage() {
		//세션 가져오기//
		
		$yn="N";
		if($_SERVER['HTTP_REFERER']=="https://dartz.swtown.co.kr/my/index_chk" || $_SERVER['HTTP_REFERER']=="https://dartz.swtown.co.kr/my"
		||$_SERVER['HTTP_REFERER']=="http://dartz.swtown.co.kr/my/index_chk" || $_SERVER['HTTP_REFERER']=="http://dartz.swtown.co.kr/my/" 
		||$_SERVER['HTTP_REFERER']=="http://dartz.swtown.co.kr/my/index_chk?chk=Y" || $_SERVER['HTTP_REFERER']=="https://dartz.swtown.co.kr/my/index_chk?chk=Y" ){
			$yn="Y";
		}
		if($yn=="N"){
			$this->javascript_lib->none_msg_location(array('nextUrl'=>'https://dartz.swtown.co.kr/my'));
		}
		$this->member_lib->loginCheck();
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y'));
		
		//이메일//
		$mail='';		
		if($data['M_EMAIL']!=''){
			$mail=explode('@',$data['M_EMAIL']);
			$data['M_EMAIL_1']=$mail[0];
			$data['M_EMAIL_2_txt']=$mail[1];
		}
		//이메일//
		//휴대폰
		if($data['M_HP']!=''){
			$cp=explode('-',$data['M_HP']);
			$data['M_HP_1']=$cp[0];
			$data['M_HP_2']=$cp[1];
			$data['M_HP_3']=$cp[2];
		}
		for($i=0;$i<sizeof($data["cp_list"] );$i++) {
			$sel="";
			if($data["cp_list"][$i]['C_CODE_PA']==$cp[0]) $sel='selected="selected"';
            $cp_list.="<option ".$sel." value='{$data["cp_list"][$i]['C_CODE_PA']}'>{$data["cp_list"][$i]['C_CODE_PA']}</option>";
        }
		//휴대폰 끝
		//집전화//
		$data['cp_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'CP','C_CODE_DEPTH'=>1,'C_REAL_YN'=>'Y','oType'=>'asc','oKey'=>'C_CODE_ORDERBY'));
		$cp='';		
		if($data['M_CP']!=''){
			$cp=explode('-',$data['M_CP']);
			$data['M_CP_2']=$cp[1];
			$data['M_CP_3']=$cp[2];
		}
		for($i=0;$i<sizeof($data["cp_list"] );$i++) {
			$sel="";
			if($data["cp_list"][$i]['C_CODE_PA']==$cp[0]) $sel='selected="selected"';
            $cp_list.="<option ".$sel." value='{$data["cp_list"][$i]['C_CODE_PA']}'>{$data["cp_list"][$i]['C_CODE_PA']}</option>";
        }
		$data['cp_list']=$cp_list;
		//집전화//
		
		//자녀 셋팅//
		
		$data['MC_list'] = $this->common_model->_select_list('TB_MEMBERS_CHILDREN',array('MC_ACCOUNTCODE'=>$M_ACCOUNTCODE));
        for($z=0; $z<count($data['MC_list']); $z++) {
        	$Ymd_date=$data['MC_list'][$z]['MC_BIRTHDATE'];
			$mc_year=substr($Ymd_date,0,4);
			$mc_month=substr($Ymd_date,4,2);
			$mc_day=substr($Ymd_date,6,2); 
			$b_year=$mc_year-10;
			$n_year=$mc_year+10;
			$mc_year_list="";
			for($i=$b_year;$i<$n_year;$i++){
				$selected="";
				if($i==$mc_year)$selected="selected='selected'";
				else $selected="";
				$mc_year_list .="<option value='".$i."' ".$selected.">".$i."년</option>";
			}			
			
			$mc_month_list="";
			for($i=1;$i<13;$i++){
				$selected="";
				
				if(str_pad($i,"2","0",STR_PAD_LEFT)==$mc_month)$selected="selected='selected'";
				else $selected="";
				$mc_month_list .="<option value='".str_pad($i,"2","0",STR_PAD_LEFT)."'  ".$selected.">".$i."월</option>";
			}
		
			$mc_day_list="";
			for($i=1;$i<=31;$i++){
				$selected="";
				if(str_pad($i,"2","0",STR_PAD_LEFT)==$mc_day)$selected="selected='selected'";
				else $selected="";
				$mc_day_list .="<option value='".str_pad($i,"2","0",STR_PAD_LEFT)."'  ".$selected.">".$i."일</option>";
			}
			$data['MC_list'][$z]['mc_year'] =$mc_year;
			$data['MC_list'][$z]['mc_month'] =$mc_month;
			$data['MC_list'][$z]['mc_day'] =$mc_day;
			$data['MC_list'][$z]['mc_year_list'] =$mc_year_list;
			$data['MC_list'][$z]['mc_month_list'] =$mc_month_list;
			$data['MC_list'][$z]['mc_day_list'] =$mc_day_list;
		}
		//자녀 셋팅//
	
		//기본값//
		$year=date("Y");
		$b_year=$year-10;
		$n_year=$year+10;
		$year_list="";
		for($i=$b_year;$i<$n_year;$i++){
			
			$selected="";
			if($year==$i)$selected="selected='selected'";
			else $selected="";
			$year_list .="<option value='".$i."' ".$selected.">".$i."년</option>";
		}
		
		$month=date("m");
		$month_list="";
		for($i=1;$i<13;$i++){
			$month_list .="<option value='".$i."' >".$i."월</option>";
		}
		
		for($i=1;$i<=31;$i++){
			$day_list .="<option value='".$i."' >".$i."일</option>";
		}
		
		$data['year_list']=$year_list;
		$data['month_list']=$month_list;
		$data['day_list']=$day_list;
		$this ->_print($data);
	}
	
	public function mypage_process(){
		$params = $this->input->post();
		/*-세션 가져오기-*/
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$this->member_lib->urlCheck(array('data'=>$M_ACCOUNTCODE));
		$this->member_lib->loginCheck();
		$params['M_ACCOUNTCODE']=$M_ACCOUNTCODE;
		$params['M_ACCOUNT']=$M_ACCOUNT;
		$old_data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT));
		if($params['hp_chk']=="Y"){
			$params['M_HP']=$params['M_HP_1']."-".$params['M_HP_2']."-".$params['M_HP_3'];
		}else{
			$params['M_HP']=$old_data['M_HP'];
		}
		
		if($params['M_CP_2']!="" && $params['M_CP_3']!=""){
			$params['M_CP']=$params['M_CP_1']."-".$params['M_CP_2']."-".$params['M_CP_3'];
		}else{
			$params['M_CP']="";
		}
		$params['M_EMAIL']=$params['M_EMAIL_1']."@".$params['M_EMAIL_2_txt'];
		$params['M_IP']=$_SERVER['REMOTE_ADDR'];
		
		$reslut="";
		if($params['M_CP']!=$old_data['M_CP'])$reslut.="전화";
		if($params['M_EMAIL']!=$old_data['M_EMAIL'])$reslut.="이메일,";
		if($params['M_POST']!=$old_data['M_POST']){
			$reslut.="주소,";
		}else if($params['M_ADDR1']!=$old_data['M_ADDR1']){
			$reslut.="주소,";
		}else if($params['M_ADDR2']!=$old_data['M_ADDR2']){
			$reslut.="주소,";
		}
		if($params['M_SMS_YN']!=$old_data['M_SMS_YN'])$reslut.="SMS수신여부,";
		if($params['M_EMS_YN']!=$old_data['M_EMS_YN'])$reslut.="EMS수신여부,";
		if($reslut!=""){
			$reslut=substr($reslut, 0, -1);	
			$logparams['ME_M_ACCOUNTCODE']=$M_ACCOUNTCODE;
			$logparams['ME_ACCOUNT']=$M_ACCOUNT;
			$logparams['ME_NAME']=$old_data['M_NAME'];
			$logparams['ME_LOG']=$reslut;
			$logparams['ME_TYPE']="P";
			$logparams['ME_IP']=$_SERVER['REMOTE_ADDR'];
			$this->my_model->mypage_log($logparams);//기본정보 입력
		}
		
		
		
		$this->my_model->mypage_modify($params);//기본정보 입력
		/*
		foreach ($params as $k => $v) {
	        
	        echo "$k => $v<br/>";
	    }*/
		
		
		$ch_count=count($params['ch_name']);
		$mccount=0;
		if($M_ACCOUNTCODE !="" && $M_ACCOUNTCODE !="0"){
			if($ch_count>1){
				$this->common_model->_delete('TB_MEMBERS_CHILDREN',array('MC_ACCOUNTCODE'=>$M_ACCOUNTCODE));
				$ch_count=$ch_count-1;
				for($i=1;$i<=$ch_count;$i++){
					if($params['ch_name'][$i]!=""){
						$MC_BIRTHDATE=$params['ch_year'][$i].str_pad($params['ch_month'][$i],"2","0",STR_PAD_LEFT).str_pad($params['ch_day'][$i],"2","0",STR_PAD_LEFT);
						$MC_GENDER=$params['ch_gender'][$i];
						$MC_NAME=$params['ch_name'][$i];
						$this->join_model->children_join(array('MC_ACCOUNTCODE'=>$M_ACCOUNTCODE,'MC_BIRTHDATE'=>$MC_BIRTHDATE,'MC_NAME'=>$MC_NAME,'MC_GENDER'=>$MC_GENDER,'MC_IP'=>$_SERVER['REMOTE_ADDR']));//기본정보 입력
						$mccount++;
					}
				}
			}else{
				$this->common_model->_delete('TB_MEMBERS_CHILDREN',array('MC_ACCOUNTCODE'=>$M_ACCOUNTCODE));
			}
		}

		if($old_data['M_MCPOINT']<1500){
			$add_point=$mccount*500;	
			if($add_point>0){
				$this->common_model->_modify('TB_MEMBERS',array('M_MCPOINT'=>$add_point),array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_USEYN'=>'Y'));
				$ch_point=$this->point_model->sms_children_count(array('PL_ACCOUNTCODE'=>$M_ACCOUNTCODE));
				if($ch_point['pl_point']<$add_point)
				{
					//포인트 셋팅
					$point['ACCOUNTCODE']=$M_ACCOUNTCODE;
					$point['code']="PC02";
					$point['use']="적립";
					$point['point']=$add_point-$ch_point['pl_point'];
					$point['memo']="아기등록";
					$this->add_point($point);
				}
				
			}
		}
		/*
		$this->load->helper('url');
		redirect("/my/mypage");
		 * */
		$this->javascript_lib->top_location(array('msg'=>'수정되었습니다.','nextUrl'=>"/my/mypage"));		
	}
	public function pw_change(){
		$this->member_lib->loginCheck();
		//임의설정
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y'));
		$this ->_print($data);
	}
	public function pw_chk(){
		$params = $this->input->get();
		
		//임의설정
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$this->member_lib->urlCheck(array('data'=>$M_ACCOUNTCODE));
		$this->member_lib->loginCheck();
		$result="N";
		$params['pwori']  = hash('sha256',$params['pwori'],true);
		$params['pwori'] =  base64_encode($params['pwori']);
		$data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y','M_PW'=>$params['pwori'] ));
		if($data){
			$result="Y";
			if($M_ACCOUNTCODE!="0"&&$M_ACCOUNTCODE!=""){
				$params['pw']  = hash('sha256',$params['pw'],true);
				$params['pw'] =  base64_encode($params['pw']);
				$this->common_model->_modify("TB_MEMBERS",array('M_PW'=>$params['pw']),array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y'));

			}
		}else{
			$result="N";
		}
		
		echo $result;
	}
	
	//회원탈퇴
	public function withdrawal(){
		$params = $this->input->get();
		$this->member_lib->loginCheck();
		//임의설정
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y'));
		$data['pldata']=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
		$this->_print($data);
	}
	public function withdrawal_process(){
		$params = $this->input->post();
		$this->member_lib->loginCheck();
		//임의설정
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$this->member_lib->urlCheck(array('data'=>$M_ACCOUNTCODE));
		$this->member_lib->loginCheck();
		$params['M_PW']  = hash('sha256',$params['M_PW'],true);
		$params['M_PW'] =  base64_encode($params['M_PW']);
		$params['M_IP']=$_SERVER['WL_IP'];
		$data=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y','M_PW'=>$params['M_PW'] ));
		if($data){
			$data['pldata']=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
			$params['M_CERTKEY']=$data['M_CERTKEY'];
			$params['M_ACCOUNT']=$data['M_ACCOUNT'];
			$params['M_ACCOUNTCODE']=$data['M_ACCOUNTCODE'];
			if($data['pldata']['PL_TOTALPOINT']>0){

				$point['ACCOUNTCODE']=$data['M_ACCOUNTCODE'];
				$point['code']="PC15";
				$point['use']="차감";
				$point['point']=$data['pldata']['PL_TOTALPOINT'];
				$point['title']="탈퇴";
				$point['memo']="탈퇴처리";
				$this->out_point($point);
			}
			$this->my_model->del_member($params);
			$this->my_model->del_log($params);
			//탈퇴 복귀 로직 개발예정
			$this->load->helper('url');
			redirect("/login/withdrawal_end?CODE=".$M_ACCOUNT);
		}else{
			$this->javascript_lib->top_location(array('msg'=>'비밀번호가 다릅니다.','nextUrl'=>'/my/withdrawal'));
			
		}
		
	}


	//회원탈퇴 끝
	//1:1 문의
	public function my_qnalist(){
		$params = $this->input->get();
		$this->member_lib->loginCheck();
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
 
        
        $params['ACCOUNTCODE']=$M_ACCOUNTCODE;
        $params['count_yn']="Y";
        $params["maxcount"] = $this->my_model->my_qnalist($params); // pgfile_카운트  정보
        $page_cnt=10;

        if($params['per_page']=="" || $params['per_page']<1){
            $page= 1;
            $params['per_page'] = 0;
        }else {
            $page= $params['per_page'];
        }

        $data["pagination"]=$this->get_pagination('/my/my_qnalist?',count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;
        $params['count_yn']="N";
        $data['list'] = $this->my_model->my_qnalist($params);
        $data['start_no']=count($params["maxcount"])-$result_page;
        $data['per_page']=$params['per_page'];
		$this->_print($data);
	}
	//1:1 문의 끝
	//포인트
	public function point_list(){
		$params = $this->input->get();
		$this->member_lib->loginCheck();
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data['pldata']=$this->point_model->rowdata_pl(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
		$data['ptdata']=$this->point_model->outdata_pt(array('PT_ACCOUNTCODE'=>$M_ACCOUNTCODE));
		
		$params['where'].=" and PL_ACCOUNTCODE ='".$M_ACCOUNTCODE."' ";
		
        $params['count_yn']='Y';
        $params["maxcount"] = $this->point_model->point_list($params); // pgfile_카운트  정보 갖고오기
        $page_cnt=10;

        if($params['per_page']=="" || $params['per_page']<1)$page= 1;
        else $page= $params['per_page'];
        $params['count_yn']='N';
        $data["pagination"]=$this->get_pagination('/my/point_list?',count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;

        $data['list'] = $this->point_model->point_list($params);
		for($i=0;$i<sizeof($data['list'] );$i++) {
			$data['list'][$i]['PL_CODE_TXT'] = $this->get_code_txt($data['list'][$i]['PL_CODE']);
		}
        $data['start_no']=count($params["maxcount"])-$result_page;
        $data['per_page']=$params['per_page'];
		$data['PL_ACCOUNTCODE']=$params['PL_ACCOUNTCODE'];
		
        $this->_print($data);
	}
	//포인트끝
	//회원 주문
		
	public function my_orderlist(){
		$params = $this->input->get();
		$this->member_lib->loginCheck();
		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
        if($params['search_type']!=""){
            //$params['search_type']
            
        }
		if($params['search_date']!=""){
            if($params['search_date']!="")$params['where'].=" and ".$params['search_F']." like '%".$params['search_S']."%'";
        }
        
        $params['ACCOUNTCODE']=$M_ACCOUNTCODE;
        $params['count_yn']="Y";
        $params["maxcount"] = $this->my_model->my_orderlist($params); // pgfile_카운트  정보
        $page_cnt=4;

        if($params['per_page']=="" || $params['per_page']<1){
            $page= 1;
            $params['per_page'] = 0;
        }else {
            $page= $params['per_page'];
        }

        $data["pagination"]=$this->get_pagination('/diary/special?search_F='.$params['search_F'].'&search_S='.$params['search_S'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;
        $params['count_yn']="N";
        $data['list'] = $this->my_model->my_orderlist($params);
        $data['start_no']=count($params["maxcount"])-$result_page;
        $data['search_S']=$params['search_S'];
        $data['search_F']=$params['search_F'];
        $data['per_page']=$params['per_page'];
		$this->_print($data);
	}

	public function set_state(){
        $params=$this->input->get();
		$data = $this->my_model->my_orderview($params);
		if($data['OP_STATE']=="2"){
			$this->common_model->_modify('TB_ORDER_PAYMENT',array('OP_STATE'=>$params['code']),array('OP_OD_IDX'=>$data['OD_IDX']));
		}
    }
	

	
	public function my_orderview(){
		$params = $this->input->get();
		
		$this->member_lib->urlCheck(array('data'=>$params['OM_IDX']));
		$this->member_lib->loginCheck();
		$data=$this->my_model->my_orderview(array('OM_IDX'=>$params['OM_IDX']));
		
		$data['HP']=explode("-",$data['OM_HP']);
		$data['CP']=explode("-",$data['OM_CP']);
		//집전화//
		$data['cp_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'CP','C_CODE_DEPTH'=>1,'C_REAL_YN'=>'Y','oType'=>'asc','oKey'=>'C_CODE_ORDERBY'));
		$cp='';		
		if($data['OM_CP']!=''){
			$cp=explode('-',$data['OM_CP']);
		}
		for($i=0;$i<sizeof($data["cp_list"] );$i++) {
			$sel="";
			if($data["cp_list"][$i]['C_CODE_PA']==$cp[0]) $sel='selected="selected"';
            $cp_list.="<option ".$sel." value='{$data["cp_list"][$i]['C_CODE_PA']}'>{$data["cp_list"][$i]['C_CODE_PA']}</option>";
        }
		$data['cp_list']=$cp_list;
		
		$data['state'] = $this->set_roder_state($data['OP_STATE']);
		$this->_print($data);
	}
	
	public function my_order_process(){
		$params=$this->input->post();
		$this->member_lib->urlCheck(array('data'=>$params['OM_NAME']));
		$HP=$params['pay_hp1']."-".$params['pay_hp2']."-".$params['pay_hp3'];
		$CP=$params['pay_cp1']."-".$params['pay_cp2']."-".$params['pay_cp3'];
		$this->common_model->_modify('TB_ORDER_MASTER',array('OM_NAME'=>$params['OM_NAME'],'OM_HP'=>$HP,'OM_CP'=>$CP,'OM_POST'=>$params['OM_POST'],'OM_ADDR1'=>$params['OM_ADDR1'],'OM_ADDR2'=>$params['OM_ADDR2'],'OM_MEMO'=>$params['OM_MEMO']),array('OM_IDX'=>$params['OM_IDX']));
		
		$data['state'] = $this->set_roder_state($data['OP_STATE']);
		$data['search_state']=$params['search_state'];
        $data['per_page']=$params['per_page'];
        $data['search_S']=$params['search_S'];
        $data['search_F']=$params['search_F'];
        $this->load->helper('url');
		redirect("/my/my_orderview?OM_IDX=".$params['OM_IDX']."&search_state=".$params['search_state']."&per_page=".$params['per_page']."&search_S=".$params['search_S']."&search_F=".$params['search_F']);
	}
	
	//회원 주문 끝 
	
}