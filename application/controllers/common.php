<?php
/*
| -------------------------------------------------------------------
| @ TITLE   공용 컨트롤러
| @ AUTHOR  JoonCh
| @ SINCE   14. 5. 9.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/
header('Content-Type: text/html; charset=UTF-8');
//require_once($_SERVER['DOCUMENT_ROOT']."/sms/sms.api.class.php");
class common extends CI_Controller {
/*
	foreach ($arr as $key => $value) {
		echo "Key: $key; Value: $value<br />\n";
	}
*/
    protected $cfg = array(
        "module_dir_name" => "_wns",
        "title" => "관리자 페이지 입니다.",
        'metaDescription' => "",
        "session_key" => "mbID",
        "perpage" => 10,
    );
    public $scripts = array();
    public $manager = array();
    public $is_super_admin = FALSE;


    public function __construct(){
        parent::__construct();
        define('THIS', $this->router->class);
        define('THIS_MODEL', $this->router->class."_model");
        $this->load->model(THIS."_model");
		
        $this->config->load('cache_config');
        $cacheConfig = $this->config->item('cacheList');
        /*
        if(in_array($this->router->class.'_'.$this->router->method, $cacheConfig)){
            $this->template->define("tpl", $this->router->directory.$this->router->class.'/'.$this->router->method.".html");
            $this->template->setCache('tpl', 86400);
            if ($this->template->isCached('tpl')) {
                $this->_print();
                exit;
            }
        }
        */
    }
    /**
     * 검색세팅 : POST -> URI Segment로 변경하여 Redirect
     * @param array $exceptParams array("edDcType","edIsOver");
     */
    public function _set_sec($exceptParams = NULL) {
        //제외항목 우선 제거
        if(is_array($exceptParams)) {
            foreach($exceptParams as $val) {
                if(isset($_POST[$val])) {
                    unset($_POST[$val]);
                }
            }
        }

        $params = $this->input->post();
        $paramsData = array();

        if(is_array($params)) {
            foreach($params as $key => $val) {
                if( is_array($this->input->post($key)) ) {
                    if(! $val[0] && ! $val[1]) {
                        continue;
                    }
//                    $paramsData[$key] = "Array";
//                    $paramsData[$key."_CNT"] = count($this->input->post($key));
                    foreach($this->input->post($key) as $key1 => $data) {
                        $paramsData[$key."_".$key1] = urlencode($data);
                    }
                    continue;
                }
                if(! $val) {
                    continue;
                }
                $paramsData[$key] = urlencode($val);
            }

            $url = site_url("/".$this->uri->uri_string()."/".$this->uri->assoc_to_uri($paramsData));
            redirect($url);
        }
    }

    public function get_code_txt($code){
        $this->load->helper('string');
        $this->load->model("common_model");
        $data=$this->common_model->_select_row('COMMON_CODE',array('C_CODE_PA'=>$code));
        if($data['C_CODE_TXT']!=""){
            $code_txt=$data['C_CODE_TXT'];
        }else{
            $code_txt='없음';
        }
        return $code_txt;
    }


    /**
     * 검색세팅 : URI Segment를 Key-Value 배열로 생성
     * @param array $assoc (URI Segment No)
     */
    public function _get_sec($assoc=1) {
        $uriAry = $this->uri->ruri_to_assoc($assoc);
        $return = array();
        foreach ($uriAry as $key => $data) {
            if($key == $this->router->class) continue;
            if ($data === "Array") {
                $tmpAry = array();
                for ($i = 0; $i < $uriAry[$key . "_CNT"]; $i++) {
                    $tmpAry[] = $uriAry[$key . "_" . $i];
                }
                $return[$key] = $tmpAry;
                continue;
            }
            if (strrpos($key, "_CNT")) {
                continue;
            }
            $return[$key] = $data;
        }
        return $return;
    }




    /**
     * 페이징 : Codeigniter Pagination Library config & load
     * @param array $params
     */
    public function _set_pager($params) {
        $config["suffix"] = $this->config->item("url_suffix");

        $config["total_rows"] = $params["CNT"];
        $config["per_page"] = $params["PRPAGE"];

        $url = preg_replace("/\/page\/(.*)/", "", "/".$this->uri->uri_string());
        $config["first_url"] = $url.$config["suffix"];
        $config["base_url"] = "/ci/".$url."/page/";
        $config["uri_segment"] = count(explode("/", $url)) + 1;

        $config["num_links"] = 10;
        $config['full_tag_open'] = '<div class="text-center"><ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul></div>';
        $config["first_link"] = "처음";
        $config["first_tag_open"] = "";
        $config["first_tag_close"] = "";
        $config["prev_link"] = "&lt;";
        $config["prev_tag_open"] = "";
        $config["prev_tag_close"] = "";

        $config["next_link"] = "&gt;";
        $config["next_tag_open"] = "";
        $config["next_tag_close"] = "";
        $config["last_link"] = "끝";
        $config["last_tag_open"] = "";
        $config["last_tag_close"] = "";

        $config["cur_tag_open"] = "<li class='active'><a href=\"#\">";
        $config["cur_tag_close"] = "</a></li>";
        $this->load->library("pagination");
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }



    /**
     *  템플릿언더바 사용
     * @param array $params
     */
    public function _print($data=array(), $result=FALSE) {
        $this->_set_value($data);
        $this->_set_modules($data);
        $data['GNS_IMAGES_DIR'] = GNS_IMAGES_DIR;
        $data['VIEW_DIR'] = APPPATH.'views';
        $data['APPPATH']=APPPATH;
        $this->template->assign($data);

        if($result === TRUE) {
            $this->template->define("tpl", $data['templateHtml']);
        } else {
            $this->template->define("tpl", $this->router->directory.$this->router->class.'/'.$this->router->method.".html");
        }

        /*
        $this->config->load('cache_config');
        $cacheConfig = $this->config->item('cacheList');

        if(in_array($this->router->class.'_'.$this->router->method, $cacheConfig)){
            $this->template->setCache('tpl', 0);
        }
        */




        if($result === TRUE) {
            return $this->template->fetch("tpl");
        } else {
            $this->template->print_("tpl");
        }
    }



    /**
     *  로그인 여부 및 설정 정보 세팅
     * @param array $params
     */
    private function _set_value(&$data) {
        $uagent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($uagent, 'iPhone') == true){
            $data["environment"] = "mobile";
            $data["device"] = "iphone";

        } else if (strpos($uagent, 'iPad') == true) {
            $data["environment"] = "mobile";
            $data["device"] = "ipad";

        }  else if (strpos($uagent, 'Android') == true) {
            $data["environment"] = "mobile";
            $data["device"] = "android";

        } else if(strpos($uagent, 'Mac') == true){
            $data["environment"] = "mac";
            $data["device"] = "mac";

        } else {
            $data["environment"] = "pc";
        }

        $data["scripts"] = array_unique($this->scripts);
        $data["url_suffix"] = $this->config->item("url_suffix");
        $data["uri_string"] = urldecode($this->uri->uri_string());
        $data["rsegment_1"] = urldecode($this->uri->rsegment(1));
        $data["rsegment_2"] = urldecode($this->uri->rsegment(2));
        $data['controller'] = $this->router->class;
        $data['method']     = $this->router->method;
        $data['dir']     = $this->router->directory;
        $data["referer"] = $this->input->server("HTTP_REFERER");
        $data["req_uri"] = $this->input->server("REQUEST_URI");
        $data["cfg"] = $this->cfg;

        $controller_chk = "";

        if($data['controller'] == "contact" || $data['controller'] == "diary" || $data['controller'] == "shop" || $data['controller'] == "event" || $data['controller'] == "join" || $data['controller'] == "my" || $data['controller'] == "order" || $data['controller'] == "story" ){
            $controller_chk = "Y";
        }else{
            $controller_chk = "N";
        }

        $data['controller_chk'] = $controller_chk;

        if(! isset($data["title"])) {
            $data["title"] = $this->cfg["title"];
        }
        
        if(! isset($data["metaDescription"])) {
            $data["metaDescription"] = $this->cfg["metaDescription"];
        }

        if($this->session->userdata('USER_ACCOUNTCODE')){
            $data["isMember"] = 'YES';
            $data["USER_ACCOUNT"] = $this->session->userdata('USER_ACCOUNT');
            $data["USER_NAME"] = $this->session->userdata('USER_NAME');
            $data["USER_ACCOUNTCODE"] = $this->session->userdata('USER_ACCOUNTCODE');
        }

    }



    /**
     *  header/footer/menu 모듈화 하기
     * @param array $params
     */
    private function _set_modules(&$data) {
        $filePath = APPPATH."views/".$this->cfg["module_dir_name"];
        $map = directory_map($filePath);
        if(is_array($map)) {
            $modulesList = array();
            foreach($map as $dir => $dirRow) {
                if(is_array($dirRow)) {
                    foreach($dirRow as $k => $modulePath) {
                        if(is_array($modulePath)) {
                            foreach($modulePath as $v) {
                                $htmlPath = $this->cfg["module_dir_name"]."/".$dir."/".$k."/".$v;
                                $modulesList[$dir."_".$k."_".substr($v,0,-5)] = $htmlPath;
                            }
                        }else {
                            $htmlPath = $this->cfg["module_dir_name"]."/".$dir."/".$modulePath;
                            $modulesList[$dir."_".substr($modulePath,0,-5)] = $htmlPath;
                        }
                    }
                }
            }
            $this->template->define($modulesList);
        }
        return TRUE;
    }

    public function send_kakao($to,$from,$text,$temp_code){
		$array_button = array(
					"button_type" => "WL",
					"button_name" => "슈어엠주식회사",
					"button_url" => "http://www.surem.co.kr"
		);
		$array_messages = array(
				"message_id" => "1000000",
				"to" => $to,
				"text" => $text,
				"from" => $from,
				"template_code" => $temp_code,
				"reserved_time" => "",
				"re_send" => "Y",
				"re_text" => "",
				"buttons" => array ($array_button)
		);
		$array_post_data = array(
				"usercode" => 'sjtest1',
				"deptcode" => '2B-DGO-O8',
				"yellowid_key" => "a6fb47772e56c22e5ae7a6cc77032498c9b44073",
				"messages" => array ($array_messages)
		);

		$data_string= json_encode($array_post_data);
		$url = "https://api.surem.com/alimtalk/v2/json";

		$ch=curl_init();
		// user credencial
		curl_setopt($ch, CURLOPT_USERPWD, "username:passwd");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		// post_data
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		//curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_VERBOSE, true);

		$response = curl_exec($ch);
		return $response;
    }

	public function get_pagination_addr($base_url, $total_rows, $per_page){ 
	    $this->load->library('pagination');
		$config = array(
			'base_url'          => $base_url,
			'total_rows'        => $total_rows,
			'per_page'          => $per_page,
			'num_links'         => 5,
			'use_page_numbers'  => true,
			'page_query_string' => true,
			'uri_segment'       => 5,
			'full_tag_open'     => '<ul class="pagination pull-right">',
			'full_tag_close'    => '</ul>',
			'first_link'        => '<<',
			'first_tag_open'    => '<li>',
			'first_tag_close'   => '</li>',
			'last_link'         => '>>',
			'last_tag_open'     => '<li>',
			'last_tag_close'    => '</li>',
			'next_link'         => '>',
			'next_tag_open'     => '<li>',
			'next_tag_close'    => '</li>',
			'prev_link'         => '<',
			'prev_tag_open'     => '<li>',
			'prev_tag_close'    => '</li>',
			'cur_tag_open'      => '<li class="active"><a>',
			'cur_tag_close'     => '</a></li>',
			'num_tag_open'      => '<li>',
			'num_tag_close'     => '</li>'
		);

		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();
		return $pagination;
	}

	public function get_pagination($base_url, $total_rows, $per_page){ 
        $this->load->library('pagination');
        $config = array(
            'base_url'          => $base_url,
            'total_rows'        => $total_rows,
            'per_page'          => $per_page,
            'num_links'         => 2,
            'use_page_numbers'  => true,
            'page_query_string' => true,
            'uri_segment'       => 3,
            'full_tag_open'     => '<ul class="paging">',
            'full_tag_close'    => '</ul>',
            'first_link'        => '',
            'first_tag_open'    => '<li class="btn_nav first">',
            'first_tag_close'   => '</li>',
            'last_link'         => '',
            'last_tag_open'     => '<li class="btn_nav last">',
            'last_tag_close'    => '</li>',
            'next_link'         => '',
            'next_tag_open'     => '<li class="btn_nav next">',
            'next_tag_close'    => '</li>',
            'prev_link'         => '',
            'prev_tag_open'     => '<li class="btn_nav prev">',
            'prev_tag_close'    => '</li>',
            'cur_tag_open'      => '<li class="num on"><a>',
            'cur_tag_close'     => '</a></li>',
            'num_tag_open'      => '<li class="num">',
            'num_tag_close'     => '</li>'
            
        );

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();
        return $pagination;
    }

	public function get_pagination_front($base_url, $total_rows, $per_page){ 
	    $this->load->library('pagination');
		$config = array(
			'base_url'          => $base_url,
			'total_rows'        => $total_rows,
			'per_page'          => $per_page,
			'num_links'         => 5,
			'use_page_numbers'  => true,
			'page_query_string' => true,
			'uri_segment'       => 5,
			'full_tag_open'     => '<div id="paging">',
			'full_tag_close'    => '</div>',
			'first_link'        => '<i class="fa fa-angle-double-left"></i>',
			'first_tag_open'    => '<span class="db-prev">',
			'first_tag_close'   => '</span>',
			'last_link'         => '<i class="fa fa-angle-double-right"></i>',
			'last_tag_open'     => '<span class="db-next">',
			'last_tag_close'    => '</span>',
			'next_link'         => '<i class="fa fa-angle-right"></i>',
			'next_tag_open'     => '<span class="next">',
			'next_tag_close'    => '</span>',
			'prev_link'         => '<i class="fa fa-angle-left"></i>',
			'prev_tag_open'     => '<span class="prev">',
			'prev_tag_close'    => '</span>',
			'cur_tag_open'      => '<a href="#" class="on">',
			'cur_tag_close'     => '</a>',
			'num_tag_open'      => '',
			'num_tag_close'     => ''
		);

		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links();
		return $pagination;
	}

    public function get_pagination_contact($base_url, $total_rows, $per_page){
        $this->load->library('pagination');
        $config = array(
            'base_url'          => $base_url,
            'total_rows'        => $total_rows,
            'per_page'          => $per_page,
            'num_links'         => 10,
            'use_page_numbers'  => true,
            'page_query_string' => true,
            'uri_segment'       => 10,
            'full_tag_open'     => '<div class="paging">',
            'full_tag_close'    => '</div>',
            'first_link'        => '<<',
            'first_tag_open'    => '<a class="btn_nav first">',
            'first_tag_close'   => '</a>',
            'last_link'         => '>>',
            'last_tag_open'     => '<a class="btn_nav last">',
            'last_tag_close'    => '</a>',
            'next_link'         => '>',
            'next_tag_open'     => '<a class="btn_nav next">',
            'next_tag_close'    => '</a>',
            'prev_link'         => '<',
            'prev_tag_open'     => '<a class="btn_nav prev">',
            'prev_tag_close'    => '</a>',
            'cur_tag_open'      => '<a href="#" class="num on">',
            'cur_tag_close'     => '</a>',
            'num_tag_open'      => '',
            'num_tag_close'     => ''
        );

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();
        return $pagination;
    }

	public function get_pagination_search($base_url, $total_rows, $per_page){ 
	    $this->load->library('pagination');
		$config = array(
			'base_url'          => $base_url,
			'total_rows'        => $total_rows,
			'per_page'          => $per_page,
			'num_links'         => 5,
			'use_page_numbers'  => true,
			'page_query_string' => true,
			'uri_segment'       => 5,
			'full_tag_open'     => '<div id="paging">',
			'full_tag_close'    => '</div>',
			'first_link'        => '<i class="fa fa-angle-double-left"></i>',
			'first_tag_open'    => '<span class="db-prev">',
			'first_tag_close'   => '</span>',
			'last_link'         => '<i class="fa fa-angle-double-right"></i>',
			'last_tag_open'     => '<span class="db-next">',
			'last_tag_close'    => '</span>',
			'next_link'         => '<i class="fa fa-angle-right"></i>',
			'next_tag_open'     => '<span class="next">',
			'next_tag_close'    => '</span>',
			'prev_link'         => '<i class="fa fa-angle-left"></i>',
			'prev_tag_open'     => '<span class="prev">',
			'prev_tag_close'    => '</span>',
			'cur_tag_open'      => '<a href="#" class="on">',
			'cur_tag_close'     => '</a>',
			'num_tag_open'      => '',
			'num_tag_close'     => ''
		);

		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links_post();
		return $pagination;
	}
	public function url_get_pagination_search($base_url, $total_rows, $per_page){ 
	    $this->load->library('pagination');
		$config = array(
			'base_url'          => $base_url,
			'total_rows'        => $total_rows,
			'per_page'          => $per_page,
			'num_links'         => 5,
			'use_page_numbers'  => true,
			'page_query_string' => true,
			'uri_segment'       => 5,
			'full_tag_open'     => '<div id="paging">',
			'full_tag_close'    => '</div>',
			'first_link'        => '<i class="fa fa-angle-double-left"></i>',
			'first_tag_open'    => '<span class="db-prev">',
			'first_tag_close'   => '</span>',
			'last_link'         => '<i class="fa fa-angle-double-right"></i>',
			'last_tag_open'     => '<span class="db-next">',
			'last_tag_close'    => '</span>',
			'next_link'         => '<i class="fa fa-angle-right"></i>',
			'next_tag_open'     => '<span class="next">',
			'next_tag_close'    => '</span>',
			'prev_link'         => '<i class="fa fa-angle-left"></i>',
			'prev_tag_open'     => '<span class="prev">',
			'prev_tag_close'    => '</span>',
			'cur_tag_open'      => '<a href="#" class="on">',
			'cur_tag_close'     => '</a>',
			'num_tag_open'      => '',
			'num_tag_close'     => ''
		);

		$this->pagination->initialize($config);
		$pagination = $this->pagination->create_links_get();
		return $pagination;
	}
	
	//포인트 저장
	public function add_point($params){
		$this->load->helper('string');
        $this->load->model("point_model");
		$ACCOUNTCODE=$params["ACCOUNTCODE"];
		 //필요사항 
		$pldata['PL_ACCOUNTCODE']=$ACCOUNTCODE;
		$pldata['PL_TYPE']="U";
		$pldata['PL_CODE']=$params['code'];
		$pldata['PL_POINT']=$params['point'];
		$pldata['PL_TITLE']=$params['title'];
		$pldata['PL_USE']=$params['use'];
		$pldata['PL_MEMO']=$params['memo'];
		$pldata['PL_IP']=$_SERVER['REMOTE_ADDR'];
		$pl_idx=$this->point_model->pl_insert($pldata);
	
		$ptdata['PT_PL_IDX']=$pl_idx;
		$ptdata['PT_POINT']=$params['point'];
		$ptdata['PT_ACCOUNTCODE']=$ACCOUNTCODE;
		
		$this->point_model->pt_insert($ptdata);
		$ptudata['PT_ACCOUNTCODE']=$ACCOUNTCODE;
		$ptudata['PL_IDX']=$pl_idx;
		$this->point_model->pl_totalpoint_update($ptudata);
		$this->point_model->member_totalpoint_update($ptudata);
        
	}
	
	//포인트 소모
	public function out_point($params){
		$this->load->helper('string');
        $this->load->model("point_model");
		$ACCOUNTCODE=$params["ACCOUNTCODE"];
		 //필요사항 
		$pldata['PL_ACCOUNTCODE']=$ACCOUNTCODE;
		$pldata['PL_TYPE']="D";
		$pldata['PL_CODE']=$params['code'];
		$pldata['PL_POINT']=$params['point'];
		$pldata['PL_TITLE']=$params['title'];
		$pldata['PL_USE']=$params['use'];
		$pldata['PL_MEMO']=$params['memo'];
		$pldata['PL_IP']=$_SERVER['REMOTE_ADDR'];
		$pl_idx=$this->point_model->pl_insert($pldata);
		
		//차감 반복문 
		//pt테이블에 등록날짜 별로 한개씩가져와 포인트 차감
		 
		$out_point=$pldata['PL_POINT'];					//차감해야할 총 포인트

		$count=0;
		while ($out_point > 0) {
			if($out_point>0){
				$ptrdata=$this->common_model->pt_rowdata(array('PT_ACCOUNTCODE'=>$ACCOUNTCODE));
				$pt_point=$ptrdata['PT_POINT'];			//1row에 있는 총 포인트
				if($pt_point >=$out_point){
					$update_point=$pt_point-$out_point; // pt테이블 업데이트될 포인트
					$dbout_point=$out_point;				//DB용 포인트 부여
					$out_point=0;							//while문용 포인트 부여 무조건 0처리
					if($update_point==0) $ptrdata['PT_USE_YN']="Y";
					//pt table update, pc table insert   
				}else{
					$update_point=$out_point-$pt_point; // pt테이블 업데이트될 포인트
					$dbout_point=$pt_point;				//DB용 포인트 부여 무조건 0처리
					$out_point=$update_point;			//while문용 포인트 부여
					$update_point=0;
					$ptrdata['PT_USE_YN']="Y";
					//pt table 0 update ,pt select ,
				}
				$pcidata['PC_PL_IDX']=$ptrdata['PT_PL_IDX'];
				$pcidata['PC_PT_IDX']=$ptrdata['PT_IDX'];
				$pcidata['PT_POINT']=$dbout_point;
				$this->point_model->pc_insert($pcidata);			  //pc테이블 insert
				$this->point_model->pt_updatepoint(array('PT_POINT'=>$update_point,'PT_USE_YN'=>$ptrdata['PT_USE_YN'],'PT_IDX'=>$ptrdata['PT_IDX']));	  //pt테이블 업데이트
			}
			
		}
		$ptudata['PT_ACCOUNTCODE']=$ACCOUNTCODE;
		$ptudata['PL_IDX']=$pl_idx;
		$this->point_model->pl_totalpoint_update($ptudata);
		$this->point_model->member_totalpoint_update($ptudata);
		//echo $out_point."____".$count."--------------".$update_point;
	}
	
	
}



?>