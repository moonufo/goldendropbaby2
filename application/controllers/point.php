<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);

/*
| -------------------------------------------------------------------
| @ TITLE   point 페이지 컨트롤러
| @ AUTHOR cjs
| @ SINCE   18. 02. 26.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class point extends common {

    public function __construct(){
    	
        parent::__construct();
    }
	public function index(){
		$this->_print();
	}
	public function add_point_member(){
		$this->load->helper('string');
        $this->load->model("point_model");
		$ACCOUNTCODE='04lxatq9sTD';
		 //필요사항 
		$pldata['PL_ACCOUNTCODE']=$ACCOUNTCODE;
		$pldata['PL_TYPE']="U";
		$pldata['PL_CODE']='';
		$pldata['PL_POINT']='1000';
		$pldata['PL_TITLE']='1';
		$pldata['PL_USE']='지급';
		$pldata['PL_MEMO']='1000331';
		$pldata['PL_IP']=$_SERVER['REMOTE_ADDR'];
		$pl_idx=$this->point_model->pl_insert($pldata);
	
		$ptdata['PT_PL_IDX']=$pl_idx;
		$ptdata['PT_POINT']='1000';
		$ptdata['PT_ACCOUNTCODE']=$ACCOUNTCODE;
		
		$this->point_model->pt_insert($ptdata);
		$ptudata['PT_ACCOUNTCODE']=$ACCOUNTCODE;
		$ptudata['PL_IDX']=$pl_idx;
		$this->point_model->pl_totalpoint_update($ptudata);
		echo "<br/>완료";
        
	}
}
