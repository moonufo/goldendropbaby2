<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);
/*
| -------------------------------------------------------------------
| @ TITLE   일루마 주문
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class order extends common {

    public function __construct(){
        parent::__construct();
    }
	public function index() {
		$this->load->library('encrypt'); 
		echo $this->encrypt->encode(md5("star4uadmin"));
		$this ->_print();
	}
	//상품정보
	public function pddata(){
		$params=$this->input->get();
	 	$data=$this->common_model->_select_row('TB_PRODUCT_DATA',array('PD_PRODUCTCODE'=>$params['code'],'PD_REAL_YN'=>'Y'));
		if($params['batch_cycle']=="T") $data['trial']="Y";
		else $data['trial']="N";
		$this->_print($data);
	}
	//배송일정
	public function timedata(){
		$params=$this->input->get();
		$cycle=$params['batch_cycle'];
		$count=$params['batch_count'];
		$date=$params['batch_date'];
		
		$result="";
		$day_cycle=$cycle*7;
		$day_cycle_txt="+".$day_cycle."day";
	 	for($i=1;$i<=$count;$i++){
	 		
	 		$result.="<li><strong>".$i."회차</strong>";
			$date=str_replace("-","/",$date);
			$result.="– ".$date;
			
			if($i==1)$result.="(첫 정기배송)";
			if($count!=1){
				if($i==$count)$result.="(마지막 정기배송)";
			}
			$date=date("Y/m/d", strtotime( $date.$day_cycle_txt ) );
			$result.="</li>";
			
		}
		$data['timedata']=$result;
		$this->_print($data);
	}
	//주문 최종 정보
	public function orderdata(){
		$params=$this->input->get();
		$data=$this->common_model->_select_row('TB_PRODUCT_DATA',array('PD_PRODUCTCODE'=>$params['code'],'PD_REAL_YN'=>'Y'));
		if($params['batch_cycle']=="T") $data['trial']="Y";
		else $data['trial']="N";
		$data['type']=$params['type'];
		$data['count']=$params['batch_count'];
		$data['totalprice']=($data['PD_PRICE']*3)*$params['batch_count'];
		$this->_print($data);
	}
	public function torder_index(){
		$params=$this->input->get();
		$this->member_lib->loginCheck();
		$this ->_print($params);	
	}
	public function torder_paystep(){
		$params=$this->input->post();
		$data=$this->common_model->_select_row('TB_PRODUCT_DATA',array('PD_PRODUCTCODE'=>$params['product_code'],'PD_REAL_YN'=>'Y'));

		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data['member']=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y'));
		$data['member']['HP']=explode("-",$data['member']['M_HP']);
		$data['member']['CP']=explode("-",$data['member']['M_CP']);
		//집전화//
		$data['cp_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'CP','C_CODE_DEPTH'=>1,'C_REAL_YN'=>'Y','oType'=>'asc','oKey'=>'C_CODE_ORDERBY'));
		$cp='';		
		if($data['member']['M_CP']!=''){
			$cp=explode('-',$data['member']['M_CP']);
		}
		for($i=0;$i<sizeof($data["cp_list"] );$i++) {
			$sel="";
			if($data["cp_list"][$i]['C_CODE_PA']==$cp[0]) $sel='selected="selected"';
            $cp_list.="<option ".$sel." value='{$data["cp_list"][$i]['C_CODE_PA']}'>{$data["cp_list"][$i]['C_CODE_PA']}</option>";
        }
		$data['cp_list']=$cp_list;
		//집전화//		
		/*
		$data['product_code']="PD03";
		$data['batch_cycle']="2";
		$data['batch_count']="5";
		$data['totalprice']="500000";
		$data['batch_date']="2018-01-23";
		 */
		$data['product_code']=$params['product_code'];
		$data['batch_cycle']=$params['batch_cycle'];
		$data['batch_count']=$params['batch_count'];
		if($params['batch_cycle']=="T"){
			$data['batch_date']=date("Y-m-d");
			$data['totalprice']=($data['PD_PRICE']);
		}
		else{
			$data['batch_date']=$params['batch_date'];
			$data['totalprice']=($data['PD_PRICE']*3)*$params['batch_count'];
		 }
		$data['EP_batch_date']=str_replace("-","",$data['batch_date']);
		$data['txt_batch_date']=preg_replace('/(0)(\d)/','$2', $data['EP_batch_date']);
		 
		$code=1;
		$ORDERCODE="";
		while($code==1){
			$this->load->helper('string');
			$ORDERCODE=random_string('alnum', 11);
			$code=$this->get_code($ORDERCODE);	
		}
		
		$data['ordercode']=$ORDERCODE;
		$this ->_print($data);	
	}
	public function torder_paystep_batch(){
		$params=$this->input->post();
		$data=$this->common_model->_select_row('TB_PRODUCT_DATA',array('PD_PRODUCTCODE'=>$params['product_code'],'PD_REAL_YN'=>'Y'));

		$M_ACCOUNTCODE=$this->session->userdata('USER_ACCOUNTCODE');
		$M_ACCOUNT=$this->session->userdata('USER_ACCOUNT');
		$data['member']=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_ACCOUNT'=>$M_ACCOUNT,'M_USEYN'=>'Y'));
		$data['member']['HP']=explode("-",$data['member']['M_HP']);
		$data['member']['CP']=explode("-",$data['member']['M_CP']);
		//집전화//
		$data['cp_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'CP','C_CODE_DEPTH'=>1,'C_REAL_YN'=>'Y','oType'=>'asc','oKey'=>'C_CODE_ORDERBY'));
		$cp='';		
		if($data['member']['M_CP']!=''){
			$cp=explode('-',$data['member']['M_CP']);
		}
		for($i=0;$i<sizeof($data["cp_list"] );$i++) {
			$sel="";
			if($data["cp_list"][$i]['C_CODE_PA']==$cp[0]) $sel='selected="selected"';
            $cp_list.="<option ".$sel." value='{$data["cp_list"][$i]['C_CODE_PA']}'>{$data["cp_list"][$i]['C_CODE_PA']}</option>";
        }
		$data['cp_list']=$cp_list;
		//집전화//		
		/*
		$data['product_code']="PD03";
		$data['batch_cycle']="2";
		$data['batch_count']="5";
		$data['totalprice']="500000";
		$data['batch_date']="2018-01-23";
		 */
		$data['product_code']=$params['product_code'];
		$data['batch_cycle']=$params['batch_cycle'];
		$data['batch_count']=$params['batch_count'];
		if($params['batch_cycle']=="T"){
			$data['batch_date']=date("Y-m-d");
			$data['totalprice']=($data['PD_PRICE']);
		}
		else{
			$data['batch_date']=$params['batch_date'];
			$data['totalprice']=($data['PD_PRICE']*3)*$params['batch_count'];
		 }
		$data['EP_batch_date']=str_replace("-","",$data['batch_date']);
		$data['txt_batch_date']=preg_replace('/(0)(\d)/','$2', $data['EP_batch_date']);
		 
		$code=1;
		$ORDERCODE="";
		while($code==1){
			$this->load->helper('string');
			$ORDERCODE=random_string('alnum', 11);
			$code=$this->get_code($ORDERCODE);	
		}
		
		$data['ordercode']=$ORDERCODE;
		$this ->_print($data);	
	}
	public function order_index_1() {
		$code=1;
		$ORDERCODE="";
		while($code==1){
			$this->load->helper('string');
			$ORDERCODE=random_string('alnum', 11);
			$code=$this->get_code($ORDERCODE);	
		}
		
		$data['ordercode']=$ORDERCODE;
		$this ->_print($data);
	}
	public function order_index_batch(){
		$code=1;
		$ORDERCODE="";
		while($code==1){
			$this->load->helper('string');
			$ORDERCODE=random_string('alnum', 11);
			$code=$this->get_code($ORDERCODE);	
		}
		
		$data['ordercode']=$ORDERCODE;
		//$data['price']=$ORDERCODE;
		//$data['productcode']=$ORDERCODE;
		
		
		$this ->_print($data);
	}
	public function batch_auth(){
		
		$code=1;
		$ORDERCODE="";
		while($code==1){
			$this->load->helper('string');
			$ORDERCODE=random_string('alnum', 11);
			$code=$this->get_code($ORDERCODE);	
		}
		
		$data['ordercode']=$ORDERCODE;
		$this ->_print($data);
	}
	
	public function iframe_req()
	{
		$this ->_print();
	}
	public function popup_req()
	{
		$this ->_print();
	}
	
	public function order_req()
	{
		$this ->_print();
	}
	public function order_log(){
		$params=$this->input->post();
		foreach ($params as $key => $value) {
		//	echo "key : ".$key." value : ".$value."<br/>";
		}
		//결제사 리턴컬럼
		$result_msg = "";
		$r_res_cd         = $params[ "res_cd"         ];  // 응답코드
		$r_res_msg        = $params[ "res_msg"        ];  // 응답 메시지
		$r_cno            = $params[ "cno"            ];  // PG거래번호
		$r_memb_id        = $params[ "memb_id"        ];  // 가맹점 ID
		$r_amount         = $params[ "amount"         ];  // 총 결제금액
		$r_order_no       = $params[ "order_no"       ];  // 주문번호
		$r_noti_type      = $params[ "noti_type"      ];  // 노티구분 변경(20), 입금(30), 에스크로 변경(40)
		$r_auth_no        = $params[ "auth_no"        ];  // 승인번호
		$r_tran_date      = $params[ "tran_date"      ];  // 승인일시
		$r_card_no        = $params[ "card_no"        ];  // 카드번호
		$r_issuer_cd      = $params[ "issuer_cd"      ];  // 발급사코드
		$r_issuer_nm      = $params[ "issuer_nm"      ];  // 발급사명
		$r_acquirer_cd    = $params[ "acquirer_cd"    ];  // 매입사코드
		$r_acquirer_nm    = $params[ "acquirer_nm"    ];  // 매입사명
		$r_install_period = $params[ "install_period" ];  // 할부개월
		$r_noint          = $params[ "noint"          ];  // 무이자여부
		$r_bank_cd        = $params[ "bank_cd"        ];  // 은행코드
		$r_bank_nm        = $params[ "bank_nm"        ];  // 은행명
		$r_account_no     = $params[ "account_no"     ];  // 계좌번호
		$r_deposit_nm     = $params[ "deposit_nm"     ];  // 입금자명
		$r_expire_date    = $params[ "expire_date"    ];  // 계좌사용만료일
		$r_cash_res_cd    = $params[ "cash_res_cd"    ];  // 현금영수증 결과코드
		$r_cash_res_msg   = $params[ "cash_res_msg"   ];  // 현금영수증 결과메시지
		$r_cash_auth_no   = $params[ "cash_auth_no"   ];  // 현금영수증 승인번호
		$r_cash_tran_date = $params[ "cash_tran_date" ];  // 현금영수증 승인일시
		$r_cp_cd          = $params[ "cp_cd"          ];  // 포인트사
		$r_used_pnt       = $params[ "used_pnt"       ];  // 사용포인트
		$r_remain_pnt     = $params[ "remain_pnt"     ];  // 잔여한도
		$r_pay_pnt        = $params[ "pay_pnt"        ];  // 할인/발생포인트
		$r_accrue_pnt     = $params[ "accrue_pnt"     ];  // 누적포인트
		$r_escrow_yn      = $params[ "escrow_yn"      ];  // 에스크로 사용유무
		$r_canc_date      = $params[ "canc_date"      ];  // 취소일시
		$r_canc_acq_date  = $params[ "canc_acq_date"  ];  // 매입취소일시
		$r_refund_date    = $params[ "refund_date"    ];  // 환불예정일시
		$r_pay_type       = $params[ "pay_type"       ];  // 결제수단
		$r_auth_cno       = $params[ "auth_cno"       ];  // 인증거래번호
		$r_tlf_sno        = $params[ "tlf_sno"        ];  // 채번거래번호
		$r_account_type   = $params[ "account_type"   ];  // 채번계좌 타입 US AN 1 (V-일반형, F-고정형)
		/* -------------------------------------------------------------------------- */
		/* ::: 노티수신 - 에스크로 상태변경                                           
		/* -------------------------------------------------------------------------- */
		$r_escrow_yn      = $params[ "escrow_yn"      ];  // 에스크로유무
		$r_stat_cd        = $params[ "stat_cd "       ];  // 변경에스크로상태코드
		$r_stat_msg       = $params[ "stat_msg"       ];  // 변경에스크로상태메세지
		
		
		/*
		 * 고객정보
		 * 
		 * */
		$r_product_price   = $params[ "stat_msg"       ];  // 변경에스크로상태메세지
		 
		$r_set_type = $params[ "set_type"];  // 주문방식
		$r_batch_count = $params[ "batch_count"];  // 결제횟수
		$r_batch_cycle = $params[ "batch_cycle"];  // 결제주기
		$r_batch_date = $params[ "batch_date"];  // 결제시작일
		$r_batch_user = $params[ "batch_user"];  // 수령자
		$r_batch_hp = $params[ "batch_hp"];  // 핸드폰
		$r_batch_cp = $params[ "batch_cp"];  // 유선
		$r_batch_post = $params[ "batch_post"];  // 우편번호
		$r_batch_addr1 = $params[ "batch_addr1"];  // 주소1
		$r_batch_addr2 = $params[ "batch_addr2"];  // 주소2
		$r_batch_memo = $params[ "batch_memo"];  // 메모
		$r_product_code = $params[ "product_code"];  // 상품코드
		//echo $r_product_code;
		$params['EL_TYPE']='A';
		$params['EL_IP']=$_SERVER['REMOTE_ADDR'];
		//echo '응담 코드: '.$r_res_cd.'   '.'  응답메세지: '.$r_res_msg.'  pg거래번호: '.$r_cno;
		$this->order_model->pay_log($params);
		if($r_res_cd=='0000'){
			/*-order  테이블 셋팅 시작-*/
			
			if($r_set_type=='T'){
				$order_data['OM_COUNT']=1;	
				$order_data['OM_PAYKEY']='';					//정기시 KEY값
				$order_data['OM_SENDCYCLE']=1;
			}else{
				$order_data['OM_COUNT']=$r_batch_count;
				$r_amount=$params["product_price"];	
				$order_data['OM_PAYKEY']=$r_card_no;
				$order_data['OM_SENDCYCLE']=$r_batch_cycle;
			}
			//master
			$order_data['OM_ORDERCODE']=$r_order_no; //주문코드
			$order_data['OM_SHOPCODE']='P';			//주문위치
			$order_data['OM_TYPE']=$r_set_type;		//정기여부
			$order_data['OM_MEMO']=$r_batch_memo;	//메모
			$order_data['OM_CHARGE']=$r_amount;	//총금액
			$order_data['OM_NAME']=$r_batch_user;		
			$order_data['OM_HP']=$r_batch_hp;		
			$order_data['OM_CP']=$r_batch_cp;		
			$order_data['OM_POST']=$r_batch_post;		
			$order_data['OM_ADDR1']=$r_batch_addr1;		
			$order_data['OM_ADDR2']=$r_batch_addr2;		
			$order_data['OM_MEMO']=$r_batch_memo;
			$order_data['OM_SENDDATE']=$r_batch_date;
			
			$order_data['OM_PRODUCTCODE']=$r_product_code;
			$order_data['OM_POINT']=0;
			$order_data['OM_ACCOUNTCODE']='G3taumWrdo7';//테스트 하드코딩:choiwotmd5
			$order_data['OM_IP']=$_SERVER['REMOTE_ADDR'];
			$orderkey=$this->order_model->insert_order($order_data);
			//master end
				
			//$order_data['OD_OM_ORDERCODE']=$orderkey;
			//detail 
			//T 1회 : S 정기
			//20171226 기획 결제가=정상가 할인내용없음
			if($r_set_type=='T'){				
				$order_detail['OD_COUNT']=1;						
				$order_detail['OD_SENDCYCLE']=1;				
				$order_detail['OD_TAGPRICE']=$r_amount;
				$order_detail['OD_SALEPRICE']=0;		
				$order_detail['OD_DISCOUNTPRICE']=0;
				$order_detail['OD_CHARGE']=$r_amount;
				$order_detail['OD_POINT']=0;
				$order_detail['OD_PAY_YN']="Y";
			}else{
				$order_detail['OD_COUNT']=$r_batch_count;
				$order_detail['OD_SENDCYCLE']=$r_batch_cycle;	
				$order_detail['OD_TAGPRICE']=$r_amount/$r_batch_count;
				$order_detail['OD_SALEPRICE']=0;		
				$order_detail['OD_DISCOUNTPRICE']=0;
				$order_detail['OD_CHARGE']=$r_amount/$r_batch_count;
				$order_detail['OD_POINT']=0;
				$order_detail['OD_PAY_YN']="N";
			}
			$date = date("Y-m-d", strtotime( $str_date ) );
			$order_detail['OD_OM_IDX']=$orderkey;
			$order_detail['OD_OM_ORDERCODE']=$r_order_no; //주문코드
			$order_detail['OD_ACCOUNTCODE']='G3taumWrdo7';//테스트 하드코딩:choiwotmd5
			$order_detail['OD_TYPE']=$r_set_type;	
			$order_detail['OD_PRODUCTCODE']=$r_product_code;
			$order_detail['OD_IP']=$_SERVER['REMOTE_ADDR'];		
			
			//payment
			$order_payment['OP_OM_ORDERCODE']=$r_order_no; //주문코드
			$order_payment['OP_STATE']=1; //현재 상태
			$order_payment['OP_RES_CD']=$r_res_cd; //주문코드
			$order_payment['OP_RES_MSG']=$r_res_msg;
			$order_payment['OP_IP']=$_SERVER['REMOTE_ADDR']; //아이피 
			if($r_set_type=='T'){
				$order_payment['OP_STATE']=2; //현재 상태				
				$order_payment['OP_STATE_MEMO']='결제완료'; //상태값
				$order_payment['OP_CNO']=$r_cno; //거래번호
				$order_payment['OP_AMOUNT']=$r_amount ; //결제금액
				$order_payment['OP_AUTH_NO']=$r_auth_no ; //승인번호
				$order_payment['OP_TRAN_DATE']=$r_tran_date ; //승인일자
				$order_payment['OP_PAY_TYPE']=$r_pay_type ; //타입
				$count=1;
				$date = date("Y-m-d", time());
				$order_payment['OP_PAYDATE']="Y";
			}else{
				$order_payment['OP_STATE']=1; //현재 상태
				$count=$r_batch_count;
				$date = date("Y-m-d", strtotime( $r_batch_date ) );
				$day_cycle=$r_batch_cycle*7;
				$day_cycle_txt="+".$day_cycle."day";
				$order_payment['OP_STATE_MEMO']=$r_issuer_cd.'_'.$r_issuer_nm.'주문완료'.'_'.$r_acquirer_cd.'_'.$r_acquirer_nm; //상태값
				$order_payment['OP_PAYDATE']="N";
			}
			
			if($count>1){
				for($i=0;$i<$count;$i++){
					$order_detail['OD_COUNT_SET']=$i+1;
					$order_detail['OD_SENDDATE']=$date;
						
					$idx=$this->order_model->insert_detail($order_detail);
					$order_payment['OP_OD_IDX']=$idx;
					$this->order_model->insert_payment($order_payment);
					echo $date."<br/>";
					$date=date("Y-m-d", strtotime( $date.$day_cycle_txt ) );
					
				}
			}else{
					$order_detail['OD_COUNT_SET']=1;
					$order_detail['OD_SENDDATE']=$date;	
					$idx=$this->order_model->insert_detail($order_detail);
					$order_payment['OP_OD_IDX']=$idx;
					$this->order_model->insert_payment($order_payment);
			}
			
			//$this->order_model->insert_detail($order_detail);
			//echo  '<br/>code: '.$order_data['OD_OM_ORDERCODE']."<br/>batch_count:".$r_batch_count."<br/>batch_cycle : ".$r_batch_cycle; 
			//$this->order_model->insert_orderdetail($params);
			/*-order 테이블 셋팅 끝-*/
			//$this->load->helper('url');
			
			redirect("/order/torder_payend?ocode=".$r_order_no);
			
		}else{
			echo "<br/>결제 실패<br/>";
		}
		
		
	}
	public function torder_payend()
	{
		$params=$this->input->get();
		$data=$this->order_model->get_orderdata(array('OM_ORDERCODE'=>$params['ocode']));
		
		if($data['OM_TYPE']=="S"){
			$result="";
			$day_cycle=$data['OM_SENDCYCLE']*7;
			$day_cycle_txt="+".$day_cycle."day";
			$date=$data['OM_SENDDATE'];
			$data['title']=str_replace("-","",$date);
		 	for($i=1;$i<=$data['OM_COUNT'];$i++){
				$date=str_replace("-","/",$date);
				$result.="<li class='step0".$i."'>
	                <span class='step'>".$i."회차</span>
	                <span class='ico'><img src='/images/product/ico_step_bg.png' alt=''></span>
	                <span class='date'>".substr($date,5,5)."</span>
	            </li>";
				$date=date("Y/m/d", strtotime( $date.$day_cycle_txt ) );
			}
			$data['timedata']=$result;
		}
		$this ->_print($data);
	}
	

	//중복제거 랜덤함수
	public function get_code($ORDERCODE){
		$cnt=$this->common_model->_select_cnt('TB_ORDER_MASTER',array('OM_ORDERCODE'=>$ORDERCODE));
		if($cnt>0)$cnt=1;
		return $cnt;
	}
	
	
	public function cert_res(){
		$this ->_print();
	}
	
	public function batch_iframe_req(){
		$this ->_print();
	}
	public function batch_popup_req(){
		$this ->_print();
	}
	
}



