<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);

/*
| -------------------------------------------------------------------
| @ TITLE   일루마 가입
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class join extends common {

    public function __construct(){
        parent::__construct();
        $this->load->helper('cookie');
        $this->load->helper('url');
		$this->load->model("sms_model");
    }
	//가입
	public function index() {
		
		$params = $this->input->get();
		$this->load->helper('cookie');
		$sessionAry = array( 'route_url' =>$_SERVER["HTTP_REFERER"]);
		$this->session->set_userdata($sessionAry);
        $this->_print($params);
		
	}
	public function join_form() {
		$params = $this->input->post();
		
		$data['cp_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'CP','C_CODE_DEPTH'=>1,'C_REAL_YN'=>'Y','oType'=>'asc','oKey'=>'C_CODE_ORDERBY'));
		//연도
		$year=date("Y");
		$b_year=$year-10;
		$n_year=$year+10;
		$year_list="";
		for($i=$b_year;$i<$n_year;$i++){
			$selected="";
			if($year==$i)$selected="selected='selected'";
			else $selected="";
			$year_list .="<option value='".$i."' ".$selected.">".$i."년</option>";
		}
		
		$month=number_format(date("m"));
		$month_list="";
		for($i=1;$i<13;$i++){
			if($month==$i)$selected="selected='selected'";
			else $selected="";
			$month_list .="<option value='".$i."' >".$i."월</option>";
		}
		$day=number_format(date("d"));
		for($i=1;$i<=31;$i++){
			if($day==$i)$selected="selected='selected'";
			else $selected="";
			$day_list .="<option value='".$i."' >".$i."일</option>";
		}
		$params['now_year']=$year;;
		$params['now_month']=$month;
		$params['now_day']=$day;
		
		$params['year_list']=$year_list;
		$params['month_list']=$month_list;
		$params['day_list']=$day_list;
		
		
		//집전화
		for($i=0;$i<sizeof($data["cp_list"] );$i++) {
			$sel="";
			if($data["cp_list"][$i]['C_CODE_PA']==$cp[0]) $sel='selected="selected"';
            $cp_list.="<option ".$sel." value='{$data["cp_list"][$i]['C_CODE_PA']}'>{$data["cp_list"][$i]['C_CODE_PA']}</option>";
        }
		$params['cp_list']=$cp_list;
		
        $this->_print($params);
		
	}
	//아이디 체크
	public function userAccount_chk(){
		$params = $this->input->get();
		$result="N";
		$data=$this->join_model->member_chk(array('M_ACCOUNT'=>$params['M_ACCOUNT']));
		$datatw=$this->join_model->member_chk_tw(array('M_ACCOUNT'=>$params['M_ACCOUNT']));
		
		if($data['cnt']==0 && $params['M_ACCOUNT']!=""){
			$result="Y";	
		}
		echo $result;
	}
	
	//추천인 체크
	public function recommend_chk(){
		$params = $this->input->get();
		$result="N";
		$data=$this->join_model->member_chk(array('M_ACCOUNT'=>$params['M_ACCOUNT']));
		$userchk=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNT'=>$params['M_ACCOUNT'],'M_USEYN'=>'Y'));
		$pointlist_data=$this->common_model->_select_cnt('TB_POINT_LIST',array('PL_ACCOUNTCODE'=>$userchk['M_ACCOUNTCODE'],'PL_CODE'=>'PC10'));

		if($data['cnt']==0 && $params['M_ACCOUNT']!=""){
			$result="Y";	
		}else if($pointlist_data>0 ){
			$result="X";
		}
		echo $result;
	}
	//중복제거 랜덤함수
	public function get_code($M_ACCOUNTCODE){
		$cnt=$this->common_model->_select_cnt('TB_MEMBERS',array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE));
		if($cnt>0)$cnt=1;
		return $cnt;
	}

	//휴대전화 체크
	public function userhp_chk(){
		$params = $this->input->get();
		$result="N";
		$data=$this->join_model->join_chk(array('M_HP'=>str_replace('-','',$params['M_HP'])));
		if($data['cnt']==0 && $params['M_ACCOUNT']!=""){
			$result="Y";	
		}
		echo $result;
	}
	
	public function join_process(){
		$params = $this->input->post();
		$hp=$params['M_HP_1'].$params['M_HP_2'].$params['M_HP_3'];		
		
		
		$data=$this->join_model->join_chk(array('M_HP'=>str_replace('-','',$hp)));
		if($data['cnt']>0){				//중복체크
			$this->javascript_lib->top_location(array('msg'=>'이미 가입되어 있습니다.\n로그인 페이지로 이동 합니다.','nextUrl'=>'/login/index'));	
		}else{
			$code=1;
			$M_ACCOUNTCODE="";
			while($code==1){
				$this->load->helper('string');
				$M_ACCOUNTCODE=random_string('alnum', 11);
				$code=$this->get_code($M_ACCOUNTCODE);	
			}
			$params['M_HP']=$params['M_HP_1']."-".$params['M_HP_2']."-".$params['M_HP_3'];		
			$params['M_ACCOUNTCODE']=$M_ACCOUNTCODE;	
			$M_PW = hash('sha256',$params['M_PW'],true);
			$params['M_PW'] =  base64_encode($M_PW);
			
			
			if($params['M_CP_2']!="" && $params['M_CP_3']!=""){
				$params['M_CP']=$params['M_CP_1']."-".$params['M_CP_2']."-".$params['M_CP_3'];
			}else{
				$params['M_CP']="";
			}
			$params['M_BIRTHDATE']=$params['birthday_year'].str_pad($params['birthday_month'],"2","0",STR_PAD_LEFT).str_pad($params['birthday_day'],"2","0",STR_PAD_LEFT);
			$idx=$this->join_model->last_row();
			$params['M_EMAIL']=$params['M_EMAIL_1']."@".$params['M_EMAIL_2_txt'];
			
			if($idx['M_IDX']=="")$M_idx=1;
			else $M_idx=$idx['M_IDX']+1;
			$params['M_CERTKEY']="";
			$params['M_IDX']=$M_idx;
			$params['M_MCPOINT']=0;
			$params['M_IP']=$_SERVER['REMOTE_ADDR'];
			$this->join_model->member_join($params);//기본정보 입력
			//포인트 셋팅
			$point['ACCOUNTCODE']=$M_ACCOUNTCODE;
			$point['code']="PC01";
			$point['point']=500;
			$point['use']="적립";
			$point['memo']="회원가입";
			$this->add_point($point);
			//아이영역
			$ch_count=count($params['ch_name']);
			
			if($ch_count>1){
				$mccount=0;
				$ch_count=$ch_count-1;
				for($i=1;$i<=$ch_count;$i++){
					if($params['ch_name'][$i]!=""){
						$MC_BIRTHDATE=$params['ch_year'][$i].str_pad($params['ch_month'][$i],"2","0",STR_PAD_LEFT).str_pad($params['ch_day'][$i],"2","0",STR_PAD_LEFT);
						$MC_GENDER=$params['ch_gender'][$i];
						$MC_NAME=$params['ch_name'][$i];
						$this->join_model->children_join(array('MC_ACCOUNTCODE'=>$M_ACCOUNTCODE,'MC_BIRTHDATE'=>$MC_BIRTHDATE,'MC_NAME'=>$MC_NAME,'MC_GENDER'=>$MC_GENDER,'MC_IP'=>$_SERVER['REMOTE_ADDR']));//기본정보 입력
						$mccount++;
					}
				}
			}
			if($mccount*500>0){
				$this->common_model->_modify('TB_MEMBERS',array('M_MCPOINT'=>$mccount*500),array('M_ACCOUNTCODE'=>$M_ACCOUNTCODE,'M_USEYN'=>'Y'));
				//포인트 셋팅
				$point['ACCOUNTCODE']=$M_ACCOUNTCODE;
				$point['code']="PC02";
				$point['use']="적립";
				$point['point']=$mccount*500;
				$point['memo']="아기등록";
				$this->add_point($point);
			}
			
			//추천인 포인트 셋팅
			if($params['M_RECOMMEND']!=""){
				$userchk=$this->common_model->_select_row('TB_MEMBERS',array('M_ACCOUNT'=>$params['M_RECOMMEND'],'M_USEYN'=>'Y'));
				if($userchk['M_ACCOUNTCODE']!=""){
					$pointlist_data=$this->common_model->_select_cnt('TB_POINT_LIST',array('PL_ACCOUNTCODE'=>$userchk['M_ACCOUNTCODE'],'PL_CODE'=>'PC10'));
					if($pointlist_data<1){
						//추천인
						$point['ACCOUNTCODE']=$userchk['M_ACCOUNTCODE'];
						$point['code']="PC10";
						$point['use']="적립";
						$point['point']=100;
						$point['memo']="가입자 추천";
						$this->add_point($point);
						//가입자
						$point['ACCOUNTCODE']=$M_ACCOUNTCODE;
						$point['code']="PC09";
						$point['use']="적립";
						$point['point']=50;
						$point['memo']="추천자 입력";
						$this->add_point($point);	
					}
				}
			}
			
			$this->load->helper('url');
			redirect("/join/join_end");
		}	
	}

	public function join_end() {
		$this->session->unset_userdata('CertKey');
        $this->_print();
		
	}
	//가입끝
	
	//정보 찾기
	public function find_id(){
		$params = $this->input->get();
        $this->_print();
	}
	
	public function find_pw(){
		$params = $this->input->get();
        $this->_print();
	}

	//sms 번호발송
	public function sms_insert(){
		$params = $this->input->post();
		if($params['userhp']!=""){
			$params['HC_TYPE']=$params['type'];
			$params['userhp']=str_replace('-','',$params['userhp']);
			if ($params['type']=="join" || $params['type']=="mypage"){
				$data=$this->join_model->join_chk(array('M_HP'=>$params['userhp']));	
			}else{
				$data['cnt']=0;
			}
			if($data['cnt']==0){
				$cntdata=$this->sms_model->sms_count($params);
				$this->common_model->_modify('TB_HP_CHK',array('HC_CHKYN'=>'Y'),array('HC_HP'=>str_replace('-','',$params['userhp'])));
				if($cntdata['cnt']<5){
					
					$this->load->helper('string');
					$characters = '0123456789';
				    $charactersLength = strlen($characters);
				    $randomString = '';
				    for ($i = 0; $i < 6; $i++) {
				        $randomString .= $characters[rand(0, $charactersLength - 1)];
				    }
					$to='82'.mb_substr($params['userhp'],1);
					$from="01096880957";
					$text="[일루마] 본인확인 인증번호는 ".$randomString."입니다.";
					$temp_code="surem_100";
					
					$result=$this->send_kakao($to,$from,$text,$temp_code);
					$params['HC_IP']=$_SERVER['REMOTE_ADDR'];
					$params['HC_COUNT']=0;
					$params['HC_CHKYN']='N';
					$params['HC_HP']=str_replace('-','',$params['userhp']);
					$params['HC_CHK_NUM']=$randomString;
					$this->sms_model->sms_insert($params);
					echo "Y";
				}else{
					echo "X";
				}
			}else{
					echo "Z";				
			}

		}else{
			echo "N";	
		}
		
	}
	public function send_chknum(){
		$params = $this->input->post();
		$data=$this->common_model->_select_row('TB_HP_CHK',array('HC_HP'=>$params['userhp'],'HC_CHK_NUM'=>$params['chknum'],'HC_CHKYN'=>'N'));
		if($data['HC_IDX']==""){
			$result="N";
			echo"N";	
		}else{
			$this->common_model->_modify('TB_HP_CHK',array('HC_CHKYN'=>'Y'),array('HC_IDX'=>$data['HC_IDX'],'HC_HP'=>$params['userhp'],'HC_CHK_NUM'=>$params['chknum']));
			echo "Y";
		}
	}
	public function find_id_hpchk(){
		$params = $this->input->post();
		$data=$this->join_model->join_hpchk(array('M_NAME'=>$params['username'],'M_HP'=>str_replace('-','',$params['userhp']),'M_USEYN'=>'Y'));
		if($data){
			echo $data['M_ACCOUNT'];
		}else{
			echo "false";
		}
	}
	public function find_pw_hpchk(){
		
		//이후 sms변경예정
		$params = $this->input->post();
		$data=$this->join_model->join_pw_hpchk(array('M_ACCOUNT'=>$params['useraccount'],'M_HP'=>str_replace('-','',$params['userhp']),'M_USEYN'=>'Y'));
		if($data){
			$pw=$this->pw_reg();
			$this->load->helper('string');
			$characters = '0123456789';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 8; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
			$to='82'.mb_substr(str_replace('-','',$data['M_HP']),1);
			$from="01096880957";
			$text="[일루마] 비밀번호찾기로 발급 된 임시비밀번호는 ".$pw."입니다.
임시비밀번호로 로그인 후 회원정보수정에서 새로운 비밀번호로 변경 후 이용하시기 바랍니다.";
			$temp_code="surem_100";
			
			$result=$this->send_kakao($to,$from,$text,$temp_code);
			
			$M_PW = hash('sha256',$pw,true);
			$PW =  base64_encode($M_PW);
			$this->common_model->_modify('TB_MEMBERS',array('M_PW'=>$PW),array('M_ACCOUNTCODE'=>$data['M_ACCOUNTCODE'],'M_USEYN'=>'Y'));
			//DB업데이트
		}else{
			echo "false";
		}
	}
	public function out_chknum(){
		$params = $this->input->post();
		$this->common_model->_modify('TB_HP_CHK',array('HC_CHKYN'=>'Y'),array('HC_HP'=>str_replace('-','',$params['userhp'])));
	}
	public function pw_reg(){
		
		$chk=1;
		while ($chk != 0) {
			$pwcode=$this->getpw_code();
			$chk1=1;
			$chk2=1;
			$chk3=1;
			 
			if(preg_match("/[a-zA-Z]/",$pwcode)) $chk1 = 0;
			if(preg_match("/[0-9]/",$pwcode)) $chk2 = 0;
			if(preg_match("/[!#$%^&*()?+=\/]/",$pwcode)) $chk3 = 0;
			$chk=$chk1+$chk2+$chk3;
		}
		return $pwcode;
	
	}
	public function getpw_code(){
		$this->load->helper('string');
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz!#$%^&*';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < 8; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
		return $randomString;
	
	}
	//정보 찾기 끝
	
}
