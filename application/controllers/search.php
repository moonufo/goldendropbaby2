<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);

/*
| -------------------------------------------------------------------
| @ TITLE   search 페이지 컨트롤러
| @ AUTHOR  PJH
| @ SINCE   18. 02. 26.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class search extends common {

    public function __construct(){
        parent::__construct();
    }

	public function index(){
		$params = $this->input->get();
		if($params['search_type']==""){
			$params['search_type']=="text";
		}
		
		$params['where']="";		
		
		$txt=$params["search_S"];
		
		if($params['search_type']=="text"||$params['search_type']==""){
			if($txt=="") $params['where']=" and url=''";
			else $params['where']=" and (content like '%".$this->db->escape_str($txt)."%' or title like '%".$this->db->escape_str($txt)."%' or subject like '%".$this->db->escape_str($txt)."%')";
		}else{
			
			$stdata=$this->common_model->_select_row('TB_SEARCH_TAG',array('ST_IDX'=>$params['search_type'],'ST_DISPLAY_YN'=>'Y'));
			$txt=$stdata['ST_TITLE'];
			$searchtxt=$stdata['ST_TITLE'];
			$searchtxt=str_replace('#', '', $searchtxt);
			$params['where']=" and tag like '%#".$this->db->escape_str($searchtxt)."%'";
		}
		
		
     	$params['count_yn']='Y';
        $params["maxcount"] = $this->search_model->search_list($params); // pgfile_카운트  정보 갖고오기
        $page_cnt=5;

        if($params['per_page']=="" || $params['per_page']<1)$page= 1;
        else $page= $params['per_page'];
        $params['count_yn']='N';
        $data["pagination"]=$this->get_pagination('/search/index?search_F='.$params['search_F'].'&search_S='.$params['search_S'].'&search_type='.$params['search_type'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;

        $data['list'] = $this->search_model->search_list($params);
		$data['per_page']=$params['per_page'];
		$data['search_type']=$params['search_type'];
		$data['search_S']=$params["search_S"];
		$data['taglist']=$this->search_model->tag_list();
        $this->_print($data);
	}
	/*
	public function search_data(){
		$params = $this->input->get();
		
        $params['count_yn']='Y';
        $params["maxcount"] = $this->search_model->search_list($params); // pgfile_카운트  정보 갖고오기
        $page_cnt=10;

        if($params['per_page']=="" || $params['per_page']<1)$page= 1;
        else $page= $params['per_page'];
        $params['count_yn']='N';
        $data["pagination"]=$this->get_pagination('/search/search_data?search_F='.$params['search_F'].'&search_S='.$params['search_S'],count($params["maxcount"]),$page_cnt);//페이징작업
        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;

        $data['list'] = $this->search_model->search_list($params);
        $this->_print($data);
	}*/

}
