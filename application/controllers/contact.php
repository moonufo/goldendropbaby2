<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);
/*
| -------------------------------------------------------------------
| @ TITLE   일루마 Contact
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class contact extends common {

    public function __construct(){
        parent::__construct();
    }
	public function contact_index() {
		$this ->_print();
	}

    // ------------------------------------------------------------------------ 공지사항 ------------------------------------------------------------------------
    public function notice_list() {
        $params = $this->input->get();


        $params['count_yn']="Y";
        $params["maxcount"] = $this->contact_model->notice_list($params); // pgfile_카운트  정보
        $page_cnt=10;

        if($params['per_page']=="" || $params['per_page']<1){
            $page= 1;
            $params['per_page'] = 0;
        }else {
            $page= $params['per_page'];
        }

        $data["pagination"]=$this->get_pagination('/contact/notice_list?search_F='.$params['search_F'],count($params["maxcount"]),$page_cnt);//페이징작업

        $result_page=($page-1)*$page_cnt;
        $params['limit']=$result_page.",".$page_cnt;
        $params['count_yn']="N";
        $data['list'] = $this->contact_model->notice_list($params);

        $data['start_no']=count($params["maxcount"])-$result_page;
        $data['per_page']=$params['per_page'];
        $this->_print($data);

    }

    public function notice_view() {
        $params=$this->input->get();

        $data=$this->common_model->_select_row('TB_NOTICE',array('NOTICE_IDX'=>$params['notice_idx']));
        $data['next']=$this->contact_model->notice_next(array('notice_idx'=>$params['notice_idx']));
        $data['next_notice_idx']=$data['next']['next_notice_idx'];
        $data['next_notice_title']=$data['next']['next_notice_title'];
        $data['prev']=$this->contact_model->notice_prev(array('notice_idx'=>$params['notice_idx']));
        $data['prev_notice_idx']=$data['prev']['prev_notice_idx'];
        $data['prev_notice_title']=$data['prev']['prev_notice_title'];
        $data['per_page']=$params['per_page'];
        $this ->_print($data);
    }

    // ------------------------------------------------------------------------ 공지사항 끝 ------------------------------------------------------------------------

    // ------------------------------------------------------------------------ faq ------------------------------------------------------------------------
    public function faq_list() {
        $params = $this->input->get();
        $params['count_yn']='N';
        $data['code_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'FA'));

        if($params['F_TYPE']!=""){
            $params['where'].=" and F_TYPE='".$params['F_TYPE']."' ";
        }else{
            $params['where'].="";
        }
        if($params['search_S']!=""){
            $params['where'].=" and (F_ADATA like '%".$params['search_S']."%' or F_QDATA like '%".$params['search_S']."%') ";
        }

       // $data['faq_list']=$this->common_model->_select_list('TB_FAQ',$params);
        $data['faq_list'] = $this->contact_model->faq_list($params);
        $data['type_list']=$this->common_model->_select_list('COMMON_CODE',array('C_CODE_TYPE'=>'FA','C_CODE_DEPTH'=>1,'C_REAL_YN'=>'Y','oType'=>'asc','oKey'=>'C_CODE_ORDERBY'));
        foreach ($data['faq_list'] as $key => $value) {
            $data['faq_list'][$key]['faq_type_txt']=$this->get_code_txt($data['faq_list'][$key]['F_TYPE']);
        }

        $data['search_S']=$params['search_S'];
        $data['F_TYPE']=$params['F_TYPE'];
        $this->_print($data);
    }

    //
    public function view_chk(){

        $params=$this->input->get();
        $im_idx = $params['id'];            // 시퀀스
        $viewcount = $this->contact_model->faq_view_cnt($params); // faq 클릭 수
        $cnt_chk = $viewcount['cnt'];

        $this->common_model->_modify('TB_FAQ',array('F_CNT'=>$cnt_chk + 1),array('F_IDX'=>$im_idx));

    }

    // ------------------------------------------------------------------------ faq 끝 ------------------------------------------------------------------------

    // ------------------------------------------------------------------------ qna  ------------------------------------------------------------------------
    public function qna_index() {
        $this->member_lib->loginCheck();
        $data['name'] = $this->session->userdata('USER_NAME');
        $this ->_print($data);
    }

    public function qna_process() {
        $params = $this->input->post();
        $this->load->helper('cookie');
        $this->load->model("login_model");
        $sub_dr_name=date("Ym");
        $tmp_upload_doc="/home/dartz/public_html/";
        $path = $tmp_upload_doc.'/upload/qna';
        $db_path= '/upload/qna'."/".$sub_dr_name."/";

        $org_path = $path."/".$sub_dr_name."/";  //년월201701 별로 디렉토리 만들기
        if ( is_dir($path) && is_writable($path) ) {
            $tmp_umask = umask(0);
            mkdir($org_path,0707);
            umask($tmp_umask);
        }

        $params['qna_ip']=$_SERVER['REMOTE_ADDR'];
//        if($params['qna_email_sel']==""){
//            $email2=$params['qna_email_2'];
//        }else{
//            $email2=$params['qna_email_sel'];
//        }
//        $email=$params['qna_email_1']."@".$email2;
//
//        $params['qna_email']=$email;
		$userdata=$this->login_model->get_login_info(array('username'=>$this->session->userdata('USER_ACCOUNT')));
 		$params['QNA_EMAIL'] = $userdata['M_EMAIL'];
		$params['qna_name'] = $this->session->userdata('USER_NAME');
		$params['QNA_ACCOUNTCODE']=$this->session->userdata('USER_ACCOUNTCODE');
		$params['QNA_ID']=$this->session->userdata('USER_ACCOUNT');
        $qna_idx=$this->contact_model->insert_contact($params);//동영상,파일제외 데이터 저장
//        $data=$this->contact_model->insert_contact($params);

        if($_FILES['QNA_QUESTION_FILENAME']['name']!=""){
            $fileParams['dirName'] =$org_path;
            $fileParams['fieldName'] = 'QNA_QUESTION_FILENAME';
            $fileParams['filemulti_flag'] = false;
            $fileParams['allowed_types'] = "";
            $fileParams['files']=$_FILES;
            $fileParams['reSize']=false;
            $uploadData = $this->common_class->upload_files_new($fileParams);

            foreach($uploadData as $key=>$value){
                $this->common_model->_modify('TB_QNA',array('QNA_QUESTION_FILENAME'=>$value['file_name'],'QNA_QUESTION_PATH'=>$db_path),array('qna_idx'=>$qna_idx));
            }
        }

        $this->javascript_lib->top_location(array('msg'=>'질문이 접수되었습니다.','nextUrl'=>'/contact/qna_index'));

    }
    // ------------------------------------------------------------------------ qna 끝 ------------------------------------------------------------------------

}