<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);


class login extends common {

    public function __construct(){
        parent::__construct();
        $this->load->helper('cookie');
        $this->load->helper('url');
    }

	public function kakao_test(){
		$to="821071857532";
		$from="01096880957";
		$text="최재승님 회원가입을 축하드립니다";
		$temp_code="surem_100";
		$result=$this->send_kakao($to,$from,$text,$temp_code);
		echo $result;
	}

	public function index(){
		$params["sv_sid"]=$this->input->cookie('save_id');
		$this->load->helper('cookie');
		$sessionAry = array( 'route_url' =>$_SERVER["HTTP_REFERER"]);
		$this->session->set_userdata($sessionAry);
		$this->session->unset_userdata('CertKey');
        $this->_print($params);
	}


	public function login_check(){
		
        $params = $this->input->post();
		$vv=$params['password'];

		$params['password']  = hash('sha256',$params['password'],true);
		$params['password'] =  base64_encode($params['password']);
		
		$data=$this->login_model->get_login_info($params);
		$login_flag=true;
		if($data){
			if ($data['M_PW']!=$params['password']){
				$login_msg="잘못된 패스워드 입니다.\n확인 후 다시 입력해 주세요.";
				$login_flag=false;
			}
		}else{
			$login_msg="아이디를 찾을 수 없습니다.";
			$login_flag=false;	
		}
		if($login_flag==true){
			$this->load->library('session');
			$newdata = array(
	               'USER_ACCOUNT'  =>$data['M_ACCOUNT'],
	               'USER_NAME'     => $data['M_NAME'],
	               'USER_ACCOUNTCODE' => $data['M_ACCOUNTCODE'],
	               'isMember' => "YES"
	           );
	
			$this->session->set_userdata($newdata);
		
	        if($params['loginSave']=='Y'){
	            $tmp_saveid_cookie = array(
	                'name'   => 'id',
	                'value'  => $data['M_ACCOUNT'],
	                'expire' => 365*24*60*60,
	                'path'   => '/',
	                'prefix' => 'save_',
	            );
	        }else{
	            $tmp_saveid_cookie = array(
	                'name'   => 'id',
	                'value'  => '',
	                'expire' => 365*24*60*60,
	                'path'   => '/',
	                'prefix' => 'save_',
	            );
	        }
        	set_cookie($tmp_saveid_cookie);
		}
		
		$str_agent=$this->input->server("HTTP_USER_AGENT");
		$str_ip=$this->input->server("REMOTE_ADDR");
		if($login_flag==true) {
			$login_YN="Y";
			$this->login_model->login_chk(array('M_ACCOUNTCODE'=>$data['M_ACCOUNTCODE']));
			$loginchk=$this->login_model->login_count_chk(array('M_ACCOUNTCODE'=>$data['M_ACCOUNTCODE']));
			if(count($loginchk)==0){
				$point['ACCOUNTCODE']=$data['M_ACCOUNTCODE'];
				$point['code']="PC08";
				$point['use']="적립";
				$point['point']=10;
				$point['memo']="로그인";
				$this->add_point($point);
			}
		}else{
			$login_YN="N";
		}
		$this->login_model->login_history(array('LH_M_ACCOUNT'=>$params['username'],'LH_M_ACCOUNTCODE'=>$data['M_ACCOUNTCODE'],'LH_AGENT'=>$str_agent,'LH_IP'=>$str_ip,'LH_LOGINYN'=>$login_YN));
		
        echo json_encode(array('Stat'=>$login_flag,'Msg'=>$login_msg,'Url'=>$this->session->userdata('route_url')));

	}

    public function logout() {

        if($this->session->userdata('USER_ACCOUNT') !=""){
			$this->session->sess_destroy();
		}

        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.alert('로그아웃 되었습니다.');
        window.location.href='/main/index';
        </SCRIPT>");
    }
	//탈퇴시 로그인 해제
	public function withdrawal_end(){
	$params = $this->input->get();
		//임의설정
	$data['M_ACCOUNT']=$params['CODE'];
       if($this->session->userdata('USER_ACCOUNT') !=""){
			$this->session->sess_destroy();
	   }
		$this->_print($data);
	}
}
