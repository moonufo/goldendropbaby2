<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);


class main extends common {

    public function __construct(){
        parent::__construct();
        //$this->member_lib->loginCheck();
    }
	public function index() {
		$this->load->library('session');
		//echo "USER_ACCOUNT : ".$session_id = $this->session->userdata('USER_ACCOUNT')."<br/>";
		//echo "USER_NAME : ".$session_id = $this->session->userdata('USER_NAME')."<br/>";
	//	echo "USER_ACCOUNTCODE : ".$session_id = $this->session->userdata('USER_ACCOUNTCODE')."<br/>";
      //  echo "isMember : ".$session_id = $this->session->userdata('isMember')."<br/>";

        $data["instar"] = $this->main_model->select_instar_list(); // 인스타그램

        $data["visual"] = $this->main_model->select_visual_list(); // 메인 상단 배너

        $data["popup"] = $this->main_model->select_popup_list(); // 메인 팝업

        for($i=0; $i < sizeof($data["popup"]); $i++ ) {
            $data["popup"][$i]['mp_h'] = $data["popup"][$i]['MP_HEIGHT'] + 40;
        }

		$this ->_print($data);
	}

	public function index_step2() {
		$this->load->library('session');
		//echo "USER_ACCOUNT : ".$session_id = $this->session->userdata('USER_ACCOUNT')."<br/>";
		//echo "USER_NAME : ".$session_id = $this->session->userdata('USER_NAME')."<br/>";
	//	echo "USER_ACCOUNTCODE : ".$session_id = $this->session->userdata('USER_ACCOUNTCODE')."<br/>";
      //  echo "isMember : ".$session_id = $this->session->userdata('isMember')."<br/>";


        $data["instar"] = $this->main_model->select_instar_list(); // 인스타그램

		$this ->_print($data);
	}

    public function popup() {
        $params=$this->input->get();
        $params['where'].=" and (MP_STRDATE <= now() and MP_ENDDATE >= now()) and MP_DISPLAY_YN='Y' ";
        $data=$this->main_model->rowdata_mp(array('mp_idx'=>$params['mp_idx'],'where'=>$params['where']));

        $popup_txt['content'] = "<form name=\"pop_form{$data["MP_IDX"]}\" id=\"pop_form{$data["MP_IDX"]}\">";
        $popup_txt['content'] .= "   <div class=\"modal_wrap\">";
        $popup_txt['content'] .= "          <div class=\"modal_txt\">";
        $popup_txt['content'] .= "               <p class=\"txt\">";
        $popup_txt['content'] .= "              {$data["MP_CONTENT"]} ";
        $popup_txt['content'] .= "              </p>";
        $popup_txt['content'] .= "                  <a href=\"#\" onclick=\"javascript:closeWin({$data["MP_IDX"]});\"><img src=\"/images/main/btn_modal_close.png\" alt=\"닫기\"></a>";
        $popup_txt['content'] .= "             <div class=\"chk_wrap\">";
        $popup_txt['content'] .= "                  <label>";
        $popup_txt['content'] .= "                      <input type=\"checkbox\" name=\"notice_{$data["MP_IDX"]}\" id=\"notice_{$data["MP_IDX"]}\" value=\"1\" id=\"c\" >오늘 그만보기 ";
        $popup_txt['content'] .= "                  </label>";
        $popup_txt['content'] .= "              </div>";
        $popup_txt['content'] .= "          </div>";
        $popup_txt['content'] .= "     </div>";
        $popup_txt['content'] .= "   </form>";

        echo json_encode(array('real_txt'=>$popup_txt['content']));

    }

}