<?php
if ( ! defined("BASEPATH")) exit("No direct script access allowed");
require_once(APPPATH ."controllers/common".EXT);

/*
| -------------------------------------------------------------------
| @ TITLE   STORY 페이지 컨트롤러
| @ AUTHOR  PJH
| @ SINCE   17. 12. 27.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class shop extends common {

    public function __construct(){
        parent::__construct();
    }

	public function step1(){
        $this->_print();
	}

    public function step2(){
        $this->_print();
    }

    public function step3(){
        $this->_print();
    }

    public function purchasing(){
        $this->_print();
    }

}
