<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| @ PURPOSE
| @ AUTHOR  JoonCh
| @ SINCE   13. 9. 3.
| -------------------------------------------------------------------
| This file contains an array of mime types.  It is used by the
| Upload class to help identify allowed file types.
|
*/

class Common_class {

    function cut_str_han( $str, $n=5, $end_char = '' )
    {
        $CI =& get_instance();
        $charset = $CI->config->item('charset');

        if ( mb_strlen( $str , $charset) < $n ) {
            return $str ;
        }

        $str = preg_replace( "/\s+/iu", ' ', str_replace( array( "\r\n", "\r", "\n" ), ' ', $str ) );

        if ( mb_strlen( $str , $charset) <= $n ) {
            return $str;
        }
        return mb_substr(trim($str), 0, $n ,$charset) . $end_char ;
    }




///

function html_encode_($contents){
    return htmlspecialchars($contents,ENT_QUOTES);
}

public function file_resize($path, $file)
{
    $products = array('Tires', 'Oil', 'Spark Plugs');
    echo "2-2";
    $CI->load->library('image_lib');
    echo "2-3";
    echo count($products)."||".$products;
    exit;
    for($i=0; $i<count($sizes); $i++) 
    {
       echo $i; 
       $config['image_library']    = 'gd2';
       $config['source_image']     = $path;
       $config['create_thumb']     = true;
       $config['maintain_ratio']   = true;
       $config['width']            = $sizes[$i];
       $config['height']           = $sizes[$i];   
       $config['new_image']        = './uploads/' . $sizes[$i] . $file;

       $CI->image_lib->clear();
       $CI->image_lib->initialize($config);
       $CI->image_lib->resize();
    }
}



///


    /*
     * 멀티 업로드 (by KJC)
     * $params['fieldName'] : 필드 이름
     * $params['dirName'] : 업로드 할 경로  (ex: './uploads/showcase/' )
     * $params['files'] : $_FILES
     */
    function upload_files($fileParams) {
    	   

        $CI =& get_instance();
        $fieldName = $fileParams['123'];
        $cntFiles = count($fileParams['files'][$fieldName]['name']);

        
        if($cntFiles == 1){
            $data = $this->ci_upload($fileParams);
            // 리사이징
            if($fileParams['reSize'] && $data){
                $reConfig['image_library'] = 'gd2';
                $reConfig['source_image']	= $fileParams['dirName'].$data['upload_data']['file_name'];
                $reConfig['create_thumb'] = FALSE;
                $reConfig['maintain_ratio'] = TRUE;
                $reConfig['width']	 = $fileParams['width'];
                $reConfig['height']	= $fileParams['height'];
                $CI->load->library('image_lib', $reConfig);
                $CI->image_lib->resize();
            }
        // 멀티 업로드인 경우에만 해당됩니다.
        }else if($cntFiles > 1) {
            $idx =0;
            for($i=1; $i<=$cntFiles; $i++){
                $_FILES[$fieldName]['name']= $fileParams['files'][$fieldName]['name'][$i];
                $_FILES[$fieldName]['type']= $fileParams['files'][$fieldName]['type'][$i];
                $_FILES[$fieldName]['tmp_name']= $fileParams['files'][$fieldName]['tmp_name'][$i];
                $_FILES[$fieldName]['error']= $fileParams['files'][$fieldName]['error'][$i];
                $_FILES[$fieldName]['size']= $fileParams['files'][$fieldName]['size'][$i];
                $fileParams['idx'] = $idx;
                $data[$idx++] = $this->ci_upload($fileParams);
                if($_FILES[$fieldName]['name']) $fileParams['imgLibraryLoaded'] = "true";

            }
        }
		
	
        return $data;
    }


    function upload_files_new($fileParams) {

		$CI =& get_instance();
        $fieldName = $fileParams['fieldName'];
		$filemulti_flag = $fileParams['filemulti_flag'];

		if(!$filemulti_flag){
			//echo $fileParams['files'][$fieldName]['name'];
            $data = $this->ci_upload($fileParams);
			if($data=="error"){
				return "error"; // 에러 일경우 업로드 중단!!
				exit;
			}
        // 멀티 업로드인 경우에만 해당됩니다.
        }else{
            $idx =0;
            for($i=0; $i<=count($fileParams['files'][$fieldName]['name']); $i++){

				//echo $idx.":".$fileParams['files'][$fieldName]['name'][$i]."<br/>";

                $_FILES[$fieldName]['name']= $fileParams['files'][$fieldName]['name'][$i];
                $_FILES[$fieldName]['type']= $fileParams['files'][$fieldName]['type'][$i];
                $_FILES[$fieldName]['tmp_name']= $fileParams['files'][$fieldName]['tmp_name'][$i];
                $_FILES[$fieldName]['error']= $fileParams['files'][$fieldName]['error'][$i];
                $_FILES[$fieldName]['size']= $fileParams['files'][$fieldName]['size'][$i];
                $fileParams['idx'] = $idx;
                $temp_upload_result = $this->ci_upload($fileParams);
				if($temp_upload_result=="error"){
					return "error"; // 에러 일경우 업로드 중단!!
					exit;
				}
				$data[$idx++]= $temp_upload_result;
                if($_FILES[$fieldName]['name']) $fileParams['imgLibraryLoaded'] = "true";
            }
        }
        return $data;
    }


    function ci_upload($fileParams) {
        $fieldName = $fileParams['fieldName'];
        if(!$_FILES[$fieldName]['name'])    return;
        // 업로드
        $CI =& get_instance();
        $config['upload_path'] = $fileParams['dirName'];

		if ($fileParams['allowed_types']){
	        $config['allowed_types'] = $fileParams['allowed_types'];
		}else{
	        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		}

		if ($fileParams['max_size']){
	        $config['max_size'] = $fileParams['max_size']; //KByte
		}else{
	        $config['max_size'] = '2048'; //KByte
		}

		//echo $config['max_size'];
        $config['encrypt_name'] = false;
		//echo $fileParams['fieldName']."------".$config['allowed_types']."------".$fileParams['fieldName']["name"]."<br/>";

		$CI->load->library('upload', $config);
		$CI->upload->initialize($config);

        if ( ! $CI->upload->do_upload($fileParams['fieldName']))  
			$error = array('error' => $CI->upload->display_errors());
			if($error){
				$error["error"]=str_replace("<p>","",$error["error"]);
				$error["error"]=str_replace("</p>","",$error["error"]);
				echo "<script type='text/javascript'>";
				echo "alert('".$error["error"]."\\n파일명 : ".$_FILES[$fieldName]['name'].")');";
				echo "</script>";

				return "error";
				exit;
			}
		else{
			$file_data = array('upload_data' => $CI->upload->data());
			if($fileParams['reSize']){
				$fileParams['file_data'] = $file_data;

				$fileParams['new_image'] = $fileParams['dirName'].$fileParams['file_data']['upload_data']['raw_name'].'_s'.$fileParams['file_data']['upload_data']['file_ext'];
				//echo $fileParams['new_image'];

				$file_data['upload_data']['s_path']=$fileParams['file_data']['upload_data']['raw_name'].'_s'.$fileParams['file_data']['upload_data']['file_ext'];
				$fileParams['width'] = 70;
				$fileParams['height'] = 47;
				$this->ci_resize($fileParams);


				$fileParams['new_image'] = $fileParams['dirName'].$fileParams['file_data']['upload_data']['raw_name'].'_m'.$fileParams['file_data']['upload_data']['file_ext'];
				$file_data['upload_data']['m_path']=$fileParams['file_data']['upload_data']['raw_name'].'_m'.$fileParams['file_data']['upload_data']['file_ext'];
				$fileParams['width']=360;
				$fileParams['height']=239;
				$this->ci_resize($fileParams);
			}
			return $file_data;
		}
    }

    function ci_resize($fileParams) {
        $CIre =& get_instance();
        $reConfig['image_library'] = 'gd2';
        $reConfig['source_image']	= $fileParams['dirName'].$fileParams['file_data']['upload_data']['file_name'];
		$reConfig['new_image'] = $fileParams['new_image'];
        $reConfig['create_thumb'] = FALSE;
        $reConfig['maintain_ratio'] = FALSE;
        $reConfig['width']	 = $fileParams['width'];
        $reConfig['height']	= $fileParams['height'];
        if($fileParams['imgLibraryLoaded'] == "true")
           $CIre->image_lib->initialize($reConfig);
        else
            $CIre->load->library('image_lib', $reConfig);
		$CIre->image_lib->initialize($reConfig);
        $CIre->image_lib->resize();
        $error = $CIre->image_lib->display_errors();
        $CIre->image_lib->clear();
        if($error){
            echo "<pre>";print_r($error);echo "</pre>";exit;
        }

    }

}