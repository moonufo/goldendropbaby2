<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| @ TITLE   회원 관련 라이브러리
| @ AUTHOR  JoonCh
| @ SINCE   14. 5. 2.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class member_lib {

    /**
     * login Check
     * @param	none
     */
    function loginCheck($params=array())
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->library('javascript_lib');
        if(!$CI->session->userdata('USER_ACCOUNTCODE')){
            $nextUrl = ($params['returnUrl'])? "/login/index/returnUrl/".$params['returnUrl'] : "/login/index";
            $CI->javascript_lib->location(array("msg"=>"로그인하시겠습니까?", "nextUrl"=>$nextUrl));
        }
    }
	
	function urlCheck($params=array())
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->library('javascript_lib');
        if($params['data']==""){
            $CI->javascript_lib->location(array("msg"=>"잘못된 경로입니다.", "nextUrl"=>"/"));
        }
    }



    /**
     * admin Check
     * @param	none
     */
    function adminCheck()
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->library('javascript_lib');
        if(!$CI->session->userdata('mbID')){
            $CI->javascript_lib->confirm(array("msg"=>"로그인하시겠습니까?", "nextUrl"=>"/adm/member/login"));
            return false;
        }else {
            $CI->load->model('common_model');
            $data = $CI->common_model->_select_row('wn_member', array('mbID'=>$CI->session->userdata('mbID')));
            if($data['mbIsAdmin'] == "NO")  $CI->javascript_lib->location(array("msg"=>"관리자 전용 페이지 입니다.","nextUrl"=>"/main/index"));
            return false;
        }
    }

    /**
     * admin Check
     * @param	none
     */
    function chatAdminCheck()
    {
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->library('javascript_lib');
        if(!$CI->session->userdata('tiID')){
            $CI->javascript_lib->confirm(array("msg"=>"로그인하시겠습니까?", "nextUrl"=>"/ci/adm/chat/login"));
        }else {
            $CI->load->model('common_model');
            $data = $CI->common_model->_select_row('wn_teaminfo', array('tiID'=>$CI->session->userdata('tiID')));
            if($data['tiIsAdmin'] == "NO")  $CI->javascript_lib->back(array("msg"=>"관리자 전용 페이지 입니다."));
        }
    }
}
