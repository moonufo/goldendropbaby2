<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| @ TITLE   자바스크립트 관련 라이브러리
| @ AUTHOR  JoonCh
| @ SINCE   14. 5. 2.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class javascript_lib {

    /**
     * Javascript confirm
     * @param	$params array
     * @params	string : "msg", string : "nextUrl"
     */
    function confirm_login($params=array())
    {
        echo "<script type='text/javascript'>
                if(confirm('".$params['msg']."')){
                    location.href='/member/login/returnUrl/".$params['nextUrl']."';
                } else {
                    location.href='/member/login';
                }
             </script>";
    }

    /**
     * Javascript confirm
     * @param	$params array
     * @params	string : "msg", string : "nextUrl"
     */
    function confirm($params=array())
    {
        echo "<script type='text/javascript'>
                if(confirm('".$params['msg']."')){
                    location.href='".$params['nextUrl']."';
                } else {
                    location.href='".$params['prevUrl']."';
                }
             </script>";
    }


    /**
     * Javascript location.href
     * @param	$params array
     * @params	string : "msg", string : "nextUrl"
     */
    function location($params=array())
    {
        echo "<script type='text/javascript'>
                alert('".$params['msg']."');
                location.href='".$params['nextUrl']."';
             </script>";
    }


    /**
     * Javascript location.href
     * @param	$params array
     * @params	string : "msg", string : "nextUrl"
     */
    function none_msg_location($params=array())
    {
        echo "<script type='text/javascript'>
                location.href='".$params['nextUrl']."';
             </script>";
    }

    /**
     * Javascript history.back
     * @param	$params array
     * @params	string : "msg"
     */
    function back($params=array())
    {
        echo "<script type='text/javascript'>
                alert('".$params['msg']."');
                history.back();
             </script>";
    }

    function top_location($params=array())
    {
        echo "<script type='text/javascript'>
                alert('".$params['msg']."');
                top.location.href='".$params['nextUrl']."';
             </script>";
    }

    function alert_msg($params=array())
    {
        echo "<script type='text/javascript'>
                alert('".$params['msg']."');
             </script>";
    }
}
