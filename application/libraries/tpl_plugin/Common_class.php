<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| @ PURPOSE
| @ AUTHOR  JoonCh
| @ SINCE   13. 9. 3.
| -------------------------------------------------------------------
| This file contains an array of mime types.  It is used by the
| Upload class to help identify allowed file types.
|
*/

class Common_class {

    function cut_str_han( $str, $n=5, $end_char = '' )
    {
        $CI =& get_instance();
        $charset = $CI->config->item('charset');

        if ( mb_strlen( $str , $charset) < $n ) {
            return $str ;
        }

        $str = preg_replace( "/\s+/iu", ' ', str_replace( array( "\r\n", "\r", "\n" ), ' ', $str ) );

        if ( mb_strlen( $str , $charset) <= $n ) {
            return $str;
        }
        return mb_substr(trim($str), 0, $n ,$charset) . $end_char ;
    }




///
function upload_files_test($fileParams) {
    $CI =& get_instance();
    $fieldName = $fileParams['123'];
    $cntFiles = count($fileParams['files'][$fieldName]['name']);

  

       
        if($fileParams['reSize']=="YES"){
            $data = $this->ci_upload($fileParams);
            //$fileParams['file_data']=$data;
            //$fileParams['width_t']='70';
            //$fileParams['width_t']='150';
            //$fileParams['name_t']='_S';
            $this->upload_files_reSize_test($fileParams);
                 /*
            unset($fileParams['width_t']);
            unset($fileParams['width_t']);
            unset($fileParams['name_t']);
            $fileParams['width_t']='150';
            $fileParams['width_t']='300';
            $fileParams['name_t']='_M';
            upload_files_reSize_test($fileParams);
       
            $reConfig['image_library'] = 'gd2';
            $reConfig['source_image']   = $fileParams['dirName'].$data['upload_data']['file_name'];
            $reConfig['create_thumb'] = true;
            $reConfig['maintain_ratio'] = FALSE;
            $reConfig['width']   = $fileParams['width'];
            $reConfig['height'] = $fileParams['height'];
            //$reConfig['thumb_marker']='';
            $reConfig['new_image'] = $data['upload_data']['raw_name'].'_m'.$data['upload_data']['file_ext'];
            $CI->load->library('image_lib', $reConfig);
            $CI->image_lib->resize();
              $CI->image_lib->clear();
         */
              
   
            
        }else{
            echo "2424";
            $data = $this->ci_upload($fileParams);
        }
      
        $_params['attachments_path'] = '/uploads/data/team_info_new/intro/'.$data['upload_data']['file_name'];
        $_params['attachments_name'] = $data['upload_data']['file_name'];
        $_params['attachments_type'] = $data['upload_data']['file_ext'];
        $_params['attachments_size'] = $data['upload_data']['file_size'];
        $_params['attachments_org_name'] = $data['upload_data']['orig_name'];

    

    return $_params;
}
function upload_files_reSize_test($fileParams) {
   // echo "1515{}";
    //$reConfig['image_library'] = 'gd2';
    //$reConfig['source_image']   = $fileParams['dirName'].$fileParams['file_data']['upload_data']['file_name'];
    //echo $reConfig['source_image']."{}";
    //$reConfig['create_thumb'] = FALSE;
    //$reConfig['maintain_ratio'] = FALSE;
    //$reConfig['width']   = $fileParams['width_t'];
    //$reConfig['height'] = $fileParams['height_t'];
    //    echo $reConfig['width']."{}";
    //$reConfig['thumb_marker']='';
    //$reConfig['new_image'] = $fileParams['file_data']['upload_data']['raw_name'].$fileParams['name_t'].$fileParams['file_data']['upload_data']['file_ext'];
    //    echo $reConfig['source_image']."{}";
    //$CI->load->library('image_lib', $reConfig);
    //$CI->image_lib->resize();
    //$CI->image_lib->clear();
    echo "1";
    $this->file_resize($fileParams['dirName'], $fileParams['file_data']['upload_data']['file_name']);
    
    
}


public function file_resize($path, $file)
{
    $products = array('Tires', 'Oil', 'Spark Plugs');
    echo "2-2";
    $CI->load->library('image_lib');
    echo "2-3";
    echo count($products)."||".$products;
    exit;
    for($i=0; $i<count($sizes); $i++) 
    {
       echo $i; 
       $config['imag+e_library']    = 'gd2';
       $config['source_image']     = $path;
       $config['create_thumb']     = true;
       $config['maintain_ratio']   = true;
       $config['width']            = $sizes[$i];
       $config['height']           = $sizes[$i];   
       $config['new_image']        = './uploads/' . $sizes[$i] . $file;

       $CI->image_lib->clear();
       $CI->image_lib->initialize($config);
       $CI->image_lib->resize();
    }
}



///


    /*
     * 멀티 업로드 (by KJC)
     * $params['fieldName'] : 필드 이름
     * $params['dirName'] : 업로드 할 경로  (ex: './uploads/showcase/' )
     * $params['files'] : $_FILES
     */
    function upload_files($fileParams) {
    	   

        $CI =& get_instance();
        $fieldName = $fileParams['123'];
        $cntFiles = count($fileParams['files'][$fieldName]['name']);

        // 1개 파일 업로드
        
        if($cntFiles == 1){
     
				
            $data = $this->ci_upload($fileParams);
			
            // 리사이징
            
            if($fileParams['reSize'] && $data){
                $reConfig['image_library'] = 'gd2';
                $reConfig['source_image']	= $fileParams['dirName'].$data['upload_data']['file_name'];
                $reConfig['create_thumb'] = FALSE;
                $reConfig['maintain_ratio'] = TRUE;
                $reConfig['width']	 = $fileParams['width'];
                $reConfig['height']	= $fileParams['height'];
                $CI->load->library('image_lib', $reConfig);
                $CI->image_lib->resize();
            }

        // 멀티 업로드인 경우에만 해당됩니다.
        }else if($cntFiles > 1) {
    
            $idx =0;
            for($i=1; $i<=$cntFiles; $i++){
                $_FILES[$fieldName]['name']= $fileParams['files'][$fieldName]['name'][$i];
                $_FILES[$fieldName]['type']= $fileParams['files'][$fieldName]['type'][$i];
                $_FILES[$fieldName]['tmp_name']= $fileParams['files'][$fieldName]['tmp_name'][$i];
                $_FILES[$fieldName]['error']= $fileParams['files'][$fieldName]['error'][$i];
                $_FILES[$fieldName]['size']= $fileParams['files'][$fieldName]['size'][$i];
                $fileParams['idx'] = $idx;
                $data[$idx++] = $this->ci_upload($fileParams);
                if($_FILES[$fieldName]['name']) $fileParams['imgLibraryLoaded'] = "true";

            }
        }
		
	
        return $data;
    }

    function ci_upload($fileParams) {
        $fieldName = $fileParams['123'];
        if(!$_FILES[$fieldName]['name'])    return;
        // 업로드
        $CI =& get_instance();
        $config['upload_path'] = $fileParams['dirName'];
        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
        $config['max_size'] = '20000';
        $config['encrypt_name'] = true;
        $CI->load->library('upload', $config);
        if ( ! $CI->upload->do_upload($fileParams['123']))  $error = array('error' => $CI->upload->display_errors());
        else                                      $file_data = array('upload_data' => $CI->upload->data());
        if($error){
            echo "<pre>";print_r($error);echo "</pre>";exit;
        }
        // 리사이징
       // if($fileParams['reSize']){
        //    $fileParams['file_data'] = $file_data;
        //    $this->ci_resize($fileParams);
       // }

        return $file_data;
    }

    function ci_resize($fileParams) {
        $CIre =& get_instance();
        $idx = $fileParams['idx'];
        $reConfig['image_library'] = 'gd2';
        $reConfig['source_image']	= $fileParams['dirName'].$fileParams['file_data']['upload_data']['file_name'];
        $reConfig['create_thumb'] = FALSE;
        $reConfig['maintain_ratio'] = TRUE;
        $reConfig['width']	 = $fileParams['width'][$idx];
        $reConfig['height']	= $fileParams['height'][$idx];
        if($fileParams['imgLibraryLoaded'] == "true")
           $CIre->image_lib->initialize($reConfig);
        else
            $CIre->load->library('image_lib', $reConfig);
        $CIre->image_lib->resize();
        $error = $CIre->image_lib->display_errors();
        $CIre->image_lib->clear();
        if($error){
            echo "<pre>";print_r($error);echo "</pre>";exit;
        }

    }

}