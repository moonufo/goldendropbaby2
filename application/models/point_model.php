<?php
/*
| -------------------------------------------------------------------
| @ TITLE   일루마 point
| @ AUTHOR  cjs
| @ SINCE   18. 02. 26
| -------------------------------------------------------------------
*/

class point_model extends CI_Model {
   
 	public function pl_insert($params=array()) {
      $query = "insert into TB_POINT_LIST(
					PL_ACCOUNTCODE
					,PL_TYPE
					,PL_CODE
					,PL_USE
					,PL_POINT
					,PL_TITLE
					,PL_MEMO
					,PL_CREDATE
					,PL_REAL_YN
					,PL_IP
					) values (
					'".$this->db->escape_str($params['PL_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['PL_TYPE'])."',
					'".$this->db->escape_str($params['PL_CODE'])."',
					'".$this->db->escape_str($params['PL_USE'])."',
					".$this->db->escape_str($params['PL_POINT']).",
					'".$this->db->escape_str($params['PL_TITLE'])."',
					'".$this->db->escape_str($params['PL_MEMO'])."',
					now(),
					'Y',
					'".$this->db->escape_str($params['PL_IP'])."'
					)";
	   $result = $this->db->query($query);
	   return $this->db->insert_id();
    }
	
 	public function pt_insert($params=array()) {
		$query = "insert into TB_POINT_TOTAL (
					PT_PL_IDX
					,PT_ACCOUNTCODE
					,PT_POINT
					,PT_CREDATE
					,PT_USE_YN
					,PT_REAL_YN
					) values (
					'".$this->db->escape_str($params['PT_PL_IDX'])."',
					'".$this->db->escape_str($params['PT_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['PT_POINT'])."',
					now(),
					'N',
					'Y'
					)";
	   $result = $this->db->query($query);
	  
    }
    
	public function pl_totalpoint_update($params=array()) {
        $query = (" update TB_POINT_LIST set PL_TOTALPOINT =(select sum(PT_POINT) from TB_POINT_TOTAL 
        where PT_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."' and PT_USE_YN='N' and PT_REAL_YN='Y' ) where pl_idx=".$this->db->escape_str($params['PL_IDX'])." ");
	    $result = $this->db->query($query);
	    
    }
    public function member_totalpoint_update($params=array()) {
        $query = (" update TB_MEMBERS set M_POINT =(select sum(PT_POINT) from TB_POINT_TOTAL 
        where PT_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."' and PT_USE_YN='N' and PT_REAL_YN='Y' ) where M_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."' ");
	    $result = $this->db->query($query);
	    
    }
	
	public function pc_insert($params=array()) {
		$query = "insert into TB_POINT_CHK (
					PC_PL_IDX
					,PC_PT_IDX
					,PC_POINT
					,PC_CREDATE
					,PC_REAL_YN
					) values (
					'".$this->db->escape_str($params['PC_PL_IDX'])."',
					'".$this->db->escape_str($params['PC_PT_IDX'])."',
					'".$this->db->escape_str($params['PT_POINT'])."',
					now(),
					'Y'
					)";
	   $result = $this->db->query($query);
	  
    }
    
	public function pt_updatepoint($params=array()) {
        $query = " update TB_POINT_TOTAL set 
        PT_POINT = ".$this->db->escape_str($params['PT_POINT'])."";
		if($this->db->escape_str($params['PT_USE_YN'])=="Y"){
			$query .=" ,PT_USE_YN='Y' ";
		}
		$query .=" where PT_IDX=".$this->db->escape_str($params['PT_IDX'])."";
		
	    $result = $this->db->query($query);
	    
    }
    //총포인트 계산
	public function rowdata_pl($params=array()){
		$query = "select * from TB_POINT_LIST  where PL_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."'  order by PL_IDX desc LIMIT 1 ";
		$result = $this->db->query($query);
	    return $result->row_array();
	}
	//자녀 추가 포인트 여부
	public function sms_children_count($params=array()) {
        $query = "select sum(PL_POINT) as pl_point from TB_POINT_LIST where PL_CODE='PC02' and PL_ACCOUNTCODE='".$this->db->escape_str($params['PL_ACCOUNTCODE'])."' and PL_TYPE='U' and PL_REAL_YN='Y' ";
        
		$result = $this->db->query($query);
	    return $result->row_array();
    }
	 public function point_list($params=array()) {
        $query = "select * from TB_POINT_LIST as a inner join TB_MEMBERS as b on a.PL_ACCOUNTCODE=b.M_ACCOUNTCODE where b.M_USEYN='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by PL_IDX desc limit ".$params['limit'];

        }

        $result = $this->db->query($query);
        return $result->result_array();
    }
    //월별 차감포인트
	public function outdata_pt($params=array()) {
        $query = "select sum(PT_POINT) as pt_point from TB_POINT_TOTAL  
        where PT_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."'  and PT_CREDATE <date_add(now(), interval -2 year) and PT_USE_YN='N' and PT_REAL_YN='Y' ";
        
		$result = $this->db->query($query);
	    return $result->row_array();
    }

    // 회원 데이터
    public function rowdata_user($params=array()){
        $query = "select * from TB_MEMBERS  where M_ACCOUNTCODE='".$this->db->escape_str($params['M_ACCOUNTCODE'])."' ";
        $result = $this->db->query($query);
        return $result->row_array();
    }

    // 상품권 교환신청 USER
    public function insert_point_change($params=array()) {
        $query = "insert into TB_POINT_CHANGE (
					TP_ACCOUNTCODE
					,TP_ACCOUNT
					,TP_NAME
					,TP_CP
					,TP_POINT
					,TP_CHANGE_YN
					,TP_CREDATE
					) values (
					'".$this->db->escape_str($params['M_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['M_ACCOUNT'])."',
					'".$this->db->escape_str($params['M_NAME'])."',
					'".$this->db->escape_str($params['M_HP'])."',
					'50000',
					'N',
					now()
					)";
        $result = $this->db->query($query);

    }
    
}
?>
