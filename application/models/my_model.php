<?php
/*
| -------------------------------------------------------------------
| @ TITLE   my 모델
| @ AUTHOR  jschoi
| @ SINCE   17. 12. 5
| -------------------------------------------------------------------
*/

class my_model extends CI_Model {
    protected $table = array("data");
	
	
	public function mypage_modify($params=array()) {
		$query = "update TB_MEMBERS set
					M_HP='".$this->db->escape_str($params['M_HP'])."', 
					M_EMAIL='".$this->db->escape_str($params['M_EMAIL'])."',
					M_POST='".$this->db->escape_str($params['M_POST'])."',
					M_ADDR1='".$this->db->escape_str($params['M_ADDR1'])."',
					M_ADDR2='".$this->db->escape_str($params['M_ADDR2'])."',
					M_CP='".$this->db->escape_str($params['M_CP'])."',
					M_SMS_YN='".$this->db->escape_str($params['M_SMS_YN'])."',
					M_EMS_YN='".$this->db->escape_str($params['M_EMS_YN'])."'
					where M_ACCOUNTCODE='".$this->db->escape_str($params['M_ACCOUNTCODE'])."' and M_ACCOUNT='".$this->db->escape_str($params['M_ACCOUNT'])."'";

	    $result = $this->db->query($query);
    
	}
	public function del_member($params=array()) {
		$query = "update TB_MEMBERS set 
					M_USEYN='N',
					M_MCPOINT=0,
					M_DELDATE=now()
					where M_ACCOUNTCODE='".$this->db->escape_str($params['M_ACCOUNTCODE'])."' and M_ACCOUNT='".$this->db->escape_str($params['M_ACCOUNT'])."'";

	    $result = $this->db->query($query);
	}
	public function del_log($params=array()) {

		$query = "insert into  TB_WITHDRAWAL_LOG (
					WL_CERTKEY,
					WL_M_ACCOUNT,
					WL_M_ACCOUNTCODE,
					WL_CREDATE,
					WL_IP
					) values(
					'".$this->db->escape_str($params['M_CERTKEY'])."',
					'".$this->db->escape_str($params['M_ACCOUNT'])."',
					'".$this->db->escape_str($params['M_ACCOUNTCODE'])."',
					now(),
					'".$this->db->escape_str($params['ME_IP'])."'
					)";
		$result = $this->db->query($query);
	}
	public function mypage_log($params=array()) {

		$query = "insert into  LOG_MEMBER_EDIT (
					ME_M_ACCOUNTCODE,
					ME_ACCOUNT,
					ME_NAME,
					ME_LOG,
					ME_TYPE,
					ME_CREDATE,
					ME_IP
					) values(
					'".$this->db->escape_str($params['ME_M_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['ME_ACCOUNT'])."',
					'".$this->db->escape_str($params['ME_NAME'])."',
					'".$this->db->escape_str($params['ME_LOG'])."',
					'".$this->db->escape_str($params['ME_TYPE'])."',
					now(),
					'".$this->db->escape_str($params['ME_IP'])."'
					)";
		$result = $this->db->query($query);
	}
	public function my_orderlist($params=array()) {
        $query = "select * from TB_ORDER_MASTER as a join TB_PRODUCT_DATA as b on a.OM_PRODUCTCODE=b.PD_PRODUCTCODE where OM_ACCOUNTCODE='".$params['ACCOUNTCODE']."' and OM_REAL_YN='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by OM_IDX desc limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }
    public function my_qnalist($params=array()) {
        $query = "select * from TB_QNA where QNA_ACCOUNTCODE='".$params['ACCOUNTCODE']."' and QNA_REAL_YN='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by QNA_CREDATE desc limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }
    public function my_orderview($params=array()){
        $query = ("select a.*,b.*,OP_PAYDATE,OP_STATE,M_ACCOUNT from TB_ORDER_MASTER as a inner 
					join TB_ORDER_DETAIL as b on a.OM_ORDERCODE=b.OD_OM_ORDERCODE inner join TB_ORDER_PAYMENT as c on a.OM_ORDERCODE=c.OP_OM_ORDERCODE 
					inner join TB_MEMBERS as d on a.OM_ACCOUNTCODE=d.M_ACCOUNTCODE where   a.OM_REAL_YN='Y' and OM_IDX='".$params['OM_IDX']."'");        
        $result = $this->db->query($query);
        return $result->row_array();
    }
}
?>
