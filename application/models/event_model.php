<?php
/*
| -------------------------------------------------------------------
| @ TITLE   이벤트
| @ AUTHOR  PJH
| @ SINCE   17.12. 11.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class event_model extends CI_Model {

    // 이벤트

    public function event_list($params=array()) {
        $query = "SELECT * FROM TB_EVENT where E_DISPLAY_YN='Y' and E_REAL_YN='Y' ";
        $query .=$params['where'];
		 if($params['count_yn']=="N"){
            $query .=" order by e_credate desc limit ".$params['limit'];
        }
		 
        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function event_next($params=array()) {
        $query = "select e_idx as next_event_idx,e_title as next_event_title from TB_EVENT as a where e_real_yn='Y' and e_display_yn='Y' and e_credate > '".$params['e_credate']."' order by e_credate asc limit 1 ";
        $result = $this->db->query($query);
        return $result->row_array();
    }
    public function event_prev($params=array()) {
        $query = "select e_idx as prev_event_idx,e_title as prev_event_title from TB_EVENT as a where e_real_yn='Y' and e_display_yn='Y' and e_credate < '".$params['e_credate']."' order by e_credate desc limit 1 ";
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function entry_row($params=array()){
        $query = " select count(*) as cnt from TB_EVENT_TARGET a inner join TB_EVENT b on a.e_idx=b.e_idx where a.ET_IDX!='' and a.e_idx = ".$params['idx']." and a.ET_ACCOUNTCODE ='".$params['user_code']."' and a.ET_ACCOUNT = '".$params['user_account']."' and a.E_IDX!='' and b.E_CHK_YN = 'N' ";
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function entry_insert($Params=array()) {
        $query= "insert into TB_EVENT_TARGET
        (E_IDX,ET_ACCOUNTCODE,ET_ACCOUNT,ET_NAME,ET_JOINDATE,ET_POINT,ET_IP) values
        ('".$Params['idx']."','".$Params['user_code']."','".$Params['user_account']."','".$Params['user_name']."',now(),'".$Params['tmp_point']."','".$Params['et_ip']."')";

        $this->db->query($query);
    }

}
?>
