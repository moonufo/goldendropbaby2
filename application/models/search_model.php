<?php
/*
| -------------------------------------------------------------------
| @ TITLE   일루마 STORY
| @ AUTHOR  cjs
| @ SINCE   18. 02. 26
| -------------------------------------------------------------------
*/

class search_model extends CI_Model {
   // protected $table = array("data");
	public function search_list($params=array()) {
		if($params['search_type']=="text" || $params['search_type']==""){
			
		$query = "SELECT subject, title,content,credate,url
					FROM (
					select DM_TITLE as title,'' as subject,'' as content,DM_CREDATE as credate, CONCAT('https://dartz.swtown.co.kr/diary/smart_view?per_page=0&search_gubun=&dm_idx=',DM_IDX) as url   from TB_DIARY_SMART where DM_DISPLAY_YN='Y' and DM_REAL_YN='Y'
					UNION ALL
					select DS_TITLE as title,'' as subject,'' as content,DS_CREDATE as credate, CONCAT('https://dartz.swtown.co.kr/diary/special_view?per_page=0&ds_idx=',DS_IDX) as url  from TB_DIARY_SPECIAL where  DS_DISPLAY_YN='Y' and DS_REAL_YN='Y'
					UNION ALL
					select TSE_TITLE as title,TSE_SUBJECT as subject,TSE_CONTENT as content, TSE_CREDATE as credate, TSE_URL as url  from TB_SEARCH where TSE_REAL_YN='Y'
					) x where url!='' ";
        
		}else{
			$query="SELECT title,tag,content,credate,url
						FROM (
						select DM_TITLE as title,DM_TAG as tag,'' as content,DM_CREDATE as credate, CONCAT('https://dartz.swtown.co.kr/diary/smart_view?per_page=0&search_gubun=&dm_idx=',DM_IDX) as url  from TB_DIARY_SMART where DM_DISPLAY_YN='Y' and DM_REAL_YN='Y'
						UNION ALL
						select DS_TITLE as title,DS_TAG as tag,'' as content,DS_CREDATE as credate, CONCAT('https://dartz.swtown.co.kr/diary/special_view?per_page=0&ds_idx=',DS_IDX) as url from TB_DIARY_SPECIAL where  DS_DISPLAY_YN='Y' and DS_REAL_YN='Y'
						UNION ALL
						select TSE_TITLE as title,TSE_TAG as tag,TSE_CONTENT as content,TSE_CREDATE as credate, TSE_URL as url from TB_SEARCH where TSE_REAL_YN='Y'
						) x where url!='' ";
			
			
		}
		$query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by credate desc limit ".$params['limit'];
        }
        $result = $this->db->query($query);
        return $result->result_array();
    }
    public function tag_list($params=array()) {
     	$query = "select * from TB_SEARCH_TAG where ST_DISPLAY_YN='Y' order by ST_ORDERBY asc";
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
}
?>
