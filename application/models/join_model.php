<?php
/*
| -------------------------------------------------------------------
| @ TITLE   가입 모델
| @ AUTHOR  jschoi
| @ SINCE   17. 12. 5
| -------------------------------------------------------------------
*/

class join_model extends CI_Model {
    protected $table = array("data");
	
	public function member_chk($params=array()) {
        $query = (" select count(*) as cnt from TB_MEMBERS where M_ACCOUNT='".$this->db->escape_str($params['M_ACCOUNT'])."'");
	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	public function member_chk_tw($params=array()) {
        $query = (" select count(*) as cnt from  TB_WITHDRAWAL_LOG where WL_CREDATE>date(subdate(now(), INTERVAL 2 year)) and WL_M_ACCOUNT='".$this->db->escape_str($params['M_ACCOUNT'])."' ");
	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	
	public function member_join($params=array()) {
		$query = "insert into TB_MEMBERS (
				M_ACCOUNTCODE,M_IDX,M_NAME,M_ACCOUNT,M_PW,M_GENDER,M_BIRTHDATE,M_HP,M_EMAIL,
				M_POST,M_ADDR1,M_ADDR2,M_CP,M_SMS_YN,M_EMS_YN,M_ENTRYOBJECT,M_ENTRYOBJECT_TXT,
				M_RECOMMEND,M_JOINTYPE,M_CERTKEY,M_MCPOINT,M_CREDATE,M_IP)
				values (
				'".$this->db->escape_str($params['M_ACCOUNTCODE'])."','".$this->db->escape_str($params['M_IDX'])."','".$this->db->escape_str($params['M_NAME'])."','".$this->db->escape_str($params['M_ACCOUNT'])."','".$this->db->escape_str($params['M_PW'])."','".$this->db->escape_str($params['M_GENDER'])."','".$this->db->escape_str($params['M_BIRTHDATE'])."','".$this->db->escape_str($params['M_HP'])."','".$this->db->escape_str($params['M_EMAIL'])."',
				'".$this->db->escape_str($params['M_POST'])."','".$this->db->escape_str($params['M_ADDR1'])."','".$this->db->escape_str($params['M_ADDR2'])."','".$this->db->escape_str($params['M_CP'])."','".$this->db->escape_str($params['M_SMS_YN'])."','".$this->db->escape_str($params['M_EMS_YN'])."','".$this->db->escape_str($params['M_ENTRYOBJECT'])."','".$this->db->escape_str($params['M_ENTRYOBJECT_TXT'])."',
				'".$this->db->escape_str($params['M_RECOMMEND'])."','P','".$this->db->escape_str($params['M_CERTKEY'])."','".$this->db->escape_str($params['M_MCPOINT'])."',now(),'".$this->db->escape_str($params['M_IP'])."'
				)";
		
		
	    $result = $this->db->query($query);
    }

	public function last_row(){
		$query = (" select M_IDX from TB_MEMBERS order by M_IDX desc limit 1");
	    $result = $this->db->query($query);
	    return $result->row_array();
	}
	
	public function children_join($params=array()){
		$query="insert into TB_MEMBERS_CHILDREN (
				MC_ACCOUNTCODE,MC_NAME,MC_BIRTHDATE,MC_GENDER,MC_CREDATE,MC_IP
				) 
				values (
				'".$this->db->escape_str($params['MC_ACCOUNTCODE'])."','".$this->db->escape_str($params['MC_NAME'])."',
				'".$this->db->escape_str($params['MC_BIRTHDATE'])."','".$this->db->escape_str($params['MC_GENDER'])."',
				now(),'".$this->db->escape_str($params['MC_IP'])."')";
		$result = $this->db->query($query);
	}
	
	public function join_chk($params=array()) {
		
        $query = (" select count(*) as cnt from TB_MEMBERS where M_USEYN=1 and  REPLACE(M_HP, '-', '')='".htmlspecialchars($params['M_HP'],ENT_QUOTES)."'");
	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	public function join_hpchk($params=array()) {
        $query = (" select * from TB_MEMBERS 
        where  M_NAME='".htmlspecialchars($params['M_NAME'],ENT_QUOTES)."' and  M_USEYN=1 and  REPLACE(M_HP, '-', '')='".htmlspecialchars($params['M_HP'],ENT_QUOTES)."'");
	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	public function join_pw_hpchk($params=array()) {
        $query = (" select * from TB_MEMBERS 
        where  M_ACCOUNT='".htmlspecialchars($params['M_ACCOUNT'],ENT_QUOTES)."' and  M_USEYN=1 and  REPLACE(M_HP, '-', '')='".htmlspecialchars($params['M_HP'],ENT_QUOTES)."'");

	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	
	

}
?>
