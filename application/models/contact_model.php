<?php
/*
| -------------------------------------------------------------------
| @ TITLE   Contact
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class contact_model extends CI_Model {
    protected $table = array("data");

    //공지사항
    public function notice_list($params=array()) {
        $query = "select * from TB_NOTICE where notice_title!='' and notice_real_yn='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by notice_idx desc limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function notice_next($params=array()) {
        $query = "select notice_idx as next_notice_idx,notice_title as next_notice_title from TB_NOTICE as a where notice_real_yn='Y' and notice_title!='' and notice_idx > ".$params['notice_idx']." order by notice_idx asc limit 1 ";

        $result = $this->db->query($query);
        return $result->row_array();
    }
    public function notice_prev($params=array()) {
        $query = "select notice_idx as prev_notice_idx,notice_title as prev_notice_title from TB_NOTICE as a where notice_real_yn='Y' and notice_title!='' and notice_idx < ".$params['notice_idx']." order by notice_idx desc limit 1 ";

        $result = $this->db->query($query);
        return $result->row_array();
    }

    // FAQ
    public function faq_list($params=array()) {
        $query = "select * from TB_FAQ where F_REAL_YN='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by F_IDX desc ";
        }
        $result = $this->db->query($query);
        return $result->result_array();
    }



    public function insert_contact($Params=array()) {
        $this->db->query("insert into TB_QNA
        (QNA_EMAIL,QNA_ID,QNA_NAME,QNA_ACCOUNTCODE,QNA_QUESTION,QNA_CREDATE,QNA_IP) values
        ('".$this->db->escape_str($Params['QNA_EMAIL'])."','".$this->db->escape_str($Params['QNA_ID'])."','".htmlspecialchars($Params['qna_name'],ENT_QUOTES)."','".$this->db->escape_str($Params['QNA_ACCOUNTCODE'])."','".addslashes($Params['qna_question'])."',now(),'".$Params['qna_ip']."')");

        return $this->db->insert_id();
    }

    public function faq_view_cnt($params=array()){
        $query = (" select F_CNT as cnt from TB_FAQ where F_IDX = ".$params['id']." and F_REAL_YN='Y' ");
        $result = $this->db->query($query);
        return $result->row_array();
    }

}
?>
