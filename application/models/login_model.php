<?php
/*
| -------------------------------------------------------------------
| @ TITLE   가입 모델
| @ AUTHOR  jschoi
| @ SINCE   17. 12. 5
| -------------------------------------------------------------------
*/

class login_model extends CI_Model {
   // protected $table = array("data");

	public function get_login_info($params=array()) {
		$query = ("select * from TB_MEMBERS where M_USEYN='Y' and M_ACCOUNT='".$this->db->escape_str($params['username'])."'");

	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	public function login_history($params=array()) {

		$query = "insert into  LOG_LOING_HISTORY (
					LH_M_ACCOUNTCODE,
					LH_M_ACCOUNT,
					LH_LOGINTYPE,
					LH_AGENT,
					LH_LOGIN_CREDATE,
					LH_LOGINYN,
					LH_IP
					) values(
					'".$this->db->escape_str($params['LH_M_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['LH_M_ACCOUNT'])."',
					'P',
					'".$this->db->escape_str($params['LH_AGENT'])."',
					now(),
					'".$this->db->escape_str($params['LH_LOGINYN'])."',
					'".$this->db->escape_str($params['LH_IP'])."'
					)";
		$result = $this->db->query($query);
	}
	public function login_chk($params=array()) {
		$query = ("update  TB_MEMBERS set M_LOGINDATE=now()  where M_ACCOUNTCODE='".$this->db->escape_str($params['M_ACCOUNTCODE'])."' and M_USEYN='Y' ");

	    $result = $this->db->query($query);
    }
	public function login_count_chk($params=array()) {
		$query = "select *,date(LH_LOGIN_CREDATE) from LOG_LOING_HISTORY where LH_M_ACCOUNTCODE='".$this->db->escape_str($params['M_ACCOUNTCODE'])."' and date(LH_LOGIN_CREDATE)=date(now())";

	    $result = $this->db->query($query);
	    return $result->row_array();
    }
	
	
}
?>
