<?php


class common_model extends CI_Model {
    protected $table = array("data");

    private function _query($params) {


        /**
         * 관리자 회원 상세검색
         */
        if(isset($params["mbHumanStatus"])){
            $humanDate = date("Ymd", strtotime(date("Ymd")." -".$params["mbHumanStatus"]." day"));
            $where["mbLastLoginDate <="] = $humanDate;
            unset($params["mbHumanStatus"]);
        }
        if(isset($params["mbAge"])){
            $year = date("Y")- ($params["mbAge"]-1);
            if($params["mbAge"] != "70")    $where["mbBirth <="] = $year."-00-00";
            $where["mbBirth >="] = ($year-10)."-00-00";
            unset($params["mbAge"]);
        }




        /**
         * 키워드 선택검색
         */
        if(isset($params["secKey"]) && isset($params["secTxt"])) {
            if($params["secKey"] == "all"){
                $secColumn = $params['secColumn'];
                $secTxt = $params["secTxt"];
                unset($params['secColumn']);
            }else   $this->db->like($params["secKey"], $params["secTxt"]);
            unset($params["secKey"]); unset($params["secTxt"]);
        }
        /**
         * 일반 컬럼들 선택검색
         */
        foreach($params as $key => $val){
            if($key != "oKey" && $key != "oType" && $key != 'gType' &&
                $key != 'limit' && $key != 'offset' && $key != 'page'){
                if(count(explode("_0",$key)) >1 ){
                    $where[current(explode("_0",$key))." >="] = $val;
                } else if(count(explode("_1",$key)) >1 ){
                    $where[current(explode("_1",$key))." <="] = $val;
                }else {
                    $where[$key] = $val;
                }
            }
        }
        /**
         * 키워드 전체검색
         */
        if(isset($secColumn)) {
            foreach($secColumn as $val)
                $this->db->or_like($val, $secTxt);
        }



        $where["(1)"] = "1";
        return $where;
    }

    public function _select_cnt($secTable, $params=array()) {
        $this->table[0] = $secTable;
        $where = $this->_query($params);
        $this->db->where($where);
        $this->db->from($this->table[0]);
        return $this->db->count_all_results();
    }

    public function _select_list($secTable, $params=array()) {
        $this->table[0] = $secTable;
        $limit = (isset($params["limit"])) ? $params["limit"] : NULL;
        $offset = (isset($params["offset"])) ? $params["offset"] : NULL;
        $where = $this->_query($params);
        if(isset($params["oType"]) && isset($params["oKey"])){
            $this->db->order_by($params["oKey"], $params["oType"]);
        }
        if(isset($params["gType"])){
            $this->db->group_by($params["gType"]);
        }
        return $this->db->get_where($this->table[0], $where, $limit, $offset)->result_array();
    }

    public function _select_row($secTable, $where) {
        $this->table[0] = $secTable;
        return $this->db->where($where)->get($this->table[0], 1)->row_array();
    }

    public function _insert($secTable, $data) {
        $this->table[0] = $secTable;
        return $this->db->insert($this->table[0], $data);
    }

    public function _delete($secTable, $where) {
        $this->table[0] = $secTable;
        $this->db->where($where);
        $this->db->delete($this->table[0]);
    }

    public function _modify($secTable, $data, $where) {
        $this->table[0] = $secTable;
        return $this->db->update($this->table[0], $data, $where);
    }

    public function _set($secTable, $params) {
        $this->table[0] = $secTable;
        $this->db->set($params, NULL, FALSE);
        return $this->db->update($this->table[0]);
    }

    public function _replace($secTable, $data) {
        $this->table[0] = $secTable;
        return $this->db->replace($this->table[0], $data);
    }


    public function _set_insertNO($secTable, $params=array()) {
        $query = $this->db->query("select ".$params['secKey']." from (select IFNULL(max(".$params['secKey']."),0) + 1 as ".$params['secKey']." from ".$secTable.") as ".$params['secKey']);
        return $query->row_array();
    }

    public function _set_insertNO_where($secTable, $params=array()) {
        $query = $this->db->query("select ".$params['secKey']." from (select IFNULL(max(".$params['secKey']."),0) + 1 as ".$params['secKey']." from ".$secTable.  " WHERE ".$params['where_key']. " = '" .$params['where_val']. "'". ") as ".$params['secKey']);
        return $query->row_array();
    }

//포인트 관련
	public function pl_insert($params=array()) {
      $query = "insert into TB_POINT_LIST(
					PL_ACCOUNTCODE
					,PL_TYPE
					,PL_CODE
					,PL_USE
					,PL_POINT
					,PL_TITLE
					,PL_MEMO
					,PL_CREDATE
					,PL_REAL_YN
					,PL_IP
					) values (
					'".$this->db->escape_str($params['PL_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['PL_TYPE'])."',
					'".$this->db->escape_str($params['PL_CODE'])."',
					'".$this->db->escape_str($params['PL_USE'])."',
					".$this->db->escape_str($params['PL_POINT']).",
					'".$this->db->escape_str($params['PL_TITLE'])."',
					'".$this->db->escape_str($params['PL_MEMO'])."',
					now(),
					'Y',
					'".$this->db->escape_str($params['PL_IP'])."'
					)";
	   $result = $this->db->query($query);
	   return $this->db->insert_id();
    }
	
 	public function pt_insert($params=array()) {
		$query = "insert into TB_POINT_TOTAL (
					PT_PL_IDX
					,PT_ACCOUNTCODE
					,PT_POINT
					,PT_CREDATE
					,PT_USE_YN
					,PT_REAL_YN
					) values (
					'".$this->db->escape_str($params['PT_PL_IDX'])."',
					'".$this->db->escape_str($params['PT_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['PT_POINT'])."',
					now(),
					'N',
					'Y'
					)";
	   $result = $this->db->query($query);
	  
    }
    
	public function pl_totalpoint_update($params=array()) {
        $query = (" update TB_POINT_LIST set PL_TOTALPOINT =(select sum(PT_POINT) from TB_POINT_TOTAL 
        where PT_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."' and PT_USE_YN='N' and PT_REAL_YN='Y' ) where pl_idx=".$this->db->escape_str($params['PL_IDX'])." ");
	    $result = $this->db->query($query);
	    
    }
    public function member_totalpoint_update($params=array()) {
        $query = (" update TB_MEMBERS set M_POINT =(select sum(PT_POINT) from TB_POINT_TOTAL 
        where PT_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."' and PT_USE_YN='N' and PT_REAL_YN='Y' ) where M_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."' ");
	    $result = $this->db->query($query);
	    
    }
	public function pt_rowdata($params=array()){
		$query = "select * from TB_POINT_TOTAL  where PT_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])." ' AND PT_USE_YN='N' AND PT_REAL_YN='Y' AND PT_POINT >0 order by PT_CREDATE ASC LIMIT 1 ";
		$result = $this->db->query($query);
	
	    return $result->row_array();
	}
	
	public function pc_insert($params=array()) {
		$query = "insert into TB_POINT_CHK (
					PC_PL_IDX
					,PC_PT_IDX
					,PC_POINT
					,PC_CREDATE
					,PC_REAL_YN
					) values (
					'".$this->db->escape_str($params['PC_PL_IDX'])."',
					'".$this->db->escape_str($params['PC_PT_IDX'])."',
					'".$this->db->escape_str($params['PT_POINT'])."',
					now(),
					'Y'
					)";
	   $result = $this->db->query($query);
	  
    }
    
	public function pt_updatepoint($params=array()) {
        $query = " update TB_POINT_TOTAL set 
        PT_POINT = ".$this->db->escape_str($params['PT_POINT'])."";
		if($this->db->escape_str($params['PT_USE_YN'])=="Y"){
			$query .=" ,PT_USE_YN='Y' ";
		}
		$query .=" where PT_IDX=".$this->db->escape_str($params['PT_IDX'])."";
		
	    $result = $this->db->query($query);
	    
    }

	public function rowdata_pl($params=array()){
		$query = "select * from TB_POINT_LIST  where PL_ACCOUNTCODE='".$this->db->escape_str($params['PT_ACCOUNTCODE'])."'  order by PL_IDX desc LIMIT 1 ";
		$result = $this->db->query($query);
	    return $result->row_array();
	}
	//포인트 끝

}

?>
