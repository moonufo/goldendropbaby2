<?php
/*
| -------------------------------------------------------------------
| @ TITLE   주문 모델
| @ AUTHOR  jschoi
| @ SINCE   17. 12. 5
| -------------------------------------------------------------------
*/

class order_model extends CI_Model {
    protected $table = array("data");

	public function pay_log($params=array()) {
		$query = "insert into LOG_EASYPAY_LOG (
				EL_TYPE, EL_RES_CD, EL_RES_MSG, EL_CNO, EL_AMOUNT, EL_ORDER_NO, EL_AUTH_NO,
				EL_TRAN_DATE, EL_CARD_NO, EL_ISSUER_CD, EL_INSTALL_PERIOD, EL_PAY_TYPE, EL_CREDATE, EL_IP)
				values (
				'".$this->db->escape_str($params['EL_TYPE'])."','".$this->db->escape_str($params['res_cd'])."','".$this->db->escape_str($params['res_msg'])."','".$this->db->escape_str($params['cno'])."','".$this->db->escape_str($params['amount'])."','".$this->db->escape_str($params['order_no'])."','".$this->db->escape_str($params['auth_no'])."',
				'".$this->db->escape_str($params['tran_date'])."','".$this->db->escape_str($params['card_no'])."','".$this->db->escape_str($params['issuer_cd'])."','".$this->db->escape_str($params['install_period'])."','".$this->db->escape_str($params['pay_type'])."',now(),'".$this->db->escape_str($params['EL_IP'])."'
				)";
		
		
	    $result = $this->db->query($query);
    }
    
    public function insert_order($params=array()) {
		$query = "insert into TB_ORDER_MASTER (
				OM_ORDERCODE, 
				OM_SHOPCODE, 
				OM_TYPE,
				OM_PAYKEY,
				OM_NAME,
				OM_HP,
				OM_CP,
				OM_POST,
				OM_ADDR1,
				OM_ADDR2,
				OM_MEMO, 
				OM_COUNT,
				OM_CHARGE,
				OM_POINT,
				OM_SENDDATE,
				OM_SENDCYCLE,
				OM_PRODUCTCODE,
				OM_ACCOUNTCODE,
				OM_CREDATE, 
				OM_IP)
				values (
				'".$this->db->escape_str($params['OM_ORDERCODE'])."',
				'".$this->db->escape_str($params['OM_SHOPCODE'])."',
				'".$this->db->escape_str($params['OM_TYPE'])."',
				'".$this->db->escape_str($params['OM_PAYKEY'])."',
				'".$this->db->escape_str($params['OM_NAME'])."',
				'".$this->db->escape_str($params['OM_HP'])."',
				'".$this->db->escape_str($params['OM_CP'])."',
				'".$this->db->escape_str($params['OM_POST'])."',
				'".$this->db->escape_str($params['OM_ADDR1'])."',
				'".$this->db->escape_str($params['OM_ADDR2'])."',
				'".$this->db->escape_str($params['OM_MEMO'])."',
				'".$this->db->escape_str($params['OM_COUNT'])."',
				'".$this->db->escape_str($params['OM_CHARGE'])."',
				'".$this->db->escape_str($params['OM_POINT'])."',
				'".$this->db->escape_str($params['OM_SENDDATE'])."',
				'".$this->db->escape_str($params['OM_SENDCYCLE'])."',
				'".$this->db->escape_str($params['OM_PRODUCTCODE'])."',
				'".$this->db->escape_str($params['OM_ACCOUNTCODE'])."',
				now(),
				'".$this->db->escape_str($params['OM_IP'])."'
				)";
		
		
	    $result = $this->db->query($query);
	    return $this->db->insert_id();
    }
    
	public function insert_detail($params=array()) {
		$query = "insert into TB_ORDER_DETAIL (
					OD_OM_ORDERCODE,
					OD_ACCOUNTCODE,
					OD_TYPE,
					OD_OM_IDX,
					OD_COUNT,
					OD_COUNT_SET,
					OD_COUNT_CHK,
					OD_PAY_YN,
					OD_SEND_YN,
					OD_SENDDATE,
					OD_SENDCYCLE,
					OD_TAGPRICE,
					OD_SALEPRICE,
					OD_DISCOUNTPRICE,
					OD_PRODUCTCODE,
					OD_CHARGE,
					OD_POINT,
					OD_CREDATE,
					OD_IP)
					values
					(
					'".$this->db->escape_str($params['OD_OM_ORDERCODE'])."',
					'".$this->db->escape_str($params['OD_ACCOUNTCODE'])."',
					'".$this->db->escape_str($params['OD_TYPE'])."',
					'".$this->db->escape_str($params['OD_OM_IDX'])."',
					'".$this->db->escape_str($params['OD_COUNT'])."',
					'".$this->db->escape_str($params['OD_COUNT_SET'])."',
					'N',
					'".$this->db->escape_str($params['OD_PAY_YN'])."',
					'N',
					'".$this->db->escape_str($params['OD_SENDDATE'])."',
					'".$this->db->escape_str($params['OD_SENDCYCLE'])."',
					
					'".$this->db->escape_str($params['OD_TAGPRICE'])."',
					'".$this->db->escape_str($params['OD_SALEPRICE'])."',
					'".$this->db->escape_str($params['OD_DISCOUNTPRICE'])."',
					'".$this->db->escape_str($params['OD_PRODUCTCODE'])."',
					'".$this->db->escape_str($params['OD_CHARGE'])."',
					'".$this->db->escape_str($params['OD_POINT'])."',
					now(),
					'".$this->db->escape_str($params['OD_IP'])."'
					)";
		
		
	    $result = $this->db->query($query);
	    return $this->db->insert_id();
    }
	public function insert_payment($params=array()) {
		$query = "insert into TB_ORDER_PAYMENT( 
		OP_OM_ORDERCODE,
		OP_OD_IDX,
		OP_STATE,
		OP_STATE_MEMO,
		OP_RES_CD,
		OP_RES_MSG,
		OP_CNO,
		OP_AMOUNT,
		OP_AUTH_NO,
		OP_TRAN_DATE,
		OP_PAY_TYPE,
		OP_PAYDATE,
		OP_CREDATE,
		OP_IP)
	values(
		'".$this->db->escape_str($params['OP_OM_ORDERCODE'])."',
		'".$this->db->escape_str($params['OP_OD_IDX'])."',
		'".$this->db->escape_str($params['OP_STATE'])."',
		'".$this->db->escape_str($params['OP_STATE_MEMO'])."',
		'".$this->db->escape_str($params['OP_RES_CD'])."',
		'".$this->db->escape_str($params['OP_RES_MSG'])."',
		'".$this->db->escape_str($params['OP_CNO'])."',
		'".$this->db->escape_str($params['OP_AMOUNT'])."',
		'".$this->db->escape_str($params['OP_AUTH_NO'])."',
		'".$this->db->escape_str($params['OP_TRAN_DATE'])."',
		
		'".$this->db->escape_str($params['OP_PAY_TYPE'])."', ";
		if($params['OP_PAYDATE']=="Y"){
			$query .="now(),";
		}else{
			$query .="null,";
		}
		$query .="now(),
		'".$this->db->escape_str($params['OP_IP'])."')";
			
		$result = $this->db->query($query);
		
	}
	public function get_orderdata($params=array()){
		$query = "select * from TB_ORDER_MASTER as a join TB_PRODUCT_DATA as b on a.OM_PRODUCTCODE=b.PD_PRODUCTCODE where a.OM_ORDERCODE='".$this->db->escape_str($params['OM_ORDERCODE'])."' and OM_REAL_YN='Y' ";
		$result = $this->db->query($query);
	    return $result->row_array();
	}
}
?>
