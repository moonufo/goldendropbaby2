<?php

class main_model extends CI_Model {
    protected $table = array("data");

        public function select_instar_list() {
            $query = "select * from TB_INSTAR_DETAIL where id_idx!='' and id_url !='' and id_main_yn='Y' and id_real_yn='Y' and ID_DISPLAY_YN='Y' ";

            $result = $this->db->query($query);
            return $result->result_array();
        }

        public function select_visual_list() {
            $query = "select * from TB_MAIN_VISUAL where mv_idx!='' and mv_display_yn='Y' and mv_real_yn='Y' and mv_strdate <= now() and mv_enddate >= now() ";

            $result = $this->db->query($query);
            return $result->result_array();
        }

        public function select_popup_list() {
            $query = "select * from TB_MAIN_POP where mp_idx!='' and mp_display_yn='Y' and mp_real_yn='Y' and mp_strdate <= now() and mp_enddate >= now() ";

            $result = $this->db->query($query);
            return $result->result_array();
        }

        public function rowdata_mp($params=array()){
            $query = "select * from TB_MAIN_POP  where MP_IDX='".$this->db->escape_str($params['mp_idx'])."' '".$this->db->escape_str($params['where'])."' ";
            $result = $this->db->query($query);
            return $result->row_array();
        }

}
?>
