<?php
/*
| -------------------------------------------------------------------
| @ TITLE   Contact
| @ AUTHOR  PJH
| @ SINCE   17. 12. 5.
| @ PURPOSE 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러 프로그램 페이지 컨트롤러
| -------------------------------------------------------------------
*/

class diary_model extends CI_Model {
    protected $table = array("data");

    //Special Diary
    public function special_list($params=array()) {
        $query = "select * from TB_DIARY_SPECIAL where ds_title!='' and ds_display_yn='Y' and ds_real_yn='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by ds_idx desc limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function special_next($params=array()) {
        $query = "select ds_idx as next_special_idx,ds_title as next_special_title from TB_DIARY_SPECIAL as a where ds_real_yn='Y' and ds_display_yn='Y' and ds_idx > ".$params['ds_idx']." order by ds_idx asc limit 1 ";

        $result = $this->db->query($query);
        return $result->row_array();
    }
    public function special_prev($params=array()) {
        $query = "select ds_idx as prev_special_idx,ds_title as prev_special_title from TB_DIARY_SPECIAL as a where ds_real_yn='Y' and ds_display_yn='Y' and ds_idx < ".$params['ds_idx']." order by ds_idx desc limit 1 ";

        $result = $this->db->query($query);
        return $result->row_array();
    }

    //Smart Diary
    public function smart_list($params=array()) {
        $query = "select * from TB_DIARY_SMART where dm_title!='' and dm_display_yn='Y' and dm_real_yn='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .=" order by dm_idx desc limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function smart_next($params=array()) {
        $query = "select dm_idx as next_smart_idx,dm_title as next_smart_title from TB_DIARY_SMART as a where dm_real_yn='Y' and dm_display_yn='Y' and dm_idx > ".$params['dm_idx']." order by dm_idx asc limit 1 ";

        $result = $this->db->query($query);
        return $result->row_array();
    }
    public function smart_prev($params=array()) {
        $query = "select dm_idx as prev_smart_idx,dm_title as prev_smart_title from TB_DIARY_SMART as a where dm_real_yn='Y' and dm_display_yn='Y' and dm_idx < ".$params['dm_idx']." order by dm_idx desc limit 1 ";

        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function hashtag_list($params=array()) {
        $query = "select * from TB_INSTAR_DETAIL where id_idx!='' and id_real_yn='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .="  order by id_credate desc limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function hashtag_count($params=array()) {
        $query = "select * from TB_INSTAR_DETAIL where id_idx!='' and id_real_yn='Y' ";
        $query .=$params['where'];
        if($params['count_yn']=="N"){
            $query .="  order by id_credate desc  limit ".$params['limit'];
        }

        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function detail_row($params=array()){
        $query = (" select count(*) as cnt from TB_INSTAR_DETAIL where ID_IDX!='' and ID_REAL_YN='Y' ");
        $query .=$params['where'];
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function hash_search($params=array()){
        $query = " select IM_TEXT as im_text from TB_INSTAR_MASTER where IM_IDX!='' and IM_REAL_YN='Y' and IM_DISPLAY_YN='Y' and IM_IDX =".$params['H_TYPE'];
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function hash_first_idx($params=array()){
        $query = " select IM_IDX as im_idx from TB_INSTAR_MASTER where IM_IDX!='' and IM_REAL_YN='Y' and IM_DISPLAY_YN='Y' order by IM_ORDER DESC limit 1 ";
        $result = $this->db->query($query);
        return $result->row_array();
    }

    public function gubun_search($params=array()){
        $query = " select C_CODE_TXT as gubun_txt from COMMON_CODE where C_REAL_YN='Y' and C_CODE_PA =".$params['smart_type'];
        $result = $this->db->query($query);
        return $result->row_array();
    }

}
?>
